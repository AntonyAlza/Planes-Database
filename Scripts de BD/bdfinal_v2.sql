-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-06-2017 a las 16:24:20
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdfinal`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE `alumno` (
  `idalumno` varchar(12) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `ape_pat` varchar(45) DEFAULT NULL,
  `ape_mat` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`idalumno`, `nombre`, `ape_pat`, `ape_mat`) VALUES
('1', 'MARTIN', 'BAZALAR', 'CONTRERAS'),
('2', 'NOAH', 'BAZALAR', 'NEYRA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aula`
--

CREATE TABLE `aula` (
  `idaula` varchar(12) NOT NULL,
  `num_aula` int(11) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `codA_pab` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `aula`
--

INSERT INTO `aula` (`idaula`, `num_aula`, `descripcion`, `codA_pab`) VALUES
('1', 101, NULL, '1'),
('10', 302, NULL, '1'),
('11', 303, NULL, '1'),
('12', 304, NULL, '1'),
('13', 101, NULL, '2'),
('14', 102, NULL, '2'),
('15', 103, NULL, '2'),
('16', 104, NULL, '2'),
('17', 105, NULL, '2'),
('18', 106, NULL, '2'),
('19', 201, NULL, '2'),
('2', 102, NULL, '1'),
('20', 202, NULL, '2'),
('21', 203, NULL, '2'),
('22', 204, NULL, '2'),
('23', 205, NULL, '2'),
('24', 206, NULL, '2'),
('25', 301, NULL, '2'),
('26', 302, NULL, '2'),
('27', 303, NULL, '2'),
('28', 304, NULL, '2'),
('29', 305, NULL, '2'),
('3', 103, NULL, '1'),
('30', 306, NULL, '2'),
('31', 101, NULL, '5'),
('32', 102, NULL, '5'),
('33', 103, NULL, '5'),
('34', 104, NULL, '5'),
('35', 201, NULL, '5'),
('36', 101, NULL, '3'),
('37', 102, NULL, '3'),
('38', 103, NULL, '3'),
('39', 201, NULL, '3'),
('4', 104, NULL, '1'),
('40', 202, NULL, '3'),
('41', 203, NULL, '3'),
('42', 301, NULL, '3'),
('43', 302, NULL, '3'),
('44', 303, NULL, '3'),
('5', 201, NULL, '1'),
('6', 202, NULL, '1'),
('7', 203, NULL, '1'),
('8', 204, NULL, '1'),
('9', 301, NULL, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso`
--

CREATE TABLE `curso` (
  `idcurso` varchar(12) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `creditos` int(11) DEFAULT NULL,
  `h_teo` int(11) DEFAULT NULL,
  `h_lab` int(11) DEFAULT NULL,
  `h_pract` int(11) DEFAULT NULL,
  `estado_hab_inhab` tinyint(1) DEFAULT NULL,
  `req01` varchar(11) DEFAULT NULL,
  `req02` varchar(11) DEFAULT NULL,
  `req03` varchar(11) DEFAULT NULL,
  `cooreq` varchar(11) DEFAULT NULL,
  `cred_req` varchar(12) DEFAULT NULL,
  `nombreProp` varchar(45) DEFAULT NULL,
  `codLab` varchar(12) DEFAULT NULL,
  `usu_crea_reg` varchar(45) DEFAULT NULL,
  `fec_crea_reg` datetime DEFAULT NULL,
  `ult_usu_mod_reg` varchar(45) DEFAULT NULL,
  `fec_ult_mod_reg` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `curso`
--

INSERT INTO `curso` (`idcurso`, `nombre`, `creditos`, `h_teo`, `h_lab`, `h_pract`, `estado_hab_inhab`, `req01`, `req02`, `req03`, `cooreq`, `cred_req`, `nombreProp`, `codLab`, `usu_crea_reg`, `fec_crea_reg`, `ult_usu_mod_reg`, `fec_ult_mod_reg`) VALUES
('90002000000', 'Lenguaje', 2, 1, 0, 2, 1, '-----', NULL, NULL, NULL, NULL, 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('90003000000', 'Filosofia', 3, 3, 0, 0, 1, '-----', NULL, NULL, NULL, NULL, 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('90005000000', 'Actividades I', 1, 0, 0, 2, 1, '-----', NULL, NULL, NULL, NULL, 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('90020000000', 'Actividades II', 1, 0, 0, 2, 1, '90005000000', NULL, NULL, NULL, NULL, 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('90034000000', 'Etica', 2, 1, 0, 2, 1, '', '', '', '', '170 créditos', 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('90037000000', 'Quimic', 3, 1, 0, 4, 1, '90662000000', NULL, NULL, NULL, NULL, 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90049000000', 'Construcción I', 3, 2, 0, 2, 1, '91277000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90053000000', 'Algoritmo y Estructura de Datos I', 5, 3, 0, 4, 1, '91114000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('90054000000', 'Estadistica_y_Probabilidades_I', 4, 3, 0, 2, 1, '90655000000', '', '', '', '', 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('90056000000', 'Física I', 5, 3, 0, 4, 1, '90655000000', '90366000000', '', '', '', 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('90060000000', 'Estadística y Probabilidades II', 4, 3, 0, 2, 1, '90054000000', '', '', '', '', 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('90067000000', 'Construcción II', 4, 3, 0, 2, 1, '90049000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90068000000', 'Circuitos Eléctricos I', 5, 3, 0, 4, 1, '90656000000', '', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90069000000', 'Algoritmo y Estructura de Datos II', 5, 3, 0, 4, 1, '90053000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('90070000000', 'Proyecto II', 4, 4, 0, 0, 1, '90673000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('90072000000', 'Química Industrial', 5, 3, 0, 4, 1, '90056000000', '', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90074000000', 'Física II', 5, 3, 0, 4, 1, '90056000000', '', '', '', '', 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('90077000000', 'Microeconomía', 4, 3, 0, 2, 1, '91274000000', '', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90081000000', 'Teoría de Campos', 4, 2, 0, 4, 1, '90091000000', '90695000000', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90082000000', 'Circuitos Electrónicos I', 5, 3, 0, 4, 1, '90083000000', '90694000000', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90083000000', 'Circuitos Eléctricos II', 5, 3, 0, 4, 1, '90083000000', '90074000000', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90085000000', 'Investigación Operativa I', 4, 3, 0, 2, 1, '90060000000', '90662000000', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90086000000', 'Materiales de Ingeniería', 4, 3, 0, 2, 1, '90072000000', '90177000000', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90087000000', 'Mecánica Aplicada', 5, 4, 0, 2, 1, '90056000000', '', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90088000000', 'Teoría General de Sistemas', 4, 4, 0, 0, 1, '90090000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('90089000000', 'Teoría y Diseño de Base de Datos', 5, 3, 0, 4, 1, '90069000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('90090000000', 'Ingeniería Administrativa', 4, 3, 0, 2, 1, '90077000000', '', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90091000000', 'Física III', 4, 2, 0, 4, 1, '90074000000', '', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90096000000', 'Instalaciones Sanitarias', 3, 2, 0, 2, 1, '90265000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90109000000', 'Líneas de Transmisión y Antenas', 4, 2, 0, 4, 1, '90081000000', '', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90110000000', 'Circuitos Digitales I', 5, 3, 0, 4, 1, '90053000000', '90694000000', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90111000000', 'Circuitos Electrónicos II', 4, 2, 0, 4, 1, '90082000000', '', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90112000000', 'Máquinas Eléctricas', 4, 2, 0, 4, 1, '90083000000', '', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90116000000', 'Investigación Operativa II', 4, 3, 0, 2, 1, '90085000000', '', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90118000000', 'Ingeniería de Métodos I', 4, 3, 0, 2, 1, '90060000000', '', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90119000000', 'Ingeniería de Software I', 5, 3, 0, 4, 1, '90932000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('90122000000', 'Contabilidad General', 4, 3, 0, 2, 1, '', '', '', '', '80 créditos', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90125000000', 'Sistemas de Control I', 4, 2, 0, 4, 1, '90696000000', '', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90126000000', 'Telecomunicaciones I', 5, 3, 0, 4, 1, '90054000000', '90696000000', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90127000000', 'Circuitos Digitales II', 5, 3, 0, 4, 1, '90110000000', '', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90128000000', 'Circuitos Electrónicos III', 4, 2, 0, 4, 1, '90111000000', '', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90131000000', 'Ingeniería de Costos', 4, 3, 0, 2, 1, '90122000000', '', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90132000000', 'Ingeniería de Métodos II', 4, 3, 0, 2, 1, '90118000000', '90136000000', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90134000000', 'Mercadotecnia', 4, 3, 0, 2, 1, '90090000000', '90131000000', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90136000000', 'Ingeniería de Procesos', 5, 3, 0, 4, 1, '90090000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('90137000000', 'Ingeniería de Software II', 5, 3, 0, 4, 1, '90119000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('90140000000', 'Proceso de Manufactura', 4, 1, 0, 6, 1, '91280000000', '', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90141000000', 'Planeamiento y Control de la Producción I', 4, 3, 0, 2, 1, '90116000000', '90132000000', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90145000000', 'Gestión Financiera', 4, 3, 0, 2, 1, '90131000000', '', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90148000000', 'Arquitectura de Computadores I', 5, 3, 0, 4, 1, '90127000000', '', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90150000000', 'Sistemas de Control II', 4, 2, 0, 4, 1, '90125000000', '', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90151000000', 'Telecomunicaciones II', 5, 3, 0, 4, 1, '90126000000', '', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90156000000', 'Planeamiento y Control de la Producción II', 4, 2, 0, 4, 1, '90141000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90158000000', 'Arquitectura de Computadores II', 5, 3, 0, 4, 1, '90148000000', '', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90164000000', 'Control de Calidad', 4, 2, 0, 4, 1, '90140000000', '', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90165000000', 'Diseño de Sistemas de Producción', 4, 3, 0, 2, 1, '90141000000', '', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90170000000', 'Automatización Industrial', 4, 1, 0, 6, 1, '91143000000', '', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90171000000', 'Arquitectura de Redes', 4, 2, 0, 4, 1, '90151000000', '90158000000', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90177000000', 'Diseño Industrial por Computador', 3, 1, 0, 4, 1, '90661000000', '', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90197000000', 'Sistemas de Control Digital', 4, 2, 0, 4, 1, '90150000000', '', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90201000000', 'Planeamiento, Desa. e Ing. del Producto', 4, 2, 0, 4, 1, '90134000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90210000000', 'Ingeniería de Métodos', 4, 3, 0, 2, 1, '90141000000', '90836000000', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90213000000', 'Procesamiento Digital de Señales', 4, 1, 0, 6, 1, '90126000000', '', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90251000000', 'Geología General', 2, 1, 0, 2, 1, '90662000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90254000000', 'Estática', 4, 3, 0, 2, 1, '90056000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90256000000', 'Dinámica', 3, 2, 0, 2, 1, '90056000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90260000000', 'Resistencia de Materiales I', 5, 4, 0, 2, 1, '90254000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90261000000', 'Mecánica de Suelos I', 4, 3, 0, 2, 1, '90251000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90265000000', 'Mecánica de Fluidos I', 5, 4, 0, 2, 1, '90412000000', '90256000000', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90266000000', 'Resistencia de Materiales II', 4, 3, 0, 2, 1, '90260000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90267000000', 'Mecánica de Suelos II', 4, 3, 0, 2, 1, '90261000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90268000000', 'Tecnología del Concreto', 3, 2, 0, 2, 1, '91277000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90269000000', 'Mecánica de Fluidos II', 5, 4, 0, 2, 1, '90265000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90271000000', 'Análisis Estructural I', 4, 3, 0, 2, 1, '90266000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90275000000', 'Concreto Armado I', 4, 3, 0, 2, 1, '90271000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90282000000', 'Caminos I', 4, 3, 0, 2, 1, '91275000000', '90049000000', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90285000000', 'Puentes y Obras de Arte', 3, 2, 0, 2, 1, '90295000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90286000000', 'Concreto Armado II', 4, 3, 0, 2, 1, '90275000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90295000000', 'Análisis Estructural II', 4, 3, 0, 2, 1, '90271000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90309000000', 'Hidráulica', 4, 3, 0, 2, 1, '90596000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90366000000', 'Álgebra Lineal', 5, 4, 0, 2, 1, '----', NULL, NULL, NULL, NULL, 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('90412000000', 'Ecuaciones Diferenciales', 4, 3, 0, 2, 1, '90656000000', '', '', '', '', 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('90458000000', 'Investigación de Mercados', 4, 3, 0, 2, 1, '90134000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('90548000000', 'Formulación y Evaluación de Proyectos', 4, 3, 0, 2, 1, '90145000000', '', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90595000000', 'Presupuesto y Programación de Obra', 4, 2, 0, 4, 1, '90145000000', '90067000000', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90596000000', 'Hidrología', 3, 2, 0, 2, 1, '90269000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90597000000', 'Ecología e Impacto Ambiental', 3, 2, 0, 2, 1, '90049000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90617000000', 'Gestión Estratégica', 4, 3, 0, 2, 1, '', '', '', '', '120 créditos', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('90635000000', 'Laboratorio de Química General', 0, 0, 0, 0, 1, '-----', NULL, NULL, NULL, NULL, 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90655000000', 'Cálculo I', 5, 4, 0, 2, 1, '90663000000', '90668000000', '', '', '', 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('90656000000', 'Cálculo II', 5, 4, 0, 2, 1, '90655000000', '', '', '', '', 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('90661000000', 'Dibujo y Diseño Gráfico', 3, 2, 0, 2, 1, '90663000000', '', '', '', '', 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('90662000000', 'Introduccion a la Ingeniería', 2, 1, 0, 2, 1, '-----', NULL, NULL, NULL, NULL, 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('90663000000', 'Geometría Analítica', 4, 3, 0, 2, 1, '-----', NULL, NULL, NULL, NULL, 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('90664000000', 'Gestión de Recursos de TI', 4, 4, 0, 0, 1, '90137000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('90665000000', 'Fundamentos de Diseño Web', 3, 2, 0, 2, 1, '90662000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('90666000000', 'Inteligencia Artificial y Robótica', 4, 4, 0, 0, 1, '90671000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('90667000000', 'Liderazgo y Oratoria', 2, 1, 0, 2, 1, '', '', '', '', '100 créditos', 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('90668000000', 'Matemática Discreta', 5, 4, 0, 2, 1, '------', NULL, NULL, NULL, NULL, 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('90670000000', 'Planeamiento Estratégico de TI', 4, 4, 0, 0, 1, '90664000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('90671000000', 'Programación I', 5, 3, 0, 4, 1, '90089000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('90673000000', 'Proyecto I', 4, 4, 0, 0, 1, '91121000000', '90548000000', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('90679000000', 'Seguridad y Auditoría de SI', 4, 3, 0, 2, 1, '90721000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('90681000000', 'Gestión de Personal y Legislación laboral', 4, 3, 0, 2, 1, '90683000000', '', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90682000000', 'Taller de Manufactura Moderna', 2, 0, 0, 4, 1, '90140000000', '', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90683000000', 'Psicología Industrial y Organizacional', 2, 1, 0, 2, 1, '', '', '', '', '174 créditos', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90690000000', 'Proyecto Final de Ingeniería Industrial I', 4, 2, 0, 4, 1, '90548000000', '90164000000', '91127000000', '90170000000', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90691000000', 'Proyecto Final de Ingeniería Industrial II', 4, 2, 0, 4, 1, '90156000000', '90690000000', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90694000000', 'Dispositivos Electrónicos', 4, 1, 0, 6, 1, '90037000000', '', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90695000000', 'Matemática Aplicada', 4, 2, 0, 4, 1, '90412000000', '', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90696000000', 'Señales y Sistemas', 4, 2, 0, 4, 1, '90695000000', '', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90697000000', 'Análisis Contable y Financiero', 4, 3, 0, 2, 1, '90090000000', '', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90698000000', 'Proyecto de Ingeniería I', 4, 2, 0, 4, 1, '90128000000', '90158000000', '90213000000', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90699000000', 'Proyecto de Ingeniería II', 4, 2, 0, 4, 1, '90698000000', '', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90700000000', 'Electrónica de Potencia', 4, 2, 0, 4, 1, '90112000000', '90128000000', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('90709000000', 'Realidad Nacional', 3, 3, 0, 0, 1, '------', NULL, NULL, NULL, NULL, 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('90710000000', 'Método de Estudio', 2, 1, 0, 2, 1, '------', NULL, NULL, NULL, NULL, 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('90721000000', 'Diseño e Implementación de Sistemas', 4, 3, 0, 2, 1, '90137000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('90803000000', 'Alimentación y Nutrición Humana', 4, 2, 0, 4, 1, '90804000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90804000000', 'Análisis de Alimentos', 4, 2, 0, 4, 1, '90829000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90805000000', 'Biología General', 4, 2, 0, 4, 1, '90662000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90806000000', 'Bioquímica', 5, 3, 0, 4, 1, '90805000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90808000000', 'Control de Calidad de Alimentos', 4, 2, 0, 4, 1, '90835000000', '90824000000', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90809000000', 'Elemento de Máquinas', 3, 1, 0, 4, 1, '91044000000', '91093000000', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90810000000', 'Envases y Embalajes', 3, 1, 0, 4, 1, '90837000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90811000000', 'Fenómenos de Transporte', 4, 2, 0, 4, 1, '90804000000', '90841000000', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90812000000', 'Física General', 4, 2, 0, 4, 1, '90655000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90813000000', 'Físico Química de alimentos', 4, 2, 0, 4, 1, '90831000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90814000000', 'Gestión Ambiental en la Ind. Alimen. ', 3, 2, 0, 2, 1, '90141000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90817000000', 'Ingeniería de Alimentos I', 4, 2, 0, 4, 1, '90811000000', '90820000000', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90818000000', 'Ingeniería de Alimentos II', 4, 2, 0, 4, 1, '90817000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90820000000', 'Introducción a la Ingeniería de Alimentos', 3, 1, 0, 4, 1, '90813000000', '90824000000', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90821000000', 'Laboratorio de Bioquímica', 0, 0, 0, 0, 1, '90805000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90822000000', 'Laboratorio de Microbiología', 0, 0, 0, 0, 1, '90806000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90823000000', 'Maquinaria para la Industria Alimentaria', 3, 1, 0, 4, 1, '90809000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90824000000', 'Métodos Estadísticos', 4, 3, 0, 2, 1, '91028000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90825000000', 'Microbiología', 4, 2, 0, 4, 1, '90806000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90826000000', 'Microbiología de Alimentos', 4, 2, 0, 4, 1, '90825000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90829000000', 'Química de Alimentos', 4, 2, 0, 4, 1, '90831000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90830000000', 'Química Orgánica', 4, 2, 0, 4, 1, '90037000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90831000000', 'Química Analítica', 4, 2, 0, 4, 1, '90830000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90832000000', 'Ingeniería del Frío', 4, 2, 0, 4, 1, '90837000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90835000000', 'Tecnología de Alimentos I', 4, 2, 0, 4, 1, '90820000000', '90804000000', '90826000000', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90836000000', 'Tecnología de Alimentos II', 4, 2, 0, 4, 1, '90835000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90837000000', 'Tecnologías de Alimentos III', 4, 2, 0, 4, 1, '90818000000', '90836000000', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90841000000', 'Temodinámica', 4, 2, 0, 4, 1, '90813000000', '90829000000', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90847000000', 'Laboratorio de Química Orgánica', 0, 0, 0, 0, 1, '90037000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90849000000', 'Total Quality Management TQM', 4, 2, 0, 4, 1, '90164000000', '', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('90864000000', 'Laboratorio de Física General', 0, 0, 0, 0, 1, '90655000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90865000000', 'Laboratorio de Biología General', 0, 0, 0, 0, 1, '90662000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90866000000', 'Laboratorio de Química Analítica', 0, 0, 0, 0, 1, '90830000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90873000000', 'Taller I', 7, 4, 0, 6, 1, '-----', NULL, NULL, NULL, NULL, 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('90875000000', 'Expresión Arquitectónica I', 3, 1, 0, 4, 1, '-----', NULL, NULL, NULL, NULL, 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('90876000000', 'Taller II', 7, 4, 0, 6, 1, '90873000000', '', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('90879000000', 'Expresión Arquitectónica II', 3, 2, 0, 2, 1, '90875000000', '', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('90883000000', 'Estructuras I', 3, 3, 0, 0, 1, '91312000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90886000000', 'Expresión Arquitectónica III', 3, 2, 0, 2, 1, '90879000000', '', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('90889000000', 'Estructuras II', 3, 3, 0, 0, 1, '90883000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90892000000', 'Expresión Arquitectónica IV', 3, 2, 0, 2, 1, '90886000000', '', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('90893000000', 'Taller V', 8, 4, 0, 8, 1, '91315000000', '90886000000', '90883000000', '91320000000', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('90897000000', 'Taller VI', 8, 4, 0, 8, 1, '90893000000', '', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('90900000000', 'Laboratorio de Medios Digitales', 3, 2, 0, 2, 1, '90892000000', '', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('90901000000', 'Taller VII', 8, 4, 0, 8, 1, '90897000000', '91324000000', '91325000000', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('90904000000', 'Taller VIII', 8, 4, 0, 8, 1, '90901000000', '', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('90905000000', 'Ejercicio Profesional', 3, 2, 0, 2, 1, '91330000000', '', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('90906000000', 'Métodos de Investigación', 4, 3, 0, 2, 1, '-----', '', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('90932000000', 'Gestión de Procesos', 5, 4, 0, 2, 1, '91276000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('90934000000', 'Inteligencia de Negocios', 4, 4, 0, 0, 1, '91288000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('90938000000', 'Instalaciones Eléctricas en Edificaciones', 2, 0, 0, 4, 1, '90049000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90939000000', 'Topografía Avanzada', 2, 1, 0, 2, 1, '91275000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('90943000000', 'Economía Empresarial', 3, 2, 0, 2, 1, '91028000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90949000000', 'Laboratorio de Química de Alimentos', 0, 0, 0, 0, 1, '90831000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('90971000000', 'Inglés I', 1, 0, 0, 2, 1, '------', NULL, NULL, NULL, NULL, 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('90972000000', 'Aerodinámica', 4, 3, 0, 2, 1, '91311000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('90976000000', 'Meteorología', 4, 3, 0, 2, 1, '90972000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('90977000000', 'Operaciones de Piloto Privado', 4, 3, 0, 2, 1, '91311000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('90978000000', 'Sistemas y Componentes', 4, 3, 0, 2, 1, '-----', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('90991000000', 'Operaciones de Piloto Instrumental', 4, 3, 0, 2, 1, '90977000000', '90976000000', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('90994000000', 'Regulaciones Aéreas I', 3, 3, 0, 0, 1, '-----', NULL, NULL, NULL, NULL, 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91028000000', 'Estadística General', 4, 3, 0, 2, 1, '90655000000', '', '', '', '', 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('91029000000', 'Laboratorio de Microbiología de Alimentos', 0, 0, 0, 0, 1, '90825000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('91030000000', 'Laboratorio de Análisis de Alimentos', 0, 0, 0, 0, 1, '90829000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('91037000000', 'Automatización en la Industria Alimentaria ', 3, 1, 0, 4, 1, '90809000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('91038000000', 'Proyectos de Ingeniería en Ind. Aliment. I', 4, 3, 0, 2, 1, '90548000000', '90201000000', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('91039000000', 'Gestión de la Calidad e Inocuidad Alimentaria', 4, 3, 0, 2, 1, '90808000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('91040000000', 'Proyectos de Ingeniería en Ind. Aliment. II', 4, 3, 0, 2, 1, '91038000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('91044000000', 'Electrotécnia', 3, 1, 0, 4, 1, '90812000000', '90656000000', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('91076000000', 'Laboratorio de Electrotécnia', 0, 0, 0, 0, 1, '90812000000', '90656000000', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('91077000000', 'Lab. de Introducción a la Ing. de Alimentos', 0, 0, 0, 0, 1, '90813000000', '90824000000', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('91078000000', 'Lab. de Alimentación y Nutrición Humana', 0, 0, 0, 0, 1, '90804000000', '', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('91093000000', 'Mecánica de Materiales', 4, 2, 0, 4, 1, '90812000000', '90177000000', '', '', '', 'Ingeniería en Industrias Alimentarias', NULL, NULL, NULL, NULL, NULL),
('91114000000', ' Introducción a la Programación', 5, 3, 0, 4, 1, '90668000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('91115000000', 'Tecnología de Información I', 5, 4, 0, 2, 1, '91114000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('91121000000', 'Taller de Proyectos', 5, 0, 0, 10, 1, '90119000000', '90671000000', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('91123000000', 'Laboratorio Introducción a la Programación', 4, 0, 0, 0, 1, '90668000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('91127000000', 'Planeamiento y Cuadro de Mando Integral', 4, 2, 0, 4, 1, '90134000000', '', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('91142000000', 'Ingeniería Eléctrica y Electrónica', 5, 3, 0, 4, 1, '90074000000', '90412000000', '', '', '', 'Ingeniería Electrónica', NULL, NULL, NULL, NULL, NULL),
('91143000000', 'Instrumentación y Control Industrial', 2, 0, 0, 4, 1, '91142000000', '', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('91144000000', 'Mantenimiento, Seguridad y Salud Ocupacional', 4, 2, 0, 4, 1, '90165000000', '90156000000', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('91149000000', 'Tecnología de Información II', 4, 3, 0, 2, 1, '91115000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('91155000000', 'Inglés II', 1, 0, 0, 2, 1, '90971000000', '', '', '', '', 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('91176000000', 'Comportamiento del Consumidor', 4, 4, 0, 0, 1, '90458000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91179000000', 'Gestión de Ventas', 4, 3, 0, 2, 1, '91176000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91181000000', 'Marketing Estratégico en Aviación', 4, 4, 0, 0, 1, '91176000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91274000000', 'Introducción a la Economía', 3, 2, 0, 2, 1, '90709000000', '', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('91275000000', 'Topografía', 3, 1, 0, 4, 1, '----', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('91276000000', 'Sistemas de Información', 3, 2, 0, 2, 1, '90662000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('91277000000', 'Tecnología de los Materiales', 3, 1, 0, 4, 1, '90251000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('91279000000', 'Servidores y Sistemas Operativos', 4, 3, 0, 2, 1, '91149000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('91280000000', 'Resistencia de Materiales', 5, 4, 0, 2, 1, '90087000000', '90086000000', '', '', '', 'Ingeniería Industrial', NULL, NULL, NULL, NULL, NULL),
('91281000000', 'Pavimentos', 5, 3, 0, 4, 1, '90282000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('91288000000', 'Arquitectura Empresarial', 4, 4, 0, 0, 1, '90088000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('91289000000', 'Proyecto Final de Ingeniería Civil I', 4, 4, 0, 0, 1, '90548000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('91290000000', 'Ingeniería Antisísmica', 4, 3, 0, 2, 1, '90295000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('91292000000', 'Proyecto Final de Ingeniería Civil II', 4, 4, 0, 0, 1, '91289000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('91293000000', 'Abastecimiento de Agua y Alcantarillado', 4, 3, 0, 2, 1, '90596000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('91294000000', 'Ingeniería de Valuaciones y Tasaciones', 4, 3, 0, 2, 1, '90145000000', '90595000000', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('91295000000', 'Organización y Dir. de Emp. Constructoras', 4, 3, 0, 2, 1, '90548000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('91310000000', 'Matemática I', 4, 3, 0, 2, 1, '-----', NULL, NULL, NULL, NULL, 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('91311000000', 'Introducción a la Aviación', 3, 3, 0, 0, 1, '-----', NULL, NULL, NULL, NULL, 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91312000000', 'Matemática II', 4, 3, 0, 2, 1, '91310000000', '', '', '', '', 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('91313000000', 'Geometría Descriptiva', 4, 3, 0, 2, 1, '91310000000', '', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('91314000000', 'Física General I', 4, 3, 0, 2, 1, '------', NULL, NULL, NULL, NULL, 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('91315000000', 'Construcción I', 4, 2, 0, 4, 1, '91312000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('91316000000', 'Taller III', 8, 4, 0, 8, 1, '90875000000', '90876000000', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('91317000000', 'Construcción II', 4, 2, 0, 4, 1, '91315000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('91318000000', 'Fotografía', 2, 1, 0, 2, 1, '-----', '', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('91319000000', 'Percepción del Arte y la Arquitectura', 2, 2, 0, 0, 1, '-----', '', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('91320000000', 'Taller IV', 8, 4, 0, 8, 1, '91316000000', '', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('91321000000', 'Física General II', 4, 3, 0, 2, 1, '91314000000', '', '', '', '', 'Departamento Académico', NULL, NULL, NULL, NULL, NULL),
('91322000000', 'Historia de la Arquitectura I', 3, 3, 0, 0, 1, '------', '', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('91324000000', 'Construcción III', 4, 2, 0, 4, 1, '91317000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('91325000000', 'Urbanismo I', 4, 4, 0, 0, 1, '------', '', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('91326000000', 'Historia de la Arquitectura II', 3, 3, 0, 0, 1, '91322000000', '', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('91328000000', 'Instalaciones Sanitarias y Electromecánicas', 4, 4, 0, 0, 1, '91324000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('91329000000', 'Urbanismo II', 4, 4, 0, 0, 1, '91325000000', '', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('91330000000', 'Diseño Bioclimático', 3, 2, 0, 2, 1, '91328000000', '', '', '', '', 'Ingeniería Civil', NULL, NULL, NULL, NULL, NULL),
('91331000000', 'Historia de la Arquitectura III', 3, 3, 0, 0, 1, '91326000000', '', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('91332000000', 'Urbanismo III', 4, 4, 0, 0, 1, '91329000000', '', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('91333000000', 'Historia de la Arquitectura IV', 3, 3, 0, 0, 1, '91331000000', '', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('91334000000', 'Seminario de Construcción', 6, 5, 0, 2, 1, '90906000000', '90905000000', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('91335000000', 'Taller IX', 8, 4, 0, 8, 1, '91330000000', '91332000000', '9904000000', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('91336000000', 'Seminario Urbano', 6, 5, 0, 2, 1, '91332000000', '90906000000', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('91337000000', 'Taller X', 8, 4, 0, 8, 1, '91335000000', '', '', '', '', 'Arquitectura', NULL, NULL, NULL, NULL, NULL),
('91348000000', 'Finanzas', 4, 3, 0, 2, 1, '90122000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91349000000', 'Marketing Digital', 4, 4, 0, 0, 1, '90548000000', '', '', '', '', 'Ingeniería de Computación y Sistemas', NULL, NULL, NULL, NULL, NULL),
('91351000000', 'Mercancías Peligrosas', 2, 2, 0, 0, 1, '90994000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91352000000', 'Motores y Turbinas de Aeronaves', 4, 2, 0, 4, 1, '91314000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91353000000', 'Derecho Empresarial', 4, 3, 0, 2, 1, '91358000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91354000000', 'Negocios Internacionales', 4, 4, 0, 0, 1, '90134000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91355000000', 'Regulaciones Aéreas II', 3, 3, 0, 0, 1, '90994000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91356000000', 'Seguridad Aeronáutica', 3, 3, 0, 0, 1, '90977000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91357000000', 'Operaciones de Piloto Comercial', 4, 2, 0, 4, 1, '90977000000', '90976000000', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91358000000', 'Introducción a la Administración', 4, 4, 0, 0, 1, '--------', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91359000000', 'Estadística y Probabilidades', 4, 3, 0, 2, 1, '91312000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91362000000', 'Administración Logística', 4, 4, 0, 0, 1, '91358000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91367000000', 'Administración de Operaciones', 4, 4, 0, 0, 1, '91362000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91368000000', 'Sistemas de Información Gerencial', 3, 2, 0, 2, 1, '91367000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91369000000', 'Gestión de Recursos Humanos', 4, 3, 0, 2, 1, '91367000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91370000000', 'Proyecto Aeronáutico I', 4, 4, 0, 0, 1, '91357000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91371000000', 'Proyecto Aeronáutico II', 4, 4, 0, 0, 1, '91370000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91381000000', 'Operaciones de Despacho Aéreo', 4, 3, 0, 2, 1, '91355000000', '91357000000', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91382000000', 'Gestión Pública', 4, 4, 0, 0, 1, '91368000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91383000000', 'Performance de Aeronaves', 4, 3, 0, 2, 1, '91314000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91384000000', 'Fisiología de Vuelo', 3, 2, 0, 2, 1, '91311000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91385000000', 'Navegación Doméstica e Internacional', 4, 3, 0, 2, 1, '90977000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91386000000', 'Planeamiento de Carrera y Entrevista', 2, 2, 0, 0, 1, '-------', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91387000000', 'Comportamiento Organizacional en Aviación', 4, 4, 0, 0, 1, '91358000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL),
('91388000000', 'Derecho Aeronáutico', 4, 3, 0, 2, 1, '91353000000', '', '', '', '', 'Ciencias Aeronáuticas', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_alum_seccion`
--

CREATE TABLE `det_alum_seccion` (
  `codDas_alum` varchar(12) NOT NULL,
  `codDas_secc` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_asig_aula`
--

CREATE TABLE `det_asig_aula` (
  `codDaa_aula` varchar(12) NOT NULL,
  `codDaa_seccion` varchar(12) NOT NULL,
  `disponibilidad` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_curso_esc`
--

CREATE TABLE `det_curso_esc` (
  `codDce_curso` varchar(12) NOT NULL,
  `codDce_escuela` varchar(12) NOT NULL,
  `idPlan` varchar(12) NOT NULL,
  `codDce_sem` varchar(12) DEFAULT NULL,
  `ciclo` int(11) DEFAULT NULL,
  `cupos` int(11) DEFAULT NULL,
  `matriculados` int(11) DEFAULT NULL,
  `cat_fia` varchar(45) DEFAULT NULL,
  `tipo_fia` varchar(45) DEFAULT NULL,
  `tipo_sunedu` varchar(45) DEFAULT NULL,
  `tipo_pres_virt` varchar(45) DEFAULT NULL,
  `usu_crea_reg` varchar(45) DEFAULT NULL,
  `fec_crea_reg` datetime DEFAULT NULL,
  `ult_usu_mod_reg` varchar(45) DEFAULT NULL,
  `dec_ult_mod_reg` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `det_curso_esc`
--

INSERT INTO `det_curso_esc` (`codDce_curso`, `codDce_escuela`, `idPlan`, `codDce_sem`, `ciclo`, `cupos`, `matriculados`, `cat_fia`, `tipo_fia`, `tipo_sunedu`, `tipo_pres_virt`, `usu_crea_reg`, `fec_crea_reg`, `ult_usu_mod_reg`, `dec_ult_mod_reg`) VALUES
('90002000000', '101', '1', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90003000000', '101', '1', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90005000000', '101', '1', '4', 1, 45, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90020000000', '101', '1', '4', 2, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90034000000', '101', '1', '4', 10, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90053000000', '101', '1', '4', 3, 50, 0, 'ciencias de la computacion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90054000000', '101', '1', '4', 3, 50, 0, 'metodos cuantitativos', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90056000000', '101', '1', '4', 3, 50, 0, 'matematicas y ciencias', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90060000000', '101', '1', '4', 4, 50, 0, 'metodos cuantitativos', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90069000000', '101', '1', '4', 4, 50, 0, 'ciencias de la computacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90070000000', '101', '1', '4', 10, 50, 0, 'sistemas de informacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90074000000', '101', '1', '4', 4, 50, 0, 'matematicas y ciencias', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90077000000', '101', '1', '4', 4, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90085000000', '101', '1', '4', 6, 50, 0, 'metodos cuantitativos', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90088000000', '101', '1', '4', 6, 50, 0, 'gestion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90089000000', '101', '1', '4', 5, 50, 0, 'ciencias de la computacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90090000000', '101', '1', '4', 5, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90119000000', '101', '1', '4', 6, 50, 0, 'ingenieria de software', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90122000000', '101', '1', '4', 5, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90131000000', '101', '1', '4', 6, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90137000000', '101', '1', '4', 7, 50, 0, 'ingenieria de software', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90145000000', '101', '1', '4', 7, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90366000000', '101', '1', '4', 2, 50, 0, 'matematicas y ciencias', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90548000000', '101', '1', '4', 8, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90655000000', '101', '1', '4', 2, 50, 0, 'matematicas y ciencias', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90662000000', '101', '1', '4', 1, 45, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90663000000', '101', '1', '4', 1, 45, 0, 'matematicas y ciencias', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90664000000', '101', '1', '4', 8, 50, 0, 'sistemas de informacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90665000000', '101', '1', '4', 2, 50, 0, 'ingenieria de software', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90666000000', '101', '1', '4', 7, 50, 0, 'metodos cuantitativos', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90667000000', '101', '1', '4', 9, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90668000000', '101', '1', '4', 1, 45, 0, 'matematicas y ciencias', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90670000000', '101', '1', '4', 9, 50, 0, 'sistemas de informacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90671000000', '101', '1', '4', 6, 50, 0, 'ciencias de la computacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90673000000', '101', '1', '4', 9, 50, 0, 'sistemas de informacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90679000000', '101', '1', '4', 9, 50, 0, 'sistemas de informacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90709000000', '101', '1', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90710000000', '101', '1', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90721000000', '101', '1', '4', 8, 50, 0, 'sistemas de informacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90932000000', '101', '1', '4', 5, 50, 0, 'sistemas de informacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90934000000', '101', '1', '4', 9, 50, 0, 'tecnologia de informacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90971000000', '101', '1', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91114000000', '101', '1', '4', 2, 50, 0, 'ciencias de la computacion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91115000000', '101', '1', '4', 3, 50, 0, 'tecnologia de informacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91121000000', '101', '1', '4', 7, 50, 0, 'ingenieria de software', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91149000000', '101', '1', '4', 4, 50, 0, 'tecnologia de inforamcion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91155000000', '101', '1', '4', 2, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91274000000', '101', '1', '4', 2, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91276000000', '101', '1', '4', 3, 50, 0, 'sistemas de informacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91279000000', '101', '1', '4', 5, 50, 0, 'tecnologia de información', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91288000000', '101', '1', '4', 8, 50, 0, 'tecnologia de informacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91349000000', '101', '1', '4', 10, 50, 0, 'gestion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90002000000', '202', '2', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90003000000', '202', '2', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90005000000', '202', '2', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90020000000', '202', '2', '4', 2, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90034000000', '202', '2', '4', 10, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90053000000', '202', '2', '4', 3, 50, 0, 'ciencias de la computacion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90054000000', '202', '2', '4', 3, 50, 0, 'metodos cuantitativos', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90056000000', '202', '2', '4', 3, 50, 0, 'matematicas y ciencias', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90060000000', '202', '2', '4', 4, 50, 0, 'metodos cuantitativos', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90072000000', '202', '2', '4', 4, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90074000000', '202', '2', '4', 4, 50, 0, 'matematicas y ciencias', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90077000000', '202', '2', '4', 4, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90085000000', '202', '2', '4', 6, 50, 0, 'metodos cuantitativos', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90086000000', '202', '2', '4', 5, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90087000000', '202', '2', '4', 5, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90090000000', '202', '2', '4', 5, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90116000000', '202', '2', '4', 7, 50, 0, 'metodos cuantitativos', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90118000000', '202', '2', '4', 6, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90122000000', '202', '2', '4', 5, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90131000000', '202', '2', '4', 6, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90132000000', '202', '2', '4', 7, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90134000000', '202', '2', '4', 7, 50, 0, 'industrial', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90136000000', '202', '2', '4', 6, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90140000000', '202', '2', '4', 7, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90141000000', '202', '2', '4', 8, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90145000000', '202', '2', '4', 7, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90156000000', '202', '2', '4', 9, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90164000000', '202', '2', '4', 8, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90165000000', '202', '2', '4', 9, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90170000000', '202', '2', '4', 8, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90177000000', '202', '2', '4', 3, 50, 0, 'industrial', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90366000000', '202', '2', '4', 2, 50, 0, 'matematicas y ciencias', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90412000000', '202', '2', '4', 4, 50, 0, 'matematicas y ciencias', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90548000000', '202', '2', '4', 8, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90655000000', '202', '2', '4', 2, 50, 0, 'matematicas y ciencias', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90656000000', '202', '2', '4', 3, 50, 0, 'matematicas y ciencias', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90661000000', '202', '2', '4', 2, 50, 0, 'computacion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90662000000', '202', '2', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90663000000', '202', '2', '4', 1, 50, 0, 'matematicas y ciencias', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90668000000', '202', '2', '4', 1, 50, 0, 'matematicas y ciencias', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90681000000', '202', '2', '4', 10, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90682000000', '202', '2', '4', 8, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90683000000', '202', '2', '4', 9, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90690000000', '202', '2', '4', 9, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90691000000', '202', '2', '4', 10, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90709000000', '202', '2', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90710000000', '202', '2', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90849000000', '202', '2', '4', 9, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90971000000', '202', '2', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91114000000', '202', '2', '4', 2, 50, 0, 'ciencias de la computacion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91127000000', '202', '2', '4', 8, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91142000000', '202', '2', '4', 5, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91143000000', '202', '2', '4', 7, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91144000000', '202', '2', '4', 10, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91155000000', '202', '2', '4', 2, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91274000000', '202', '2', '4', 2, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91280000000', '202', '2', '4', 6, 50, 0, 'industrial', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90002000000', '303', '3', '4', 1, 50, 0, 'generales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90005000000', '303', '3', '4', 1, 50, 0, 'generales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90020000000', '303', '3', '4', 2, 50, 0, 'generales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90667000000', '303', '3', '4', 7, 50, 0, 'generales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90709000000', '303', '3', '4', 1, 50, 0, 'generales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90710000000', '303', '3', '4', 1, 50, 0, 'generales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90873000000', '303', '3', '4', 1, 50, 0, 'diseño y urbanismo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90875000000', '303', '3', '4', 1, 50, 0, 'expresion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90876000000', '303', '3', '4', 2, 50, 0, 'diseño y urbanismo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90879000000', '303', '3', '4', 2, 50, 0, 'expresion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90883000000', '303', '3', '4', 3, 50, 0, 'edificacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90886000000', '303', '3', '4', 3, 50, 0, 'expresion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90889000000', '303', '3', '4', 4, 50, 0, 'edificacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90892000000', '303', '3', '4', 4, 50, 0, 'expresion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90893000000', '303', '3', '4', 5, 50, 0, 'diseño y urbanismo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90897000000', '303', '3', '4', 6, 50, 0, 'diseño y urbanismo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90900000000', '303', '3', '4', 6, 50, 0, 'expresion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90901000000', '303', '3', '4', 7, 50, 0, 'diseño y urbanismo', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90904000000', '303', '3', '4', 8, 50, 0, 'diseño y urbanismo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90905000000', '303', '3', '4', 8, 50, 0, 'edificacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90906000000', '303', '3', '4', 8, 50, 0, 'generales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90971000000', '303', '3', '4', 1, 50, 0, 'generales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91155000000', '303', '3', '4', 2, 50, 0, 'generales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91274000000', '303', '3', '4', 5, 50, 0, 'generales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91275000000', '303', '3', '4', 2, 50, 0, 'edificacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91310000000', '303', '3', '4', 1, 50, 0, 'generales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91312000000', '303', '3', '4', 2, 50, 0, 'generales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91313000000', '303', '3', '4', 2, 50, 0, 'generales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91314000000', '303', '3', '4', 3, 50, 0, 'generales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91315000000', '303', '3', '4', 3, 50, 0, 'edificacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, '0000-00-00 00:00:00'),
('91316000000', '303', '3', '4', 3, 50, 0, 'diseño y urbanismo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91317000000', '303', '3', '4', 4, 50, 0, 'edificacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91318000000', '303', '3', '4', 4, 50, 0, 'expresion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91319000000', '303', '3', '4', 4, 50, 0, 'historia, teoria y critica', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91320000000', '303', '3', '4', 4, 50, 0, 'diseño y urbanismo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91322000000', '303', '3', '4', 5, 50, 0, 'historia, teoria y critica', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91324000000', '303', '3', '4', 5, 50, 0, 'edificacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91325000000', '303', '3', '4', 5, 50, 0, 'diseño y urbanismo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91326000000', '303', '3', '4', 6, 50, 0, 'historia, teoria y critica', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91328000000', '303', '3', '4', 6, 50, 0, 'edificacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91329000000', '303', '3', '4', 6, 50, 0, 'diseño y urbanismo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91330000000', '303', '3', '4', 7, 50, 0, 'edificacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91331000000', '303', '3', '4', 7, 50, 0, 'historia, teoria y critica', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91332000000', '303', '3', '4', 7, 50, 0, 'diseño y urbanismo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91333000000', '303', '3', '4', 8, 50, 0, 'historia, teoria y critica', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91334000000', '303', '3', '4', 9, 50, 0, 'edificacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91335000000', '303', '3', '4', 9, 50, 0, 'diseño y urbanismo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91336000000', '303', '3', '4', 10, 50, 0, 'diseño y urbanismo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91337000000', '303', '3', '4', 10, 50, 0, 'diseño y urbanismo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90002000000', '404', '4', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90003000000', '404', '4', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90005000000', '404', '4', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90020000000', '404', '4', '4', 2, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90034000000', '404', '4', '4', 10, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90037000000', '404', '4', '4', 3, 50, 0, 'cursos instrumentales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90049000000', '404', '4', '4', 4, 50, 0, 'construccion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90054000000', '404', '4', '4', 3, 50, 0, 'cursos instrumentales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90056000000', '404', '4', '4', 3, 50, 0, 'cursos instrumentales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90067000000', '404', '4', '4', 5, 50, 0, 'construccion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90074000000', '404', '4', '4', 4, 50, 0, 'cursos instrumentales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90096000000', '404', '4', '4', 7, 50, 0, 'hidraulica', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90122000000', '404', '4', '4', 5, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90131000000', '404', '4', '4', 6, 50, 0, 'construccion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90145000000', '404', '4', '4', 7, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90251000000', '404', '4', '4', 2, 50, 0, 'transportes', 'obligatorio', 'especificos', 'presencial', NULL, NULL, NULL, NULL),
('90254000000', '404', '4', '4', 4, 50, 0, 'estructuras', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90256000000', '404', '4', '4', 4, 50, 0, 'hidraulica', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90260000000', '404', '4', '4', 5, 50, 0, 'estructuras', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90261000000', '404', '4', '4', 6, 50, 0, 'transportes', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90265000000', '404', '4', '4', 6, 50, 0, 'hidraulica', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90266000000', '404', '4', '4', 6, 50, 0, 'estructuras', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90267000000', '404', '4', '4', 7, 50, 0, 'transportes', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90268000000', '404', '4', '4', 4, 50, 0, 'construccion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90269000000', '404', '4', '4', 7, 50, 0, 'hidraulica', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90271000000', '404', '4', '4', 7, 50, 0, 'estructuras', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90275000000', '404', '4', '4', 8, 50, 0, 'estructuras', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90282000000', '404', '4', '4', 5, 50, 0, 'transportes', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90285000000', '404', '4', '4', 9, 50, 0, 'estructuras', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90286000000', '404', '4', '4', 9, 50, 0, 'estructuras', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90295000000', '404', '4', '4', 8, 50, 0, 'estructuras', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90309000000', '404', '4', '4', 9, 50, 0, 'hidraulica', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90366000000', '404', '4', '4', 2, 50, 0, 'cursos instrumentales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90412000000', '404', '4', '4', 4, 50, 0, 'cursos instrumentales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90548000000', '404', '4', '4', 8, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90595000000', '404', '4', '4', 8, 50, 0, 'construccion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90596000000', '404', '4', '4', 8, 50, 0, 'hidraulica', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90597000000', '404', '4', '4', 5, 50, 0, 'estructuras', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90655000000', '404', '4', '4', 2, 50, 0, 'cursos instrumentales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90656000000', '404', '4', '4', 3, 50, 0, 'cursos instrumentales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90661000000', '404', '4', '4', 2, 50, 0, 'cursos instrumentales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90662000000', '404', '4', '4', 1, 50, 0, 'cursos instrumentales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90663000000', '404', '4', '4', 1, 50, 0, 'cursos instrumentales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90668000000', '404', '4', '4', 1, 50, 0, 'cursos instrumentales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90709000000', '404', '4', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90710000000', '404', '4', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90938000000', '404', '4', '4', 5, 50, 0, 'construccion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90939000000', '404', '4', '4', 3, 50, 0, 'transportes', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90971000000', '404', '4', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91155000000', '404', '4', '4', 2, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91274000000', '404', '4', '4', 2, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91275000000', '404', '4', '4', 2, 50, 0, 'transportes', 'obligatorio', 'especificos', 'presencial', NULL, NULL, NULL, NULL),
('91277000000', '404', '4', '4', 3, 50, 0, 'estructuras', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91281000000', '404', '4', '4', 6, 50, 0, 'transportes', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91289000000', '404', '4', '4', 9, 50, 0, 'hidraulica', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91290000000', '404', '4', '4', 9, 50, 0, 'estructuras', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91292000000', '404', '4', '4', 10, 50, 0, 'humanidades', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91293000000', '404', '4', '4', 10, 50, 0, 'humanidades', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91294000000', '404', '4', '4', 10, 50, 0, 'construccion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91295000000', '404', '4', '4', 10, 50, 0, 'gestion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90002000000', '505', '5', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90003000000', '505', '5', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90005000000', '505', '5', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90020000000', '505', '5', '4', 2, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90034000000', '505', '5', '4', 10, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90037000000', '505', '5', '4', 3, 50, 0, 'matematicas y ciencias basicas', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90053000000', '505', '5', '4', 3, 50, 0, 'computacion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90054000000', '505', '5', '4', 3, 50, 0, 'matematicas y ciencias basicas', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90056000000', '505', '5', '4', 3, 50, 0, 'matematicas y ciencias basicas', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90068000000', '505', '5', '4', 4, 50, 0, 'comunicaciones y redes', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90074000000', '505', '5', '4', 4, 50, 0, 'matematicas y ciencias basicas', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90077000000', '505', '5', '4', 4, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90081000000', '505', '5', '4', 6, 50, 0, 'sistemas de control y automatizacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90082000000', '505', '5', '4', 6, 50, 0, 'comunicaciones y redes', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90083000000', '505', '5', '4', 5, 50, 0, 'comunicaciones y redes', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90090000000', '505', '5', '4', 5, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90091000000', '505', '5', '4', 5, 50, 0, 'matematicas y ciencias basicas', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90109000000', '505', '5', '4', 7, 50, 0, 'sistemas de control y automatizacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90110000000', '505', '5', '4', 5, 50, 0, 'sistemas digitales', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90111000000', '505', '5', '4', 7, 50, 0, 'comunicaciones y redes', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90112000000', '505', '5', '4', 6, 50, 0, 'comunicaciones y redes', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90125000000', '505', '5', '4', 7, 50, 0, 'sistemas electricos y electronicos', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90126000000', '505', '5', '4', 7, 50, 0, 'sistemas de control y automatizacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90127000000', '505', '5', '4', 6, 50, 0, 'sistemas digitales', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90128000000', '505', '5', '4', 8, 50, 0, 'comunicaciones y redes', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90148000000', '505', '5', '4', 7, 50, 0, 'sistemas digitales', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90150000000', '505', '5', '4', 8, 50, 0, 'sistemas electricos y electronicos', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90151000000', '505', '5', '4', 8, 50, 0, 'sistemas de control y automatizacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90158000000', '505', '5', '4', 8, 50, 0, 'sistemas digitales', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90171000000', '505', '5', '4', 9, 50, 0, 'sistemas de control y automatizacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90197000000', '505', '5', '4', 9, 50, 0, 'sistemas electricos y electronicos', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90213000000', '505', '5', '4', 8, 50, 0, 'sistemas de control y automatizacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90366000000', '505', '5', '4', 2, 50, 0, 'matematicas y ciencias basicas', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90412000000', '505', '5', '4', 4, 50, 0, 'matematicas y ciencias basicas', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90655000000', '505', '5', '4', 2, 50, 0, 'matematicas y ciencias basicas', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90656000000', '505', '5', '4', 3, 50, 0, 'matematicas y ciencias basicas', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90661000000', '505', '5', '4', 2, 50, 0, 'computacion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90662000000', '505', '5', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90663000000', '505', '5', '4', 1, 50, 0, 'matematicas y ciencias basicas', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90667000000', '505', '5', '4', 9, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90668000000', '505', '5', '4', 1, 50, 0, 'matematicas y ciencias basicas', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90694000000', '505', '5', '4', 4, 50, 0, 'comunicaciones y redes', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90695000000', '505', '5', '4', 5, 50, 0, 'matematicas y ciencias basicas', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90696000000', '505', '5', '4', 6, 50, 0, 'sistemas de control y automatizacion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90697000000', '505', '5', '4', 10, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90698000000', '505', '5', '4', 9, 50, 0, 'diseño de sistemas digitales', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90699000000', '505', '5', '4', 10, 50, 0, 'diseño de sistemas electronicos', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90700000000', '505', '5', '4', 9, 50, 0, 'comunicaciones y redes', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90709000000', '505', '5', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90710000000', '505', '5', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90971000000', '505', '5', '4', 1, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91114000000', '505', '5', '4', 2, 50, 0, 'computacion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91155000000', '505', '5', '4', 2, 50, 0, 'humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91274000000', '505', '5', '4', 2, 50, 0, 'gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90002000000', '606', '6', '4', 1, 50, 0, 'investigacion y humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90005000000', '606', '6', '4', 1, 50, 0, 'investigacion y humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90020000000', '606', '6', '4', 2, 50, 0, 'investigacion y humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90034000000', '606', '6', '4', 5, 50, 0, 'investigacion y humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90037000000', '606', '6', '4', 1, 50, 0, 'quimica biologia', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90090000000', '606', '6', '4', 5, 50, 0, 'economia y gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90134000000', '606', '6', '4', 6, 50, 0, 'economia y gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90141000000', '606', '6', '4', 6, 50, 0, 'economia y gestion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90177000000', '606', '6', '4', 4, 50, 0, 'matematicas e informatica', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90201000000', '606', '6', '4', 7, 50, 0, 'economia y gestion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90210000000', '606', '6', '4', 9, 50, 0, 'economia y gestion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90548000000', '606', '6', '4', 8, 50, 0, 'economia y gestion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90635000000', '606', '6', '4', 1, 50, 0, 'quimica biologia', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90655000000', '606', '6', '4', 2, 50, 0, 'matematicas e informatica', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90656000000', '606', '6', '4', 3, 50, 0, 'matematicas e informatica', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90661000000', '606', '6', '4', 2, 50, 0, 'matematicas e informatica', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90662000000', '606', '6', '4', 1, 50, 0, 'matematicas e informatica', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90663000000', '606', '6', '4', 1, 50, 0, 'matematicas e informatica', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90668000000', '606', '6', '4', 1, 50, 0, 'matematicas e informatica', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90697000000', '606', '6', '4', 6, 50, 0, 'economia y gestion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90709000000', '606', '6', '4', 1, 50, 0, 'economia y gestion', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90710000000', '606', '6', '4', 1, 50, 0, 'investigacion y humanidades', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90803000000', '606', '6', '4', 7, 50, 0, 'ciencias alimentarias', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90804000000', '606', '6', '4', 5, 50, 0, 'ciencias alimentarias', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90805000000', '606', '6', '4', 2, 50, 0, 'quimica biologia', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90806000000', '606', '6', '4', 3, 50, 0, 'quimica biologia', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90808000000', '606', '6', '4', 8, 50, 0, 'ciencias alimentarias', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90809000000', '606', '6', '4', 7, 50, 0, 'ingenieria de alimentos', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90810000000', '606', '6', '4', 10, 50, 0, 'ingenieria de alimentos', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90811000000', '606', '6', '4', 6, 50, 0, 'ingenieria de alimentos', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90812000000', '606', '6', '4', 3, 50, 0, 'matematicas e informatica', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90813000000', '606', '6', '4', 4, 50, 0, 'ciencias alimentarias', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90814000000', '606', '6', '4', 7, 50, 0, 'economia y gestion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90817000000', '606', '6', '4', 7, 50, 0, 'ingenieria de alimentos', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90818000000', '606', '6', '4', 8, 50, 0, 'ingenieria de alimentos', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90820000000', '606', '6', '4', 6, 50, 0, 'ingenieria de alimentos', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90821000000', '606', '6', '4', 3, 50, 0, 'quimica biologia', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90822000000', '606', '6', '4', 4, 50, 0, 'quimica biologia', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90823000000', '606', '6', '4', 9, 50, 0, 'ingenieria de alimentos', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90824000000', '606', '6', '4', 4, 50, 0, 'matematicas e informatica', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90825000000', '606', '6', '4', 4, 50, 0, 'quimica biologia', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90826000000', '606', '6', '4', 5, 50, 0, 'ciencias alimentarias', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90829000000', '606', '6', '4', 4, 50, 0, 'ciencias alimentarias', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90830000000', '606', '6', '4', 2, 50, 0, 'quimica biologia', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90831000000', '606', '6', '4', 3, 50, 0, 'quimica biologia', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90832000000', '606', '6', '4', 10, 50, 0, 'tecnologia de alimentos', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90835000000', '606', '6', '4', 7, 50, 0, 'tecnologia de alimentos', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90836000000', '606', '6', '4', 8, 50, 0, 'tecnologia de alimentos', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90837000000', '606', '6', '4', 9, 50, 0, 'tecnologia de alimentos', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90841000000', '606', '6', '4', 5, 50, 0, 'ingenieria de alimentos', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90847000000', '606', '6', '4', 2, 50, 0, 'quimica biologia', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90864000000', '606', '6', '4', 3, 50, 0, 'matematicas e informatica', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90865000000', '606', '6', '4', 2, 50, 0, 'quimica biologia', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90866000000', '606', '6', '4', 3, 50, 0, 'quimica biologia', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90943000000', '606', '6', '4', 4, 50, 0, 'economia y gestion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90949000000', '606', '6', '4', 4, 50, 0, 'ciencias alimentarias', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90971000000', '606', '6', '4', 1, 50, 0, 'idiomas', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91028000000', '606', '6', '4', 3, 50, 0, 'investigacion y humanidades', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91029000000', '606', '6', '4', 5, 50, 0, 'ciencias alimentarias', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91030000000', '606', '6', '4', 5, 50, 0, 'ciencias alimentarias', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91037000000', '606', '6', '4', 9, 50, 0, 'ingenieria de alimentos', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91038000000', '606', '6', '4', 9, 50, 0, 'economia y gestion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91039000000', '606', '6', '4', 10, 50, 0, 'ciencias alimentarias', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91040000000', '606', '6', '4', 10, 50, 0, 'economia y gestion', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91044000000', '606', '6', '4', 6, 50, 0, 'ingenieria de alimentos', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91076000000', '606', '6', '4', 6, 50, 0, 'ingenieria de alimentos', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91077000000', '606', '6', '4', 6, 50, 0, 'ingenieria de alimentos', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91078000000', '606', '6', '4', 7, 50, 0, 'ciencias alimentarias', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91093000000', '606', '6', '4', 5, 50, 0, 'ingenieria de alimentos', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91114000000', '606', '6', '4', 2, 50, 0, 'matematicas e informatica', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91123000000', '606', '6', '4', 2, 50, 0, 'matematicas e informatica', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91155000000', '606', '6', '4', 2, 50, 0, 'idiomas', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90002000000', '707', '7', '4', 1, 50, 0, 'estudios generales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90005000000', '707', '7', '4', 1, 50, 0, 'actividades e ingles', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90020000000', '707', '7', '4', 2, 50, 0, 'actividades e ingles', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90034000000', '707', '7', '4', 10, 50, 0, 'ciencias administrativas y economicas', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90122000000', '707', '7', '4', 5, 50, 0, 'ciencias administrativas y economicas', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90134000000', '707', '7', '4', 6, 50, 0, 'ciencias administrativas y economicas', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90458000000', '707', '7', '4', 7, 50, 0, 'ciencias administrativas y economicas', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90617000000', '707', '7', '4', 10, 50, 0, 'ciencias administrativas y economicas', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90667000000', '707', '7', '4', 6, 50, 0, 'ciencias administrativas y economicas', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90709000000', '707', '7', '4', 1, 50, 0, 'estudios generales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90710000000', '707', '7', '4', 1, 50, 0, 'estudios generales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90971000000', '707', '7', '4', 1, 50, 0, 'actividades e ingles', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('90972000000', '707', '7', '4', 2, 50, 0, 'ciencias aeronauticas e instruccion en vuelo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90976000000', '707', '7', '4', 3, 50, 0, 'ciencias aeronauticas e instruccion en vuelo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90977000000', '707', '7', '4', 2, 50, 0, 'ciencias aeronauticas e instruccion en vuelo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90978000000', '707', '7', '4', 3, 50, 0, 'ciencias aeronauticas e instruccion en vuelo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90991000000', '707', '7', '4', 4, 50, 0, 'ciencias aeronauticas e instruccion en vuelo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90994000000', '707', '7', '4', 1, 50, 0, 'ciencias aeronauticas e instruccion en vuelo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91155000000', '707', '7', '4', 2, 50, 0, 'actividades e ingles', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91176000000', '707', '7', '4', 8, 50, 0, 'ciencias administrativas y economicas', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91179000000', '707', '7', '4', 9, 50, 0, 'ciencias administrativas y economicas', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91181000000', '707', '7', '4', 9, 50, 0, 'ciencias administrativas y economicas', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91274000000', '707', '7', '4', 5, 50, 0, 'ciencias administrativas y economicas', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91310000000', '707', '7', '4', 1, 50, 0, 'estudios generales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91311000000', '707', '7', '4', 1, 50, 0, 'gestion aeronautica', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91312000000', '707', '7', '4', 2, 50, 0, 'estudios generales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91314000000', '707', '7', '4', 1, 50, 0, 'estudios generales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91321000000', '707', '7', '4', 2, 50, 0, 'estudios generales', 'obligatorio', 'general', 'presencial', NULL, NULL, NULL, NULL),
('91348000000', '707', '7', '4', 6, 50, 0, 'ciencias administrativas y economicas', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91351000000', '707', '7', '4', 2, 50, 0, 'ciencias aeronauticas e instruccion en vuelo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91352000000', '707', '7', '4', 3, 50, 0, 'ciencias aeronauticas e instruccion en vuelo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91353000000', '707', '7', '4', 6, 50, 0, 'gestion aeronautica', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91354000000', '707', '7', '4', 7, 50, 0, 'ciencias administrativas y economicas', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91355000000', '707', '7', '4', 2, 50, 0, 'ciencias aeronauticas e instruccion en vuelo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91356000000', '707', '7', '4', 3, 50, 0, 'ciencias aeronauticas e instruccion en vuelo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91357000000', '707', '7', '4', 4, 50, 0, 'ciencias aeronauticas e instruccion en vuelo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91358000000', '707', '7', '4', 4, 50, 0, 'ciencias administrativas y economicas', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91359000000', '707', '7', '4', 4, 50, 0, 'ciencias administrativas y economicas', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL);
INSERT INTO `det_curso_esc` (`codDce_curso`, `codDce_escuela`, `idPlan`, `codDce_sem`, `ciclo`, `cupos`, `matriculados`, `cat_fia`, `tipo_fia`, `tipo_sunedu`, `tipo_pres_virt`, `usu_crea_reg`, `fec_crea_reg`, `ult_usu_mod_reg`, `dec_ult_mod_reg`) VALUES
('91362000000', '707', '7', '4', 5, 50, 0, 'ciencias administrativas y economicas', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91367000000', '707', '7', '4', 6, 50, 0, 'ciencias administrativas y economicas', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91368000000', '707', '7', '4', 7, 50, 0, 'ciencias administrativas y economicas', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91369000000', '707', '7', '4', 8, 50, 0, 'ciencias administrativas y economicas', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91370000000', '707', '7', '4', 9, 50, 0, 'gestiona aeronautica', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91371000000', '707', '7', '4', 10, 50, 0, 'gestion aeronautica', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91381000000', '707', '7', '4', 5, 50, 0, 'gestion aeronautica', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91382000000', '707', '7', '4', 8, 50, 0, 'ciencias administrativas y economicas', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91383000000', '707', '7', '4', 3, 50, 0, 'ciencias aeronauticas e instruccion en vuelo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91384000000', '707', '7', '4', 3, 50, 0, 'ciencias aeronauticas e instruccion en vuelo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91385000000', '707', '7', '4', 4, 50, 0, 'ciencias aeronauticas e instruccion en vuelo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91386000000', '707', '7', '4', 4, 50, 0, 'ciencias aeronauticas e instruccion en vuelo', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91387000000', '707', '7', '4', 5, 50, 0, 'ciencias administrativas y economicas', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('91388000000', '707', '7', '4', 7, 50, 0, 'ciencias administrativas y economicas', 'obligatorio', 'especifico', 'presencial', NULL, NULL, NULL, NULL),
('90037000000', '101', '4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('90037000000', '202', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_doc_cur`
--

CREATE TABLE `det_doc_cur` (
  `id` int(11) NOT NULL,
  `id_doc` bigint(12) NOT NULL,
  `id_cur` varchar(12) NOT NULL,
  `descri` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_doc_referido`
--

CREATE TABLE `det_doc_referido` (
  `codDet_ref` varchar(12) NOT NULL,
  `codDet_doc` bigint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_interes_docente`
--

CREATE TABLE `det_interes_docente` (
  `codOi_Inte` int(11) NOT NULL,
  `codOi_doc` bigint(12) NOT NULL,
  `codOi_sem` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_seccion`
--

CREATE TABLE `det_seccion` (
  `codDs_sec` varchar(12) NOT NULL,
  `codDs_doc` int(11) NOT NULL,
  `turno` varchar(45) DEFAULT NULL,
  `actividad` varchar(45) DEFAULT NULL,
  `totMatr` int(11) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_usu_esc`
--

CREATE TABLE `det_usu_esc` (
  `idUsuario` varchar(12) NOT NULL,
  `idEscuela` varchar(12) NOT NULL,
  `idPerfl` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `det_usu_esc`
--

INSERT INTO `det_usu_esc` (`idUsuario`, `idEscuela`, `idPerfl`) VALUES
('COD001', '999', '1'),
('COD002', '101', '2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `disponibilidad_docente`
--

CREATE TABLE `disponibilidad_docente` (
  `id` int(11) NOT NULL,
  `cod_Docente` bigint(12) NOT NULL,
  `cod_semestre` varchar(45) NOT NULL,
  `hor_ini` int(11) DEFAULT NULL,
  `hor_fin` int(11) DEFAULT NULL,
  `dia` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `docente`
--

CREATE TABLE `docente` (
  `id` int(11) NOT NULL,
  `numdoc` bigint(12) NOT NULL,
  `apepat` varchar(45) NOT NULL,
  `apemat` varchar(45) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `pais` varchar(45) DEFAULT NULL,
  `fecini` date NOT NULL,
  `ley30220` tinyint(2) NOT NULL,
  `mayorgrado` varchar(45) NOT NULL,
  `menciongrado` varchar(45) NOT NULL,
  `univ` varchar(100) NOT NULL,
  `paisuniv` varchar(45) NOT NULL,
  `pregrado` varchar(45) DEFAULT NULL,
  `maestria` varchar(45) DEFAULT NULL,
  `doctorado` tinyint(4) DEFAULT NULL,
  `categoria` tinyint(4) NOT NULL,
  `regimen` varchar(45) DEFAULT NULL,
  `horaclase` int(2) NOT NULL,
  `horaactiv` int(2) NOT NULL,
  `totalhoras` int(2) NOT NULL,
  `investigador` tinyint(2) NOT NULL,
  `dina` varchar(255) DEFAULT NULL,
  `codD_sem` varchar(12) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telefono` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





--
-- Volcado de datos para la tabla `docente`
--

INSERT INTO `docente` (`id`, `numdoc`, `apepat`, `apemat`, `nombre`, `pais`, `fecini`, `ley30220`, `mayorgrado`, `menciongrado`, `univ`, `paisuniv`, `pregrado`, `maestria`, `doctorado`, `categoria`, `regimen`, `horaclase`, `horaactiv`, `totalhoras`, `investigador`, `dina`, `codD_sem`, `email`, `telefono`) VALUES
(1, 12345678, 'Abad', 'Escalante', 'Juan Carlos', 'Perú', '2012-02-15', 1, 'Doctor', 'Administración', 'Usmp', 'Perú', 'Si', 'No', 1, 1, 'Tiempo Completo', 16, 4, 20, 1, 'No', '4', NULL, NULL),
(2, 87654321, 'Acosta', 'Acosta', 'Willian Sergio', 'Perú', '2010-03-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(3, 87654322, 'Aguero', 'Martinez La Rosa', 'Julio Cesar', 'Perú', '2010-03-17', 0, 'Titulo', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 0, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(4, 87654320, 'Acuña', 'Flores', 'Carlos Christian', 'Perú', '2015-04-16', 1, 'Maestro', 'Bachiller en Biología', 'Unmsm', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Completo', 10, 2, 12, 1, 'No', '4', NULL, NULL),
(5, 87654323, 'Aguirre', 'Medrano', 'Rosa Virginia', 'Perú', '2009-03-17', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(6, 87654324, 'Alarco', 'Jerí', 'Iván Erick', 'Perú', '2010-04-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Usmp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(7, 87654325, 'Alarcon', 'Centti', 'Jose', 'Perú', '2010-05-16', 1, 'Doctor', 'Bachiller en Economia', 'Usmp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(8, 87654326, 'Alegria', 'Vidal', 'Rosa Mercedes', 'Perú', '2010-06-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(9, 87654327, 'Alferrano', 'Donofrio', 'Mirtha', 'Perú', '2010-07-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Utp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(10, 87654328, 'Ambia', 'Rebatta', 'Hugo Paulino', 'Perú', '2010-08-16', 1, 'Doctor', 'Bachiller en Derecho', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(11, 87654329, 'Amoros', 'Figueroa', 'Rodrigo', 'Perú', '2010-09-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Unmsm', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(12, 87654311, 'Añaños', 'Gomez Sanchez', 'Roberto Enrique', 'Perú', '2010-10-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(13, 87654331, 'Ara', 'Rojas', 'Silvia Liliana', 'Perú', '2010-11-16', 1, 'Doctor', 'Bachiller en Educacion', 'Utp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(14, 87654341, 'Arrieta', 'Freyre', 'Javier Eduardo', 'Perú', '2010-12-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(15, 87654351, 'Arrieta', 'Taboada', 'Amanda', 'Perú', '2010-03-01', 1, 'Doctor', 'Bachiller en Ciencias', 'Uni', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(16, 87654361, 'Arriola', 'Guevara', 'Luis Alberto', 'Perú', '2010-03-02', 1, 'Doctor', 'Bachiller en Ciencias', 'Uni', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(17, 87654371, 'Bacigalupo', 'Olivari', 'Miguel Angel', 'Perú', '2010-03-03', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(18, 87654381, 'Balcazar', 'Hernandez', 'Yudy', 'Perú', '2010-03-04', 1, 'Doctor', 'Bachiller en Ciencias', 'Usmp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(19, 87654391, 'Ballena', 'Gonzales', 'Manuel', 'Perú', '2010-03-05', 1, 'Doctor', 'Bachiller en Ciencias', 'Usmp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(20, 87654301, 'Balta', 'Rospliglosi', 'Manuel Valeriano', 'Perú', '2010-03-06', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(21, 87654121, 'Balta', 'Vargas-Machuca', 'Manuel', 'Perú', '2010-03-07', 1, 'Doctor', 'Bachiller en Ciencias', 'Usmp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(22, 87654221, 'Barnet', 'Champommier', 'Yann Oliver', 'Francia', '2010-03-08', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(23, 87654421, 'Barnett', 'Mendoza', 'Edy Dalmiro', 'Perú', '2010-03-09', 1, 'Doctor', 'Bachiller en Ciencias', 'Usmp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(24, 87654521, 'Barrantes', 'Mann', 'Luis Alfonso', 'Perú', '2010-03-10', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(25, 87654621, 'Barraza', 'Salguero', 'Victor Eduardo', 'Perú', '2010-03-11', 1, 'Doctor', 'Bachiller en Ciencias', 'Utp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(26, 87654721, 'Barreto', 'Bardales', 'Tomas Fernando', 'Perú', '2010-03-12', 1, 'Doctor', 'Bachiller en Ciencias', 'ULima', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(27, 87654821, 'Basile', 'Migliore', 'Jose Antonio', 'Perú', '2010-03-20', 1, 'Doctor', 'Bachiller en Ciencias', 'Uni', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(28, 87654921, 'Bayona', 'Ore', 'Luz Sussy', 'Perú', '2010-03-21', 1, 'Doctor', 'Bachiller en Ciencias', 'Uni', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(29, 87654021, 'Becerra', 'Pacherres', 'Augusto Oscar', 'Perú', '2010-03-22', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(30, 87651321, 'Bedia', 'Guillen', 'Ciro Sergio', 'Perú', '2010-03-23', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(31, 87652321, 'Benavente', 'Villena', 'Maria Elena', 'Perú', '2010-03-24', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(32, 87653321, 'Benites', 'Gonzales', 'Willian Sergio', 'Perú', '2010-03-25', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(33, 87655321, 'Benites', 'Vilela', 'Luis Fernando', 'Perú', '2010-03-26', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(34, 87656321, 'Bernabel', 'Liza', 'Ana Maria', 'Perú', '2010-03-27', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(35, 87657321, 'Bernal', 'Ortiz', 'Carlos Adolfo', 'Perú', '2010-03-28', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(36, 87658321, 'Bernuy', 'Alva', 'Augusto Ernesto', 'Perú', '2010-03-29', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(37, 87659321, 'Bertolotti', 'Zuñiga', 'Carmen Rosa', 'Perú', '2010-03-30', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(38, 87650321, 'Bezada', 'Sanchez', 'Cesar Alfredo', 'Perú', '2011-03-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(39, 87614321, 'Blanco', 'Zuñiga', 'Yvan', 'Perú', '2012-03-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Unmsm', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(40, 87624321, 'Bocangel', 'Weydert', 'Guillermo Augusto', 'Perú', '2013-03-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(41, 87634321, 'Buendia', 'Rios', 'Hildebrando', 'Perú', '2014-03-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(42, 87644321, 'Bustos', 'Diaz', 'Silverio', 'Perú', '2015-03-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(43, 87664321, 'Cabrera', 'Iturria', 'Ricardo', 'Perú', '2016-03-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Utp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(44, 87674321, 'Caceres', 'Echegaray', 'Hector Alejandro', 'Perú', '2009-03-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(45, 87684321, 'Caceres', 'Lampen', 'Manuel Alejandro', 'Perú', '2008-03-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(46, 87694321, 'Calderon', 'Caceres', 'Jose Luis', 'Perú', '2007-03-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(47, 87604321, 'Campos', 'Perez', 'Rosalvina', 'Perú', '2006-03-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Usmp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(48, 87154321, 'Cano', 'Tejada', 'Jose Antonio', 'Perú', '2005-03-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(49, 87254321, 'Caparachin', 'Chuquihuaraca', 'Jaime Santos', 'Perú', '2004-03-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(50, 87354321, 'Cardenas', 'Lucero', 'Luis', 'Perú', '2003-03-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Uni', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(51, 87454321, 'Cardenas', 'Martinez', 'Jose Antonio', 'Perú', '2002-03-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(52, 87554321, 'Cardenas', 'Zavala', 'Germain Leonardo', 'Perú', '2001-03-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(53, 87754321, 'Carpio', 'Delgado', 'Guitter Guillermo', 'Perú', '2000-03-16', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(54, 87854321, 'Casavilca', 'Maldonado', 'Edmundo', 'Perú', '2010-04-01', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(55, 87954321, 'Castillo', 'Cavero', 'Rodolfo', 'Perú', '2010-04-02', 1, 'Doctor', 'Bachiller en Ciencias', 'ULima', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(56, 87054321, 'Castillo', 'Sini', 'Gustavo Alejandro', 'Perú', '2010-04-03', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(57, 81654321, 'Castro', 'Salazar', 'Fredy Adan', 'Perú', '2010-04-04', 1, 'Doctor', 'Bachiller en Ciencias', 'Usmp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(58, 82654321, 'Cataño', 'Espinoza', 'Humberto Daniel', 'Perú', '2010-04-05', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(59, 83654321, 'Ccoyure', 'Tito', 'Ricardo Wilber', 'Perú', '2010-04-06', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(60, 84654321, 'Celi', 'Saavedra', 'Luis', 'Perú', '2010-04-07', 1, 'Doctor', 'Bachiller en Ciencias', 'Usmp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(61, 85654321, 'Cerdan', 'Chavarri', 'Mario Wilbe', 'Perú', '2010-04-08', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(62, 86654321, 'Cerpa', 'Espinosa', 'David', 'Perú', '2010-04-09', 1, 'Doctor', 'Bachiller en Ciencias', 'Ucv', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(63, 88654321, 'Cerron', 'Ruiz', 'Eulogio', 'Perú', '2010-04-10', 1, 'Doctor', 'Bachiller en Ciencias', 'Uap', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(64, 89654321, 'Cevallos', 'Echevarria', 'Alejandro Nestor', 'Perú', '2010-04-11', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(65, 80654321, 'Chacon', 'Moscoso', 'Hugo Liu', 'Perú', '2010-04-12', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(66, 17654321, 'Chanca', 'Cortez', 'Luis Manuel', 'Perú', '2010-04-13', 1, 'Doctor', 'Bachiller en Ciencias', 'Utp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(67, 27654321, 'Chavarry', 'Vallejos', 'Carlos Magno', 'Perú', '2010-04-14', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(68, 37654321, 'Chavez', 'Rodriguez', 'Michael Jesus', 'Perú', '2010-04-15', 1, 'Doctor', 'Bachiller en Ciencias', 'Usmp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(69, 47654321, 'Cieza', 'Davila', 'Javier Eduardo', 'Perú', '2010-04-17', 1, 'Doctor', 'Bachiller en Ciencias', 'Usmp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(70, 57654321, 'Consigliere', 'Cevasco', 'Luis Ricardo', 'Perú', '2010-04-18', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(71, 67654321, 'Contreras', 'Villarreal', 'Luis Wilfredo', 'Perú', '2010-04-19', 1, 'Doctor', 'Bachiller en Ciencias', 'ULima', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(72, 77654321, 'Cortez', 'Silva', 'Dimas Antonio', 'Perú', '2010-04-20', 1, 'Doctor', 'Bachiller en Ciencias', 'Unmsm', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(73, 97654321, 'Cuadros', 'Ricra', 'Ruben Dario', 'Perú', '2010-04-21', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(74, 7654321, 'De La Vega', 'Picoaga', 'Fresia', 'Perú', '2010-04-22', 1, 'Doctor', 'Bachiller en Ciencias', 'Usmp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(75, 84654321, 'De La Torre', 'Puente', 'Maria Elena', 'Perú', '2010-04-23', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(76, 87954321, 'De Los Rios', 'Hermoza', 'Justo Rafael', 'Perú', '2010-04-24', 1, 'Doctor', 'Bachiller en Ciencias', 'Utp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(77, 81554321, 'De Olarte', 'Tristan', 'Jorge Luis', 'Perú', '2010-04-25', 1, 'Doctor', 'Bachiller en Ciencias', 'Usmp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(78, 82154321, 'Del Carpio', 'Damian', 'Christian Carlos', 'Perú', '2010-04-26', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(79, 82254321, 'Diaz', 'Tejada', 'Gabriel Eduardo', 'Perú', '2010-04-27', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(80, 82354321, 'Durán', 'Ramirez', 'Gary Gary', 'Perú', '2010-04-28', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(81, 82454321, 'Egoavil', 'La Torre', 'Victor Raul', 'Perú', '2010-04-29', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(82, 82554321, 'Elaez', 'Cisneros', 'Eliazaf Guillermo', 'Perú', '2010-04-30', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(83, 82654321, 'Enriquez', 'Quinde', 'Benjamin Carlos', 'Perú', '2010-05-01', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(84, 82754321, 'Esparza', 'Castillo', 'Olenka', 'Perú', '2010-05-02', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(85, 82854321, 'Estela', 'Benavides', 'Bertha', 'Perú', '2010-05-03', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(86, 82954321, 'Falcon', 'Soto', 'Arnaldo', 'Perú', '2010-05-04', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(87, 83054321, 'Fano', 'Miranda', 'Gonzalo', 'Perú', '2010-05-05', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(88, 83154321, 'Figueroa', 'Lezama', 'Rafael', 'Perú', '2010-05-06', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(89, 83254321, 'Figueroa', 'Revilla', 'Jorge Martin', 'Perú', '2010-05-07', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(90, 83354321, 'Florian', 'Castillo', 'Tulio Elias', 'Perú', '2010-05-08', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(91, 83454321, 'Fuentes', 'Flores', 'Maximo', 'Perú', '2010-05-09', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(92, 83554321, 'Galindo', 'Guerra', 'Gary Joseph Loui', 'Perú', '2010-05-10', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(93, 83654321, 'Gallegos', 'Valderrama', 'Meriedi', 'Perú', '2010-05-11', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(94, 83754321, 'Galloso', 'Gentile', 'Alberto Cesar', 'Perú', '2010-05-12', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(95, 83854321, 'Gamarra', 'Villacorta', 'Raul', 'Perú', '2010-05-13', 1, 'Doctor', 'Bachiller en Ciencias', 'Usmp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(96, 83954321, 'Gamboa', 'Cruzado', 'Javier Arturo', 'Perú', '2010-05-14', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(97, 84054321, 'Gamero', 'Rengifo', 'Luis Fernando', 'Perú', '2010-05-18', 1, 'Doctor', 'Bachiller en Ciencias', 'Usmp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(98, 84154321, 'Garcia', 'Farje', 'Ruben Osvaldo', 'Perú', '2010-05-17', 1, 'Doctor', 'Bachiller en Ciencias', 'ULima', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(99, 84254321, 'Garcia', 'Flores', 'Elva Nelly', 'Perú', '2010-05-19', 1, 'Doctor', 'Bachiller en Ciencias', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(100, 84354321, 'Garcia', 'Guerra', 'Juan Diego', 'Perú', '2010-05-20', 1, 'Doctor', 'Bachiller en Ciencias', 'Utp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(101, 84454321, 'Garcia', 'Lorente', 'Cesar', 'España', '2010-05-21', 1, 'Doctor', 'Bachiller en Ciencias', 'Usmp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(102, 84554321, 'Garcia', 'Nuñez', 'Luz Elena', 'Perú', '2010-05-22', 1, 'Doctor', 'Bachiller en Ciencias', 'Unmsm', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(103, 12365479, 'Garcia', 'Sanchez', 'Marta Pamela', 'Perú', '2008-03-13', 1, 'Maestría', 'Bachiller en Ciencias', 'Upn', 'Perú', 'No', 'Si', 1, 1, 'Tiempo Completo', 15, 8, 23, 1, 'No', '4', NULL, NULL),
(104, 88888888, 'Geronimo', 'Vasquez', 'Alfonso Herminio', 'Perú', '2010-03-15', 1, 'Doctor', 'Bachiller en Arquitectura', 'Usil', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 15, 5, 20, 1, 'No', '4', NULL, NULL),
(105, 11111111, 'Gomero', 'Cordova', 'Eduardo Fernando', 'Perú', '2003-03-14', 0, 'Título', 'Bachiller en Economía', 'Ucv', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(106, 34532689, 'Gonzales', 'Alva', 'Luis Eduardo', 'Perú', '2010-03-13', 1, 'Maestría', 'Bachiller en Educación', 'Upn', 'Perú', 'Si', 'Si', 0, 1, 'Tiempo Completo', 14, 5, 19, 0, 'No', '4', NULL, NULL),
(107, 99999999, 'Gonzales', 'Chavesta', 'Celso', 'Perú', '1999-03-16', 1, 'Maestro', 'Bachiller en Computación y Sistemas', 'Uap', 'Perú', 'No', 'Si', 1, 0, 'Tiempo Parcial', 13, 5, 18, 1, 'No', '4', NULL, NULL),
(108, 17893492, 'Gonzales', 'Sanchez', 'Juan Julio', 'Perú', '2007-03-10', 1, 'Título', 'Bachiller en Derecho', 'Usil', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Completo', 12, 8, 20, 1, 'No', '4', NULL, NULL),
(109, 9431874, 'Gonzalo', 'Silva', 'Eduardo Ramon', 'Perú', '2004-03-20', 1, 'Doctor', 'Bachiller en Ciencias Matemáticas', 'Uigv', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 15, 6, 21, 1, 'No', '4', NULL, NULL),
(110, 74073297, 'Grandez', 'Pizarro', 'Waldy Mercedes', 'Perú', '2012-03-23', 1, 'Título', 'Bachiller en Economía', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(111, 23148930, 'Guerrero', 'Navarro', 'Raul Agustin', 'Perú', '2005-03-13', 1, 'Doctor', 'Bachiller en Ciencias Contables', 'Usmp', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Completo', 15, 6, 21, 1, 'No', '4', NULL, NULL),
(112, 83704341, 'Guzmán', 'Tasayco', 'Alfonso', 'Perú', '2010-03-18', 1, 'Maestro', 'Bachiller en Derecho', 'Upc', 'Perú', 'Si', 'Si', 1, 1, 'Tiempo Parcial', 12, 6, 18, 1, 'No', '4', NULL, NULL),
(113, 1010101, 'Guzman', 'Rouviros', 'Julio Alejandro', 'Perú', '2004-03-12', 1, 'Doctor', 'Bachiller en Ciencias', 'Utp', 'Perú', 'Si', 'No', 1, 0, 'Tiempo Parcial', 13, 6, 19, 1, 'No', '4', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo`
--

CREATE TABLE `equipo` (
  `idequipo` varchar(12) NOT NULL,
  `codE_aula` varchar(12) NOT NULL,
  `modelo` varchar(45) DEFAULT NULL,
  `decripcion` varchar(45) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escuela`
--

CREATE TABLE `escuela` (
  `idescuela` varchar(12) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `facultad` varchar(45) DEFAULT NULL,
  `usu_crea_reg` varchar(45) DEFAULT NULL,
  `fec_crea_reg` datetime DEFAULT NULL,
  `ult_usu_mod_reg` varchar(45) DEFAULT NULL,
  `fec_ult_mod_reg` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `escuela`
--

INSERT INTO `escuela` (`idescuela`, `descripcion`, `facultad`, `usu_crea_reg`, `fec_crea_reg`, `ult_usu_mod_reg`, `fec_ult_mod_reg`) VALUES
('101', 'Ingenieria de Computacion y Sistemas', 'FIA', NULL, NULL, NULL, NULL),
('202', 'Ingenieria Industrial', 'FIA', NULL, NULL, NULL, NULL),
('303', 'Arquitectura', 'FIA', NULL, NULL, NULL, NULL),
('404', 'Ingeniería Civil', 'FIA', NULL, NULL, NULL, NULL),
('505', 'Ingeniería Electrónica', 'FIA', NULL, NULL, NULL, NULL),
('606', 'Ingeniería en Industrias Alimentarias', 'FIA', NULL, NULL, NULL, NULL),
('707', 'Ciencias Aeronáuticas', 'FIA', NULL, NULL, NULL, NULL),
('999', 'Admin', 'Admin', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario`
--

CREATE TABLE `horario` (
  `idHorario` int(11) NOT NULL,
  `codH_sem` varchar(12) DEFAULT NULL,
  `codH_curso` varchar(12) DEFAULT NULL,
  `codH_doc` int(11) DEFAULT NULL,
  `codH_secc` varchar(12) DEFAULT NULL,
  `codH_aula` varchar(12) DEFAULT NULL,
  `dia` varchar(12) DEFAULT NULL,
  `hora_ini` int(11) DEFAULT NULL,
  `hora_fiN` int(11) DEFAULT NULL,
  `cod_esc` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `horario`
--

INSERT INTO `horario` (`idHorario`, `codH_sem`, `codH_curso`, `codH_doc`, `codH_secc`, `codH_aula`, `dia`, `hora_ini`, `hora_fiN`, `cod_esc`) VALUES
(1, '20171', '90002000000', 1, '50A', '13', 'LUNES', 800, 930, '101'),
(2, '20171', '90003000000', 1, '51A', '14', 'LUNES', 800, 910, '101'),
(3, '20171', '90002000000', 1, '52A', '15', 'MIERCOLES', 800, 845, '101'),
(4, '20171', '90034000000', 2, '53A', '1', 'MARTES', 800, 845, '101'),
(5, '20171', '90060000000', 3, '58A', '13', 'MARTES', 800, 930, '101'),
(6, '20171', '90037000000', 6, '55A', '14', 'MARTES', 800, 1015, '202'),
(7, '20171', '90049000000', 7, '56A', '15', 'LUNES', 800, 1015, '404'),
(8, '20171', '90053000000', 8, '57A', '15', 'LUNES', 845, 1100, '101'),
(9, '20171', '90054000000', 2, '54A', '13', 'MARTES', 845, 1015, '101'),
(10, '20171', '90056000000', 10, '59A', '14', 'LUNES', 930, 1100, NULL),
(11, '20171', '90060000000', 11, '60B', '13', 'MIERCOLES', 800, 930, NULL),
(12, '20171', '90067000000', 12, '61B', '13', 'MIERCOLES', 930, 1100, NULL),
(13, '20171', '90068000000', 13, '62B', '20', 'LUNES', 800, 845, NULL),
(14, '20171', '90069000000', 14, '63B', '14', 'MIERCOLES', 800, 845, NULL),
(15, '20171', '90070000000', 15, '64B', '20', 'LUNES', 845, 1015, NULL),
(16, '20171', '90072000000', 16, '65B', '20', 'LUNES', 1015, 1230, NULL),
(17, '20171', '90074000000', 17, '66B', '14', 'MIERCOLES', 845, 1015, NULL),
(18, '20171', '90077000000', 18, '67B', '20', 'MARTES', 800, 845, NULL),
(19, '20171', '90081000000', 19, '68B', '22', 'LUNES', 800, 845, NULL),
(20, '20171', '90082000000', 20, '69B', '22', 'LUNES', 845, 1015, NULL),
(21, '20171', '90083000000', 21, '70C', '21', 'LUNES', 800, 1015, NULL),
(22, '20171', '90085000000', 22, '71C', '22', 'MARTES', 800, 930, NULL),
(23, '20171', '90086000000', 23, '72C', '13', 'JUEVES', 800, 930, NULL),
(24, '20171', '90087000000', 24, '73C', '21', 'LUNES', 1015, 1230, NULL),
(25, '20171', '90088000000', 25, '74C', '10', 'JUEVES', 800, 1015, NULL),
(26, '20171', '90089000000', 26, '75C', '13', 'JUEVES', 800, 930, NULL),
(27, '20171', '90090000000', 27, '76C', '21', 'MIERCOLES', 800, 1015, NULL),
(28, '20171', '90091000000', 28, '77C', '22', 'MIERCOLES', 800, 930, NULL),
(29, '20171', '90096000000', 29, '78C', '13', 'VIERNES', 800, 1015, NULL),
(30, '20171', '90109000000', 30, '79C', '13', 'VIERNES', 1015, 1230, NULL),
(31, '20171', '90110000000', 31, '80D', '10', 'VIERNES', 800, 930, NULL),
(32, '20171', '90111000000', 32, '81D', '21', 'JUEVES', 800, 1100, NULL),
(33, '20171', '90112000000', 33, '82D', '12', 'JUEVES', 800, 1015, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `intereses`
--

CREATE TABLE `intereses` (
  `idIntereses` int(11) NOT NULL,
  `nombreInteres` varchar(45) NOT NULL,
  `hab_nohab` tinyint(1) DEFAULT NULL,
  `nombre_archivo` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pabellon`
--

CREATE TABLE `pabellon` (
  `idpabellon` varchar(12) NOT NULL,
  `numero` int(11) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pabellon`
--

INSERT INTO `pabellon` (`idpabellon`, `numero`, `descripcion`) VALUES
('1', NULL, 'Generales'),
('2', NULL, 'Especialidades'),
('3', NULL, 'Laboratorio'),
('4', NULL, 'Biblioteca'),
('5', NULL, 'Fia data');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parametros`
--

CREATE TABLE `parametros` (
  `codP_esc` varchar(12) NOT NULL,
  `codP_sem` varchar(12) NOT NULL,
  `ciclo` int(11) NOT NULL,
  `creditos` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil`
--

CREATE TABLE `perfil` (
  `idperfil` varchar(12) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `permisos` varchar(45) DEFAULT NULL,
  `descrip` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `perfil`
--

INSERT INTO `perfil` (`idperfil`, `nombre`, `permisos`, `descrip`) VALUES
('1', 'administrador', 'Admin', NULL),
('2', 'director', 'Director', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plan_curricular`
--

CREATE TABLE `plan_curricular` (
  `idPlan` varchar(45) NOT NULL,
  `idEscuela` varchar(12) DEFAULT NULL,
  `descripcion` varchar(700) DEFAULT NULL,
  `codPc_sem` varchar(12) NOT NULL,
  `estado` int(1) NOT NULL,
  `usu_crea_reg` varchar(45) DEFAULT NULL,
  `fec_crea_reg` datetime DEFAULT NULL,
  `ult_usu_mod_reg` varchar(45) DEFAULT NULL,
  `fec_ult_mod_reg` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `plan_curricular`
--

INSERT INTO `plan_curricular` (`idPlan`, `idEscuela`, `descripcion`, `codPc_sem`, `estado`, `usu_crea_reg`, `fec_crea_reg`, `ult_usu_mod_reg`, `fec_ult_mod_reg`) VALUES
('1', NULL, 'Ingeniería de Computación y Sistemas', '4', 0, NULL, NULL, NULL, NULL),
('2', NULL, 'Ingeniería Industrial', '4', 0, NULL, NULL, NULL, NULL),
('3', NULL, 'Arquitectura', '4', 0, NULL, NULL, NULL, NULL),
('4', NULL, 'Ingeniería Civil', '4', 0, NULL, NULL, NULL, NULL),
('5', NULL, 'Ingeniería Electrónica', '4', 0, NULL, NULL, NULL, NULL),
('6', NULL, 'Ingeniería en Industrias Alimentarias', '4', 0, NULL, NULL, NULL, NULL),
('7', NULL, 'Ciencias Aeronáuticas', '4', 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `referido`
--

CREATE TABLE `referido` (
  `idreferido` varchar(12) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `ape_Pat` varchar(45) DEFAULT NULL,
  `ape_Mat` varchar(45) DEFAULT NULL,
  `num_doc` varchar(16) DEFAULT NULL,
  `edad` int(3) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `especialidad` varchar(45) DEFAULT NULL,
  `titulos` varchar(45) DEFAULT NULL,
  `disponibilidad` tinyint(4) DEFAULT NULL,
  `univ_logro_acad` varchar(150) DEFAULT NULL,
  `logro_acad_insti` varchar(150) DEFAULT NULL,
  `logro_diplo` varchar(45) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `max_grad_alcanzado` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seccion`
--

CREATE TABLE `seccion` (
  `idseccion` varchar(16) NOT NULL,
  `descrip` varchar(45) DEFAULT NULL,
  `cuposTot` int(11) DEFAULT NULL,
  `modalidades` varchar(45) DEFAULT NULL,
  `codS_cur` varchar(12) DEFAULT NULL,
  `codS_sem` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `seccion`
--

INSERT INTO `seccion` (`idseccion`, `descrip`, `cuposTot`, `modalidades`, `codS_cur`, `codS_sem`) VALUES
('50A', 'Seccion_A', 12, 'Mod1', '90002000000', '1'),
('51A', 'Seccion_51A', 12, 'Mod2', '90002000000', '1'),
('52A', 'Seccion52A', 12, 'Mod4', NULL, NULL),
('53A', 'Seccion53A', 10, 'Mod3', '90034000000', '1'),
('54A', 'Seccion54A', 12, 'Mod5', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `semestre`
--

CREATE TABLE `semestre` (
  `idSemestre` varchar(12) NOT NULL,
  `ano` year(4) DEFAULT NULL,
  `semestre` int(11) DEFAULT NULL,
  `cant_sema` int(11) DEFAULT NULL,
  `estado` int(1) NOT NULL,
  `usu_crea_reg` varchar(45) DEFAULT NULL,
  `fec_crea_reg` datetime DEFAULT NULL,
  `ult_usu_mod_reg` varchar(45) DEFAULT NULL,
  `fec_ult_mod_reg` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `semestre`
--

INSERT INTO `semestre` (`idSemestre`, `ano`, `semestre`, `cant_sema`, `estado`, `usu_crea_reg`, `fec_crea_reg`, `ult_usu_mod_reg`, `fec_ult_mod_reg`) VALUES
('1', 2016, 0, 4, 0, NULL, NULL, NULL, NULL),
('2', 2016, 1, 16, 0, NULL, NULL, NULL, NULL),
('3', 2016, 2, 16, 0, NULL, NULL, NULL, NULL),
('4', 2017, 1, 16, 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` varchar(12) NOT NULL,
  `nombreUsu` varchar(45) DEFAULT NULL,
  `Password` varchar(45) DEFAULT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `ape_Pat` varchar(45) DEFAULT NULL,
  `ape_Mat` varchar(45) DEFAULT NULL,
  `correoE` varchar(45) DEFAULT NULL,
  `fecha_actualizacion` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `nombreUsu`, `Password`, `nombre`, `ape_Pat`, `ape_Mat`, `correoE`, `fecha_actualizacion`) VALUES
('COD001', '12345678', '123', 'Juan Carlos', 'Abad', 'Escalante', 'sj', '2017-06-06'),
('COD002', '87654321', '321', 'Willian Sergio', 'Acosta', 'Acosta', NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD PRIMARY KEY (`idalumno`);

--
-- Indices de la tabla `aula`
--
ALTER TABLE `aula`
  ADD PRIMARY KEY (`idaula`),
  ADD KEY `idA_pab_idx` (`codA_pab`);

--
-- Indices de la tabla `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`idcurso`);

--
-- Indices de la tabla `det_alum_seccion`
--
ALTER TABLE `det_alum_seccion`
  ADD KEY `idDas_alum_idx` (`codDas_alum`),
  ADD KEY `idDas_secc_idx` (`codDas_secc`);

--
-- Indices de la tabla `det_asig_aula`
--
ALTER TABLE `det_asig_aula`
  ADD KEY `idDaa_aula_idx` (`codDaa_aula`),
  ADD KEY `idDaa_secc_idx` (`codDaa_seccion`);

--
-- Indices de la tabla `det_curso_esc`
--
ALTER TABLE `det_curso_esc`
  ADD KEY `idDce_cur_idx` (`codDce_curso`),
  ADD KEY `idDce_esc_idx` (`codDce_escuela`),
  ADD KEY `idSem_idx` (`codDce_sem`),
  ADD KEY `idpla` (`idPlan`);

--
-- Indices de la tabla `det_doc_cur`
--
ALTER TABLE `det_doc_cur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_doc_idx` (`id_doc`),
  ADD KEY `fk_id_cur_idx` (`id_cur`),
  ADD KEY `ind_numdoc` (`id_doc`),
  ADD KEY `fk_id_cur_idx1` (`id_cur`),
  ADD KEY `ind_numdoc1` (`id_doc`);

--
-- Indices de la tabla `det_doc_referido`
--
ALTER TABLE `det_doc_referido`
  ADD KEY `idDr_ref_idx` (`codDet_ref`),
  ADD KEY `idDr_doc_idx` (`codDet_doc`);

--
-- Indices de la tabla `det_interes_docente`
--
ALTER TABLE `det_interes_docente`
  ADD KEY `codOi_sem_idx` (`codOi_sem`),
  ADD KEY `codOi_sem_idx1` (`codOi_doc`),
  ADD KEY `codOi_int_idx` (`codOi_Inte`);

--
-- Indices de la tabla `det_seccion`
--
ALTER TABLE `det_seccion`
  ADD KEY `idDs_secc_idx` (`codDs_sec`),
  ADD KEY `idDs_doc_idx` (`codDs_doc`);

--
-- Indices de la tabla `det_usu_esc`
--
ALTER TABLE `det_usu_esc`
  ADD PRIMARY KEY (`idUsuario`,`idEscuela`),
  ADD KEY `fk_idesc_idx` (`idEscuela`),
  ADD KEY `fk_perfilUsuario_idx` (`idPerfl`);

--
-- Indices de la tabla `disponibilidad_docente`
--
ALTER TABLE `disponibilidad_docente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `codDd_docente_idx` (`cod_Docente`),
  ADD KEY `codDd_semestre_idx` (`cod_semestre`);

--
-- Indices de la tabla `docente`
--
ALTER TABLE `docente`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fecini_UNIQUE` (`fecini`),
  ADD KEY `idD_sem_idx` (`codD_sem`),
  ADD KEY `IND_NUMDOC` (`numdoc`);

--
-- Indices de la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD PRIMARY KEY (`idequipo`),
  ADD KEY `idE_aula_idx` (`codE_aula`);

--
-- Indices de la tabla `escuela`
--
ALTER TABLE `escuela`
  ADD PRIMARY KEY (`idescuela`);

--
-- Indices de la tabla `horario`
--
ALTER TABLE `horario`
  ADD PRIMARY KEY (`idHorario`);

--
-- Indices de la tabla `intereses`
--
ALTER TABLE `intereses`
  ADD PRIMARY KEY (`idIntereses`);

--
-- Indices de la tabla `pabellon`
--
ALTER TABLE `pabellon`
  ADD PRIMARY KEY (`idpabellon`);

--
-- Indices de la tabla `parametros`
--
ALTER TABLE `parametros`
  ADD PRIMARY KEY (`codP_esc`,`codP_sem`,`ciclo`),
  ADD KEY `idP_sem_idx` (`codP_sem`);

--
-- Indices de la tabla `perfil`
--
ALTER TABLE `perfil`
  ADD PRIMARY KEY (`idperfil`);

--
-- Indices de la tabla `plan_curricular`
--
ALTER TABLE `plan_curricular`
  ADD PRIMARY KEY (`idPlan`),
  ADD UNIQUE KEY `ult_usu_mod_reg_UNIQUE` (`ult_usu_mod_reg`),
  ADD KEY `idPc_sem_idx` (`codPc_sem`),
  ADD KEY `fkIdEscuela_idx` (`idEscuela`);

--
-- Indices de la tabla `referido`
--
ALTER TABLE `referido`
  ADD PRIMARY KEY (`idreferido`);

--
-- Indices de la tabla `seccion`
--
ALTER TABLE `seccion`
  ADD PRIMARY KEY (`idseccion`),
  ADD KEY `idS_cur_idx` (`codS_cur`),
  ADD KEY `idS_sem_idx` (`codS_sem`);

--
-- Indices de la tabla `semestre`
--
ALTER TABLE `semestre`
  ADD PRIMARY KEY (`idSemestre`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`),
  ADD KEY `nombreUsu` (`nombreUsu`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `docente`
--
ALTER TABLE `docente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;
--
-- AUTO_INCREMENT de la tabla `horario`
--
ALTER TABLE `horario`
  MODIFY `idHorario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `intereses`
--
ALTER TABLE `intereses`
  MODIFY `idIntereses` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `aula`
--
ALTER TABLE `aula`
  ADD CONSTRAINT `idA_pab` FOREIGN KEY (`codA_pab`) REFERENCES `pabellon` (`idpabellon`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `det_alum_seccion`
--
ALTER TABLE `det_alum_seccion`
  ADD CONSTRAINT `idDas_alum` FOREIGN KEY (`codDas_alum`) REFERENCES `alumno` (`idalumno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idDas_secc` FOREIGN KEY (`codDas_secc`) REFERENCES `seccion` (`idseccion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `det_asig_aula`
--
ALTER TABLE `det_asig_aula`
  ADD CONSTRAINT `idDaa_aula` FOREIGN KEY (`codDaa_aula`) REFERENCES `aula` (`idaula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idDaa_secc` FOREIGN KEY (`codDaa_seccion`) REFERENCES `seccion` (`idseccion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `det_curso_esc`
--
ALTER TABLE `det_curso_esc`
  ADD CONSTRAINT `idcurso` FOREIGN KEY (`codDce_curso`) REFERENCES `curso` (`idcurso`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `idesc` FOREIGN KEY (`codDce_escuela`) REFERENCES `escuela` (`idescuela`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idpla` FOREIGN KEY (`idPlan`) REFERENCES `plan_curricular` (`idPlan`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `det_doc_cur`
--
ALTER TABLE `det_doc_cur`
  ADD CONSTRAINT `fk_id_cur` FOREIGN KEY (`id_cur`) REFERENCES `curso` (`idcurso`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_id_doc_1` FOREIGN KEY (`id_doc`) REFERENCES `docente` (`numdoc`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `det_doc_referido`
--
ALTER TABLE `det_doc_referido`
  ADD CONSTRAINT `idDr_doc` FOREIGN KEY (`codDet_doc`) REFERENCES `docente` (`numdoc`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idDr_ref` FOREIGN KEY (`codDet_ref`) REFERENCES `referido` (`idreferido`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `det_interes_docente`
--
ALTER TABLE `det_interes_docente`
  ADD CONSTRAINT `codOi_doc` FOREIGN KEY (`codOi_doc`) REFERENCES `docente` (`numdoc`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `codOi_int` FOREIGN KEY (`codOi_Inte`) REFERENCES `intereses` (`idIntereses`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `codOi_sem` FOREIGN KEY (`codOi_sem`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `det_seccion`
--
ALTER TABLE `det_seccion`
  ADD CONSTRAINT `idDs_doc` FOREIGN KEY (`codDs_doc`) REFERENCES `docente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idDs_secc` FOREIGN KEY (`codDs_sec`) REFERENCES `seccion` (`idseccion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `det_usu_esc`
--
ALTER TABLE `det_usu_esc`
  ADD CONSTRAINT `fk_idesc` FOREIGN KEY (`idEscuela`) REFERENCES `escuela` (`idescuela`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_idusu` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_perfilUsuario` FOREIGN KEY (`idPerfl`) REFERENCES `perfil` (`idperfil`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `disponibilidad_docente`
--
ALTER TABLE `disponibilidad_docente`
  ADD CONSTRAINT `codDd_docente` FOREIGN KEY (`cod_Docente`) REFERENCES `docente` (`numdoc`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `codDd_semestre` FOREIGN KEY (`cod_semestre`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `docente`
--
ALTER TABLE `docente`
  ADD CONSTRAINT `idD_sem` FOREIGN KEY (`codD_sem`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD CONSTRAINT `idE_aula` FOREIGN KEY (`codE_aula`) REFERENCES `aula` (`idaula`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `parametros`
--
ALTER TABLE `parametros`
  ADD CONSTRAINT `idP_esc` FOREIGN KEY (`codP_esc`) REFERENCES `escuela` (`idescuela`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idP_sem` FOREIGN KEY (`codP_sem`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `plan_curricular`
--
ALTER TABLE `plan_curricular`
  ADD CONSTRAINT `fkIdEscuela` FOREIGN KEY (`idEscuela`) REFERENCES `escuela` (`idescuela`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idPc_sem` FOREIGN KEY (`codPc_sem`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `seccion`
--
ALTER TABLE `seccion`
  ADD CONSTRAINT `idS_cur` FOREIGN KEY (`codS_cur`) REFERENCES `curso` (`idcurso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idS_sem` FOREIGN KEY (`codS_sem`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION;
  
  
-- Referenciando llaves faltantes
SET FOREIGN_KEY_CHECKS=0;



alter table horario
	drop primary key,
	add constraint pk_horario primary key (idHorario,codH_Sem,codH_curso,codH_doc,codH_secc,codH_aula) ;
    

    
alter table horario
	add constraint fk_horario_semestre foreign key(codH_Sem) references semestre(idSemestre) on delete no action on update no action,
    add constraint fk_horario_curso foreign key(codH_curso) references curso(idCurso) on delete no action on update no action,
    add constraint fk_horario_docente foreign key(codH_doc) references docente(id) on delete no action on update no action,
    add constraint fk_horario_seccion foreign key(codH_secc) references seccion(idseccion) on delete no action on update no action,
    add constraint fk_horario_aula foreign key(codH_aula) references aula(idaula) on delete no action on update no action,
    add constraint fk_horario_escuela foreign key(cod_esc) references escuela(idescuela) on delete no action on update no action;
    


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
