-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: db_final
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alumno`
--

DROP TABLE IF EXISTS `alumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alumno` (
  `idalumno` varchar(12) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `ape_pat` varchar(45) DEFAULT NULL,
  `ape_mat` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idalumno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumno`
--

LOCK TABLES `alumno` WRITE;
/*!40000 ALTER TABLE `alumno` DISABLE KEYS */;
INSERT INTO `alumno` VALUES ('1','MARTIN','BAZALAR','CONTRERAS'),('2','NOAH','BAZALAR','NEYRA');
/*!40000 ALTER TABLE `alumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aula`
--

DROP TABLE IF EXISTS `aula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aula` (
  `idaula` varchar(12) NOT NULL,
  `num_aula` int(11) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `codA_pab` varchar(12) NOT NULL,
  PRIMARY KEY (`idaula`),
  KEY `idA_pab_idx` (`codA_pab`),
  CONSTRAINT `idA_pab` FOREIGN KEY (`codA_pab`) REFERENCES `pabellon` (`idpabellon`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aula`
--

LOCK TABLES `aula` WRITE;
/*!40000 ALTER TABLE `aula` DISABLE KEYS */;
INSERT INTO `aula` VALUES ('1',101,NULL,'1'),('10',302,NULL,'1'),('11',303,NULL,'1'),('12',304,NULL,'1'),('13',101,NULL,'2'),('14',102,NULL,'2'),('15',103,NULL,'2'),('16',104,NULL,'2'),('17',105,NULL,'2'),('18',106,NULL,'2'),('19',201,NULL,'2'),('2',102,NULL,'1'),('20',202,NULL,'2'),('21',203,NULL,'2'),('22',204,NULL,'2'),('23',205,NULL,'2'),('24',206,NULL,'2'),('25',301,NULL,'2'),('26',302,NULL,'2'),('27',303,NULL,'2'),('28',304,NULL,'2'),('29',305,NULL,'2'),('3',103,NULL,'1'),('30',306,NULL,'2'),('31',101,NULL,'5'),('32',102,NULL,'5'),('33',103,NULL,'5'),('34',104,NULL,'5'),('35',201,NULL,'5'),('36',101,NULL,'3'),('37',102,NULL,'3'),('38',103,NULL,'3'),('39',201,NULL,'3'),('4',104,NULL,'1'),('40',202,NULL,'3'),('41',203,NULL,'3'),('42',301,NULL,'3'),('43',302,NULL,'3'),('44',303,NULL,'3'),('5',201,NULL,'1'),('6',202,NULL,'1'),('7',203,NULL,'1'),('8',204,NULL,'1'),('9',301,NULL,'1');
/*!40000 ALTER TABLE `aula` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curso`
--

DROP TABLE IF EXISTS `curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curso` (
  `idcurso` varchar(12) NOT NULL,
  `nombre` varchar(60) DEFAULT NULL,
  `creditos` int(11) DEFAULT NULL,
  `h_teo` int(11) DEFAULT NULL,
  `h_lab` int(11) DEFAULT NULL,
  `h_pract` int(11) DEFAULT NULL,
  `estado_hab_inhab` tinyint(1) DEFAULT NULL,
  `req01` varchar(15) DEFAULT NULL,
  `req02` varchar(15) DEFAULT NULL,
  `req03` varchar(11) DEFAULT NULL,
  `cooreq` varchar(11) DEFAULT NULL,
  `cred_req` varchar(12) DEFAULT NULL,
  `nombreProp` varchar(45) DEFAULT NULL,
  `codLab` varchar(12) DEFAULT NULL,
  `usu_crea_reg` varchar(45) DEFAULT NULL,
  `fec_crea_reg` datetime DEFAULT NULL,
  `ult_usu_mod_reg` varchar(45) DEFAULT NULL,
  `fec_ult_mod_reg` datetime DEFAULT NULL,
  PRIMARY KEY (`idcurso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curso`
--

LOCK TABLES `curso` WRITE;
/*!40000 ALTER TABLE `curso` DISABLE KEYS */;
INSERT INTO `curso` VALUES ('09000201020','Lenguaje',2,1,0,2,1,'-----',NULL,NULL,NULL,NULL,'Departamento Académico',NULL,NULL,NULL,NULL,NULL),('09000301030','Filosofia',3,3,0,0,1,'-----',NULL,NULL,NULL,NULL,'Departamento Académico',NULL,NULL,NULL,NULL,NULL),('09005303050','Algoritmo y Estructura de Datos I',5,3,0,4,1,'09111402050','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09006904050','Algoritmo y Estructura de Datos II',5,3,0,4,1,'09005303050','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09007010040','Proyecto II',4,4,0,0,1,'09067309040','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09008806040','Teoría General de Sistemas',4,4,0,0,1,'09009005040','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09008905050','Teoría y Diseño de Base de Datos',5,3,0,4,1,'09006904050','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09011607040','INVESTIGACION OPERATIVA II',0,0,0,0,1,'09008506040',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09011906050','Ingeniería de Software I',5,3,0,4,1,'09093205051','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09013707050','Ingeniería de Software II',5,3,0,4,1,'09011906050','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('090147E4020','COMPORTAMIENTO ORGANIZACIONAL',0,0,0,0,1,'09009005040',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09036602050','Álgebra Lineal',5,4,0,2,1,'----',NULL,NULL,NULL,NULL,'Departamento Académico',NULL,NULL,NULL,NULL,NULL),('090608E1040','TALLER DE CREATIVIDAD EMPRESARIAL',4,0,0,0,1,'09112107050',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09061700040','GESTION ESTRATEGICA',0,0,0,0,1,'120 CRED.',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09065502050','Cálculo I',5,4,0,2,1,'09066801051','09066301040','','','','Departamento Académico',NULL,NULL,NULL,NULL,NULL),('090658E3040','CALIDAD DE SOFTWARE',4,0,0,0,1,'091124E3040',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('090659E3040','DESARROLLO DE APLICACIONES I',4,0,0,0,1,'090672E3040',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('090660E3040','DESARROLLO DE APLICACIONES II',4,0,0,0,1,'090659E3040',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09066201020','Introduccion a la Ingeniería',2,1,0,2,1,'-----',NULL,NULL,NULL,NULL,'Departamento Académico',NULL,NULL,NULL,NULL,NULL),('09066301040','Geometría Analítica',4,3,0,2,1,'-----',NULL,NULL,NULL,NULL,'Departamento Académico',NULL,NULL,NULL,NULL,NULL),('09066408040','Gestión de Recursos de TI',4,4,0,0,1,'09013707050','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09066502031','Fundamentos de Diseño Web',3,2,0,2,1,'09066201020','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09066607040','Inteligencia Artificial y Robótica',4,4,0,0,1,'09067106050','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09066801051','Matemática Discreta',5,4,0,2,1,'------',NULL,NULL,NULL,NULL,'Departamento Académico',NULL,NULL,NULL,NULL,NULL),('09067009040','Planeamiento Estratégico de TI',4,4,0,0,1,'09066408040','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09067106050','Programación I',5,3,0,4,1,'09008905050','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('090672E3040','PROGRAMACION II',4,0,0,0,1,'09067106050',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09067309040','Proyecto I',4,4,0,0,1,'09054808040','09112107050','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('090675E2040','REDES Y CONECTIVIDAD I (CCNA I CISCO)',4,0,0,0,1,'09127905040',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('090676E2040','REDES Y CONECTIVIDAD II (CCNA II CISCO)',4,0,0,0,1,'090675E2040',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('090677E2040','REDES Y CONECTIVIDAD III (CCNA III CISCO)',4,0,0,0,1,'090676E2040',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('090678E2040','REDES Y CONECTIVIDAD VI (CCNA VI CISCO)',4,0,0,0,1,'090677E2040',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09067909040','Seguridad y Auditoría de SI',4,3,0,2,1,'09072108040','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09070901030','Realidad Nacional',3,3,0,0,1,'------',NULL,NULL,NULL,NULL,'Departamento Académico',NULL,NULL,NULL,NULL,NULL),('09071001020','Método de Estudio',2,1,0,2,1,'------',NULL,NULL,NULL,NULL,'Departamento Académico',NULL,NULL,NULL,NULL,NULL),('09072108040','Diseño e Implementación de Sistemas',4,3,0,2,1,'09013707050','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09085200030','GESTION DE PROYECTOS - PMI',0,0,0,0,1,'09054808040',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('090862E2040','SEGURIDAD INFORMATICA',4,0,0,0,1,'09067909040',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09086300020','GESTION DE LA INNOVACION',0,0,0,0,1,'09054808040',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09093205051','Gestión de Procesos',5,4,0,2,1,'09127603030','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('090933E1040','GESTION DEL CONOCIMIENTO',4,0,0,0,1,'150 CRED.',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09093409040','Inteligencia de Negocios',4,4,0,0,1,'09128808040','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09111402050','Laboratorio Introducción a la Programación',0,0,0,0,1,'09066801051','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09111503050','Tecnología de Información I',5,4,0,2,1,'09111402050','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('091119E1040','SISTEMAS INTEGRADOS DE GESTION ERP',4,0,0,0,1,'09066408040',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09112107050','Taller de Proyectos',5,0,0,10,1,'09067106050','09011906050','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('091124E3040','PRUEBAS DE SOFTWARE',4,0,0,0,1,'09013707050',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('091125E4020','TOPICOS DE COMPUTACION',0,0,0,0,1,'134 CRED.',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('091126E4040','DESARROLLO DE JUEGOS',0,0,0,0,1,'09013707050',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09114904040','Tecnología de Información II',4,3,0,2,1,'09111503050','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('091217E4020','INTRODUCCION A LA INVESTIGACION EN INFORMATICA',0,0,0,0,1,'150 CRED.',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09127603030','Sistemas de Información',3,2,0,2,1,'09066201020','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09127905040','Servidores y Sistemas Operativos',4,3,0,2,1,'09114904040','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09128808040','Arquitectura Empresarial',4,4,0,0,1,'09008806040','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('09134910040','Marketing Digital',4,4,0,0,1,'09054808040','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('90034000000','Etica',2,1,0,2,1,'','','','','170 créditos','Departamento Académico',NULL,NULL,NULL,NULL,NULL),('90037000000','Quimic',3,1,0,4,1,'90662000000',NULL,NULL,NULL,NULL,'Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90049000000','Construcción I',3,2,0,2,1,'91277000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90054000000','Estadistica_y_Probabilidades_I',4,3,0,2,1,'90655000000','','','','','Departamento Académico',NULL,NULL,NULL,NULL,NULL),('90056000000','Física I',5,3,0,4,1,'90655000000','90366000000','','','','Departamento Académico',NULL,NULL,NULL,NULL,NULL),('90060000000','Estadística y Probabilidades II',4,3,0,2,1,'90054000000','','','','','Departamento Académico',NULL,NULL,NULL,NULL,NULL),('90067000000','Construcción II',4,3,0,2,1,'90049000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90068000000','Circuitos Eléctricos I',5,3,0,4,1,'90656000000','','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90072000000','Química Industrial',5,3,0,4,1,'90056000000','','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90074000000','Física II',5,3,0,4,1,'90056000000','','','','','Departamento Académico',NULL,NULL,NULL,NULL,NULL),('90077000000','Microeconomía',4,3,0,2,1,'91274000000','','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90081000000','Teoría de Campos',4,2,0,4,1,'90091000000','90695000000','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90082000000','Circuitos Electrónicos I',5,3,0,4,1,'90083000000','90694000000','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90083000000','Circuitos Eléctricos II',5,3,0,4,1,'90083000000','90074000000','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90085000000','Investigación Operativa I',4,3,0,2,1,'90060000000','90662000000','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90086000000','Materiales de Ingeniería',4,3,0,2,1,'90072000000','90177000000','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90087000000','Mecánica Aplicada',5,4,0,2,1,'90056000000','','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90090000000','Ingeniería Administrativa',4,3,0,2,1,'90077000000','','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90091000000','Física III',4,2,0,4,1,'90074000000','','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90096000000','Instalaciones Sanitarias',3,2,0,2,1,'90265000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90109000000','Líneas de Transmisión y Antenas',4,2,0,4,1,'90081000000','','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90110000000','Circuitos Digitales I',5,3,0,4,1,'90053000000','90694000000','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90111000000','Circuitos Electrónicos II',4,2,0,4,1,'90082000000','','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90112000000','Máquinas Eléctricas',4,2,0,4,1,'90083000000','','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90116000000','Investigación Operativa II',4,3,0,2,1,'90085000000','','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90118000000','Ingeniería de Métodos I',4,3,0,2,1,'90060000000','','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90122000000','Contabilidad General',4,3,0,2,1,'','','','','80 créditos','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90125000000','Sistemas de Control I',4,2,0,4,1,'90696000000','','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90126000000','Telecomunicaciones I',5,3,0,4,1,'90054000000','90696000000','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90127000000','Circuitos Digitales II',5,3,0,4,1,'90110000000','','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90128000000','Circuitos Electrónicos III',4,2,0,4,1,'90111000000','','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90131000000','Ingeniería de Costos',4,3,0,2,1,'90122000000','','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90132000000','Ingeniería de Métodos II',4,3,0,2,1,'90118000000','90136000000','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90134000000','Mercadotecnia',4,3,0,2,1,'90090000000','90131000000','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90136000000','Ingeniería de Procesos',5,3,0,4,1,'90090000000','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('90140000000','Proceso de Manufactura',4,1,0,6,1,'91280000000','','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90141000000','Planeamiento y Control de la Producción I',4,3,0,2,1,'90116000000','90132000000','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90145000000','Gestión Financiera',4,3,0,2,1,'90131000000','','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90148000000','Arquitectura de Computadores I',5,3,0,4,1,'90127000000','','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90150000000','Sistemas de Control II',4,2,0,4,1,'90125000000','','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90151000000','Telecomunicaciones II',5,3,0,4,1,'90126000000','','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90156000000','Planeamiento y Control de la Producción II',4,2,0,4,1,'90141000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90158000000','Arquitectura de Computadores II',5,3,0,4,1,'90148000000','','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90164000000','Control de Calidad',4,2,0,4,1,'90140000000','','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90165000000','Diseño de Sistemas de Producción',4,3,0,2,1,'90141000000','','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90170000000','Automatización Industrial',4,1,0,6,1,'91143000000','','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90171000000','Arquitectura de Redes',4,2,0,4,1,'90151000000','90158000000','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90177000000','Diseño Industrial por Computador',3,1,0,4,1,'90661000000','','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90197000000','Sistemas de Control Digital',4,2,0,4,1,'90150000000','','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90201000000','Planeamiento, Desa. e Ing. del Producto',4,2,0,4,1,'90134000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90210000000','Ingeniería de Métodos',4,3,0,2,1,'90141000000','90836000000','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90213000000','Procesamiento Digital de Señales',4,1,0,6,1,'90126000000','','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90251000000','Geología General',2,1,0,2,1,'90662000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90254000000','Estática',4,3,0,2,1,'90056000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90256000000','Dinámica',3,2,0,2,1,'90056000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90260000000','Resistencia de Materiales I',5,4,0,2,1,'90254000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90261000000','Mecánica de Suelos I',4,3,0,2,1,'90251000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90265000000','Mecánica de Fluidos I',5,4,0,2,1,'90412000000','90256000000','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90266000000','Resistencia de Materiales II',4,3,0,2,1,'90260000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90267000000','Mecánica de Suelos II',4,3,0,2,1,'90261000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90268000000','Tecnología del Concreto',3,2,0,2,1,'91277000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90269000000','Mecánica de Fluidos II',5,4,0,2,1,'90265000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90271000000','Análisis Estructural I',4,3,0,2,1,'90266000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90275000000','Concreto Armado I',4,3,0,2,1,'90271000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90282000000','Caminos I',4,3,0,2,1,'91275000000','90049000000','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90285000000','Puentes y Obras de Arte',3,2,0,2,1,'90295000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90286000000','Concreto Armado II',4,3,0,2,1,'90275000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90295000000','Análisis Estructural II',4,3,0,2,1,'90271000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90309000000','Hidráulica',4,3,0,2,1,'90596000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90412000000','Ecuaciones Diferenciales',4,3,0,2,1,'90656000000','','','','','Departamento Académico',NULL,NULL,NULL,NULL,NULL),('90458000000','Investigación de Mercados',4,3,0,2,1,'90134000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('90548000000','Formulación y Evaluación de Proyectos',4,3,0,2,1,'90145000000','','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90595000000','Presupuesto y Programación de Obra',4,2,0,4,1,'90145000000','90067000000','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90596000000','Hidrología',3,2,0,2,1,'90269000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90597000000','Ecología e Impacto Ambiental',3,2,0,2,1,'90049000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90617000000','Gestión Estratégica',4,3,0,2,1,'','','','','120 créditos','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('90635000000','Laboratorio de Química General',0,0,0,0,1,'-----',NULL,NULL,NULL,NULL,'Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90656000000','Cálculo II',5,4,0,2,1,'90655000000','','','','','Departamento Académico',NULL,NULL,NULL,NULL,NULL),('90661000000','Dibujo y Diseño Gráfico',3,2,0,2,1,'90663000000','','','','','Departamento Académico',NULL,NULL,NULL,NULL,NULL),('90667000000','Liderazgo y Oratoria',2,1,0,2,1,'','','','','100 créditos','Departamento Académico',NULL,NULL,NULL,NULL,NULL),('90681000000','Gestión de Personal y Legislación laboral',4,3,0,2,1,'90683000000','','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90682000000','Taller de Manufactura Moderna',2,0,0,4,1,'90140000000','','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90683000000','Psicología Industrial y Organizacional',2,1,0,2,1,'','','','','174 créditos','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90690000000','Proyecto Final de Ingeniería Industrial I',4,2,0,4,1,'90548000000','90164000000','91127000000','90170000000','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90691000000','Proyecto Final de Ingeniería Industrial II',4,2,0,4,1,'90156000000','90690000000','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90694000000','Dispositivos Electrónicos',4,1,0,6,1,'90037000000','','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90695000000','Matemática Aplicada',4,2,0,4,1,'90412000000','','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90696000000','Señales y Sistemas',4,2,0,4,1,'90695000000','','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90697000000','Análisis Contable y Financiero',4,3,0,2,1,'90090000000','','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90698000000','Proyecto de Ingeniería I',4,2,0,4,1,'90128000000','90158000000','90213000000','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90699000000','Proyecto de Ingeniería II',4,2,0,4,1,'90698000000','','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90700000000','Electrónica de Potencia',4,2,0,4,1,'90112000000','90128000000','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('90803000000','Alimentación y Nutrición Humana',4,2,0,4,1,'90804000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90804000000','Análisis de Alimentos',4,2,0,4,1,'90829000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90805000000','Biología General',4,2,0,4,1,'90662000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90806000000','Bioquímica',5,3,0,4,1,'90805000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90808000000','Control de Calidad de Alimentos',4,2,0,4,1,'90835000000','90824000000','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90809000000','Elemento de Máquinas',3,1,0,4,1,'91044000000','91093000000','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90810000000','Envases y Embalajes',3,1,0,4,1,'90837000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90811000000','Fenómenos de Transporte',4,2,0,4,1,'90804000000','90841000000','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90812000000','Física General',4,2,0,4,1,'90655000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90813000000','Físico Química de alimentos',4,2,0,4,1,'90831000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90814000000','Gestión Ambiental en la Ind. Alimen. ',3,2,0,2,1,'90141000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90817000000','Ingeniería de Alimentos I',4,2,0,4,1,'90811000000','90820000000','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90818000000','Ingeniería de Alimentos II',4,2,0,4,1,'90817000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90820000000','Introducción a la Ingeniería de Alimentos',3,1,0,4,1,'90813000000','90824000000','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90821000000','Laboratorio de Bioquímica',0,0,0,0,1,'90805000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90822000000','Laboratorio de Microbiología',0,0,0,0,1,'90806000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90823000000','Maquinaria para la Industria Alimentaria',3,1,0,4,1,'90809000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90824000000','Métodos Estadísticos',4,3,0,2,1,'91028000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90825000000','Microbiología',4,2,0,4,1,'90806000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90826000000','Microbiología de Alimentos',4,2,0,4,1,'90825000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90829000000','Química de Alimentos',4,2,0,4,1,'90831000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90830000000','Química Orgánica',4,2,0,4,1,'90037000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90831000000','Química Analítica',4,2,0,4,1,'90830000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90832000000','Ingeniería del Frío',4,2,0,4,1,'90837000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90835000000','Tecnología de Alimentos I',4,2,0,4,1,'90820000000','90804000000','90826000000','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90836000000','Tecnología de Alimentos II',4,2,0,4,1,'90835000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90837000000','Tecnologías de Alimentos III',4,2,0,4,1,'90818000000','90836000000','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90841000000','Temodinámica',4,2,0,4,1,'90813000000','90829000000','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90847000000','Laboratorio de Química Orgánica',0,0,0,0,1,'90037000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90849000000','Total Quality Management TQM',4,2,0,4,1,'90164000000','','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('90864000000','Laboratorio de Física General',0,0,0,0,1,'90655000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90865000000','Laboratorio de Biología General',0,0,0,0,1,'90662000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90866000000','Laboratorio de Química Analítica',0,0,0,0,1,'90830000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90873000000','Taller I',7,4,0,6,1,'-----',NULL,NULL,NULL,NULL,'Arquitectura',NULL,NULL,NULL,NULL,NULL),('90875000000','Expresión Arquitectónica I',3,1,0,4,1,'-----',NULL,NULL,NULL,NULL,'Arquitectura',NULL,NULL,NULL,NULL,NULL),('90876000000','Taller II',7,4,0,6,1,'90873000000','','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('90879000000','Expresión Arquitectónica II',3,2,0,2,1,'90875000000','','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('90883000000','Estructuras I',3,3,0,0,1,'91312000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90886000000','Expresión Arquitectónica III',3,2,0,2,1,'90879000000','','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('90889000000','Estructuras II',3,3,0,0,1,'90883000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90892000000','Expresión Arquitectónica IV',3,2,0,2,1,'90886000000','','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('90893000000','Taller V',8,4,0,8,1,'91315000000','90886000000','90883000000','91320000000','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('90897000000','Taller VI',8,4,0,8,1,'90893000000','','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('90900000000','Laboratorio de Medios Digitales',3,2,0,2,1,'90892000000','','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('90901000000','Taller VII',8,4,0,8,1,'90897000000','91324000000','91325000000','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('90904000000','Taller VIII',8,4,0,8,1,'90901000000','','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('90905000000','Ejercicio Profesional',3,2,0,2,1,'91330000000','','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('90906000000','Métodos de Investigación',4,3,0,2,1,'-----','','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('90938000000','Instalaciones Eléctricas en Edificaciones',2,0,0,4,1,'90049000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90939000000','Topografía Avanzada',2,1,0,2,1,'91275000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('90943000000','Economía Empresarial',3,2,0,2,1,'91028000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90949000000','Laboratorio de Química de Alimentos',0,0,0,0,1,'90831000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('90972000000','Aerodinámica',4,3,0,2,1,'91311000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('90976000000','Meteorología',4,3,0,2,1,'90972000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('90977000000','Operaciones de Piloto Privado',4,3,0,2,1,'91311000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('90978000000','Sistemas y Componentes',4,3,0,2,1,'-----','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('90991000000','Operaciones de Piloto Instrumental',4,3,0,2,1,'90977000000','90976000000','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('90994000000','Regulaciones Aéreas I',3,3,0,0,1,'-----',NULL,NULL,NULL,NULL,'Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91028000000','Estadística General',4,3,0,2,1,'90655000000','','','','','Departamento Académico',NULL,NULL,NULL,NULL,NULL),('91029000000','Laboratorio de Microbiología de Alimentos',0,0,0,0,1,'90825000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('91030000000','Laboratorio de Análisis de Alimentos',0,0,0,0,1,'90829000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('91037000000','Automatización en la Industria Alimentaria ',3,1,0,4,1,'90809000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('91038000000','Proyectos de Ingeniería en Ind. Aliment. I',4,3,0,2,1,'90548000000','90201000000','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('91039000000','Gestión de la Calidad e Inocuidad Alimentaria',4,3,0,2,1,'90808000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('91040000000','Proyectos de Ingeniería en Ind. Aliment. II',4,3,0,2,1,'91038000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('91044000000','Electrotécnia',3,1,0,4,1,'90812000000','90656000000','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('91076000000','Laboratorio de Electrotécnia',0,0,0,0,1,'90812000000','90656000000','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('91077000000','Lab. de Introducción a la Ing. de Alimentos',0,0,0,0,1,'90813000000','90824000000','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('91078000000','Lab. de Alimentación y Nutrición Humana',0,0,0,0,1,'90804000000','','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('91093000000','Mecánica de Materiales',4,2,0,4,1,'90812000000','90177000000','','','','Ingeniería en Industrias Alimentarias',NULL,NULL,NULL,NULL,NULL),('91114000000',' Introducción a la Programación',5,3,0,4,1,'90668000000','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('91127000000','Planeamiento y Cuadro de Mando Integral',4,2,0,4,1,'90134000000','','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('91142000000','Ingeniería Eléctrica y Electrónica',5,3,0,4,1,'90074000000','90412000000','','','','Ingeniería Electrónica',NULL,NULL,NULL,NULL,NULL),('91143000000','Instrumentación y Control Industrial',2,0,0,4,1,'91142000000','','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('91144000000','Mantenimiento, Seguridad y Salud Ocupacional',4,2,0,4,1,'90165000000','90156000000','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('91176000000','Comportamiento del Consumidor',4,4,0,0,1,'90458000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91179000000','Gestión de Ventas',4,3,0,2,1,'91176000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91181000000','Marketing Estratégico en Aviación',4,4,0,0,1,'91176000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91274000000','Introducción a la Economía',3,2,0,2,1,'90709000000','','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('91275000000','Topografía',3,1,0,4,1,'----','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('91277000000','Tecnología de los Materiales',3,1,0,4,1,'90251000000','','','','','Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('91280000000','Resistencia de Materiales',5,4,0,2,1,'90087000000','90086000000','','','','Ingeniería Industrial',NULL,NULL,NULL,NULL,NULL),('91281000000','Pavimentos',5,3,0,4,1,'90282000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('91289000000','Proyecto Final de Ingeniería Civil I',4,4,0,0,1,'90548000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('91290000000','Ingeniería Antisísmica',4,3,0,2,1,'90295000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('91292000000','Proyecto Final de Ingeniería Civil II',4,4,0,0,1,'91289000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('91293000000','Abastecimiento de Agua y Alcantarillado',4,3,0,2,1,'90596000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('91294000000','Ingeniería de Valuaciones y Tasaciones',4,3,0,2,1,'90145000000','90595000000','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('91295000000','Organización y Dir. de Emp. Constructoras',4,3,0,2,1,'90548000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('91310000000','Matemática I',4,3,0,2,1,'-----',NULL,NULL,NULL,NULL,'Departamento Académico',NULL,NULL,NULL,NULL,NULL),('91311000000','Introducción a la Aviación',3,3,0,0,1,'-----',NULL,NULL,NULL,NULL,'Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91312000000','Matemática II',4,3,0,2,1,'91310000000','','','','','Departamento Académico',NULL,NULL,NULL,NULL,NULL),('91313000000','Geometría Descriptiva',4,3,0,2,1,'91310000000','','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('91314000000','Física General I',4,3,0,2,1,'------',NULL,NULL,NULL,NULL,'Departamento Académico',NULL,NULL,NULL,NULL,NULL),('91315000000','Construcción I',4,2,0,4,1,'91312000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('91316000000','Taller III',8,4,0,8,1,'90875000000','90876000000','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('91317000000','Construcción II',4,2,0,4,1,'91315000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('91318000000','Fotografía',2,1,0,2,1,'-----','','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('91319000000','Percepción del Arte y la Arquitectura',2,2,0,0,1,'-----','','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('91320000000','Taller IV',8,4,0,8,1,'91316000000','','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('91321000000','Física General II',4,3,0,2,1,'91314000000','','','','','Departamento Académico',NULL,NULL,NULL,NULL,NULL),('91322000000','Historia de la Arquitectura I',3,3,0,0,1,'------','','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('91324000000','Construcción III',4,2,0,4,1,'91317000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('91325000000','Urbanismo I',4,4,0,0,1,'------','','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('91326000000','Historia de la Arquitectura II',3,3,0,0,1,'91322000000','','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('91328000000','Instalaciones Sanitarias y Electromecánicas',4,4,0,0,1,'91324000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('91329000000','Urbanismo II',4,4,0,0,1,'91325000000','','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('91330000000','Diseño Bioclimático',3,2,0,2,1,'91328000000','','','','','Ingeniería Civil',NULL,NULL,NULL,NULL,NULL),('91331000000','Historia de la Arquitectura III',3,3,0,0,1,'91326000000','','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('91332000000','Urbanismo III',4,4,0,0,1,'91329000000','','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('91333000000','Historia de la Arquitectura IV',3,3,0,0,1,'91331000000','','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('91334000000','Seminario de Construcción',6,5,0,2,1,'90906000000','90905000000','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('91335000000','Taller IX',8,4,0,8,1,'91330000000','91332000000','9904000000','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('91336000000','Seminario Urbano',6,5,0,2,1,'91332000000','90906000000','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('91337000000','Taller X',8,4,0,8,1,'91335000000','','','','','Arquitectura',NULL,NULL,NULL,NULL,NULL),('91348000000','Finanzas',4,3,0,2,1,'90122000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91351000000','Mercancías Peligrosas',2,2,0,0,1,'90994000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91352000000','Motores y Turbinas de Aeronaves',4,2,0,4,1,'91314000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91353000000','Derecho Empresarial',4,3,0,2,1,'91358000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91354000000','Negocios Internacionales',4,4,0,0,1,'90134000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91355000000','Regulaciones Aéreas II',3,3,0,0,1,'90994000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91356000000','Seguridad Aeronáutica',3,3,0,0,1,'90977000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91357000000','Operaciones de Piloto Comercial',4,2,0,4,1,'90977000000','90976000000','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91358000000','Introducción a la Administración',4,4,0,0,1,'--------','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91359000000','Estadística y Probabilidades',4,3,0,2,1,'91312000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91362000000','Administración Logística',4,4,0,0,1,'91358000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91367000000','Administración de Operaciones',4,4,0,0,1,'91362000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91368000000','Sistemas de Información Gerencial',3,2,0,2,1,'91367000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91369000000','Gestión de Recursos Humanos',4,3,0,2,1,'91367000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91370000000','Proyecto Aeronáutico I',4,4,0,0,1,'91357000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91371000000','Proyecto Aeronáutico II',4,4,0,0,1,'91370000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91381000000','Operaciones de Despacho Aéreo',4,3,0,2,1,'91355000000','91357000000','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91382000000','Gestión Pública',4,4,0,0,1,'91368000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91383000000','Performance de Aeronaves',4,3,0,2,1,'91314000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91384000000','Fisiología de Vuelo',3,2,0,2,1,'91311000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91385000000','Navegación Doméstica e Internacional',4,3,0,2,1,'90977000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91386000000','Planeamiento de Carrera y Entrevista',2,2,0,0,1,'-------','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91387000000','Comportamiento Organizacional en Aviación',4,4,0,0,1,'91358000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91388000000','Derecho Aeronáutico',4,3,0,2,1,'91353000000','','','','','Ciencias Aeronáuticas',NULL,NULL,NULL,NULL,NULL),('91389000000','Adm. De Base De Datos',4,0,3,3,1,'90089000000',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('91390000000','CCNA I',4,3,2,1,1,'91279000000',NULL,NULL,NULL,NULL,'Ingeniería de Computación y Sistemas',NULL,NULL,NULL,NULL,NULL),('TR000101010','Inglés I',1,0,0,2,1,'------',NULL,NULL,NULL,NULL,'Departamento Académico',NULL,NULL,NULL,NULL,NULL),('TR000202010','Inglés II',1,0,0,2,1,'TR000101010','','','','','Departamento Académico',NULL,NULL,NULL,NULL,NULL),('TR000501010','Actividades I',1,0,0,2,1,'-----',NULL,NULL,NULL,NULL,'Departamento Académico',NULL,NULL,NULL,NULL,NULL),('TR000602010','Actividades II',1,0,0,2,1,'TR000501010',NULL,NULL,NULL,NULL,'Departamento Académico',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `curso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_alum_seccion`
--

DROP TABLE IF EXISTS `det_alum_seccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_alum_seccion` (
  `codDas_alum` varchar(12) NOT NULL,
  `codDas_secc` varchar(12) NOT NULL,
  KEY `idDas_alum_idx` (`codDas_alum`),
  KEY `idDas_secc_idx` (`codDas_secc`),
  CONSTRAINT `idDas_alum` FOREIGN KEY (`codDas_alum`) REFERENCES `alumno` (`idalumno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idDas_secc` FOREIGN KEY (`codDas_secc`) REFERENCES `seccion` (`idseccion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_alum_seccion`
--

LOCK TABLES `det_alum_seccion` WRITE;
/*!40000 ALTER TABLE `det_alum_seccion` DISABLE KEYS */;
/*!40000 ALTER TABLE `det_alum_seccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_asig_aula`
--

DROP TABLE IF EXISTS `det_asig_aula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_asig_aula` (
  `codDaa_aula` varchar(12) NOT NULL,
  `codDaa_seccion` varchar(12) NOT NULL,
  `disponibilidad` tinyint(4) DEFAULT NULL,
  KEY `idDaa_aula_idx` (`codDaa_aula`),
  KEY `idDaa_secc_idx` (`codDaa_seccion`),
  CONSTRAINT `idDaa_aula` FOREIGN KEY (`codDaa_aula`) REFERENCES `aula` (`idaula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idDaa_secc` FOREIGN KEY (`codDaa_seccion`) REFERENCES `seccion` (`idseccion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_asig_aula`
--

LOCK TABLES `det_asig_aula` WRITE;
/*!40000 ALTER TABLE `det_asig_aula` DISABLE KEYS */;
/*!40000 ALTER TABLE `det_asig_aula` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_curso_esc`
--

DROP TABLE IF EXISTS `det_curso_esc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_curso_esc` (
  `codDce_curso` varchar(12) NOT NULL,
  `codDce_escuela` varchar(12) NOT NULL,
  `idPlan` varchar(12) NOT NULL,
  `codDce_sem` varchar(12) DEFAULT NULL,
  `ciclo` int(11) DEFAULT NULL,
  `cupos` int(11) DEFAULT NULL,
  `matriculados` int(11) DEFAULT NULL,
  `cat_fia` varchar(45) DEFAULT NULL,
  `tipo_fia` varchar(45) DEFAULT NULL,
  `tipo_sunedu` varchar(45) DEFAULT NULL,
  `tipo_pres_virt` varchar(45) DEFAULT NULL,
  `usu_crea_reg` varchar(45) DEFAULT NULL,
  `fec_crea_reg` datetime DEFAULT NULL,
  `ult_usu_mod_reg` varchar(45) DEFAULT NULL,
  `dec_ult_mod_reg` datetime DEFAULT NULL,
  `det_curso_esccol` varchar(45) DEFAULT NULL,
  KEY `idDce_cur_idx` (`codDce_curso`),
  KEY `idDce_esc_idx` (`codDce_escuela`),
  KEY `idSem_idx` (`codDce_sem`),
  KEY `idpla` (`idPlan`),
  CONSTRAINT `idcurso` FOREIGN KEY (`codDce_curso`) REFERENCES `curso` (`idcurso`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `idesc` FOREIGN KEY (`codDce_escuela`) REFERENCES `escuela` (`idescuela`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idpla` FOREIGN KEY (`idPlan`) REFERENCES `plan_curricular` (`idPlan`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idsem` FOREIGN KEY (`codDce_sem`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_curso_esc`
--

LOCK TABLES `det_curso_esc` WRITE;
/*!40000 ALTER TABLE `det_curso_esc` DISABLE KEYS */;
INSERT INTO `det_curso_esc` VALUES ('90002000000','101','1','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90003000000','101','1','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90005000000','101','1','4',1,45,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90020000000','101','1','4',2,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90034000000','101','1','4',10,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90053000000','101','1','4',3,50,0,'ciencias de la computacion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90054000000','101','1','4',3,50,0,'metodos cuantitativos','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90056000000','101','1','4',3,50,0,'matematicas y ciencias','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90060000000','101','1','4',4,50,0,'metodos cuantitativos','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90069000000','101','1','4',4,50,0,'ciencias de la computacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90070000000','101','1','4',10,50,0,'sistemas de informacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90074000000','101','1','4',4,50,0,'matematicas y ciencias','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90077000000','101','1','4',4,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90085000000','101','1','4',6,50,0,'metodos cuantitativos','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90088000000','101','1','4',6,50,0,'gestion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90089000000','101','1','4',5,50,0,'ciencias de la computacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90090000000','101','1','4',5,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90119000000','101','1','4',6,50,0,'ingenieria de software','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90122000000','101','1','4',5,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90131000000','101','1','4',6,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90137000000','101','1','4',7,50,0,'ingenieria de software','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90145000000','101','1','4',7,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90366000000','101','1','4',2,50,0,'matematicas y ciencias','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90548000000','101','1','4',8,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90655000000','101','1','4',2,50,0,'matematicas y ciencias','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90662000000','101','1','4',1,45,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90663000000','101','1','4',1,45,0,'matematicas y ciencias','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90664000000','101','1','4',8,50,0,'sistemas de informacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90665000000','101','1','4',2,50,0,'ingenieria de software','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90666000000','101','1','4',7,50,0,'metodos cuantitativos','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90667000000','101','1','4',9,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90668000000','101','1','4',1,45,0,'matematicas y ciencias','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90670000000','101','1','4',9,50,0,'sistemas de informacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90671000000','101','1','4',6,50,0,'ciencias de la computacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90673000000','101','1','4',9,50,0,'sistemas de informacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90679000000','101','1','4',9,50,0,'sistemas de informacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90709000000','101','1','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90710000000','101','1','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90721000000','101','1','4',8,50,0,'sistemas de informacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90932000000','101','1','4',5,50,0,'sistemas de informacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90934000000','101','1','4',9,50,0,'tecnologia de informacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90971000000','101','1','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91114000000','101','1','4',2,50,0,'ciencias de la computacion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91115000000','101','1','4',3,50,0,'tecnologia de informacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91121000000','101','1','4',7,50,0,'ingenieria de software','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91149000000','101','1','4',4,50,0,'tecnologia de inforamcion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91155000000','101','1','4',2,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91274000000','101','1','4',2,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91276000000','101','1','4',3,50,0,'sistemas de informacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91279000000','101','1','4',5,50,0,'tecnologia de información','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91288000000','101','1','4',8,50,0,'tecnologia de informacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91349000000','101','1','4',10,50,0,'gestion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90002000000','202','2','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90003000000','202','2','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90005000000','202','2','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90020000000','202','2','4',2,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90034000000','202','2','4',10,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90053000000','202','2','4',3,50,0,'ciencias de la computacion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90054000000','202','2','4',3,50,0,'metodos cuantitativos','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90056000000','202','2','4',3,50,0,'matematicas y ciencias','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90060000000','202','2','4',4,50,0,'metodos cuantitativos','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90072000000','202','2','4',4,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90074000000','202','2','4',4,50,0,'matematicas y ciencias','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90077000000','202','2','4',4,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90085000000','202','2','4',6,50,0,'metodos cuantitativos','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90086000000','202','2','4',5,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90087000000','202','2','4',5,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90090000000','202','2','4',5,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90116000000','202','2','4',7,50,0,'metodos cuantitativos','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90118000000','202','2','4',6,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90122000000','202','2','4',5,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90131000000','202','2','4',6,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90132000000','202','2','4',7,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90134000000','202','2','4',7,50,0,'industrial','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90136000000','202','2','4',6,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90140000000','202','2','4',7,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90141000000','202','2','4',8,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90145000000','202','2','4',7,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90156000000','202','2','4',9,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90164000000','202','2','4',8,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90165000000','202','2','4',9,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90170000000','202','2','4',8,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90177000000','202','2','4',3,50,0,'industrial','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90366000000','202','2','4',2,50,0,'matematicas y ciencias','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90412000000','202','2','4',4,50,0,'matematicas y ciencias','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90548000000','202','2','4',8,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90655000000','202','2','4',2,50,0,'matematicas y ciencias','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90656000000','202','2','4',3,50,0,'matematicas y ciencias','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90661000000','202','2','4',2,50,0,'computacion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90662000000','202','2','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90663000000','202','2','4',1,50,0,'matematicas y ciencias','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90668000000','202','2','4',1,50,0,'matematicas y ciencias','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90681000000','202','2','4',10,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90682000000','202','2','4',8,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90683000000','202','2','4',9,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90690000000','202','2','4',9,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90691000000','202','2','4',10,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90709000000','202','2','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90710000000','202','2','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90849000000','202','2','4',9,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90971000000','202','2','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91114000000','202','2','4',2,50,0,'ciencias de la computacion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91127000000','202','2','4',8,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91142000000','202','2','4',5,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91143000000','202','2','4',7,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91144000000','202','2','4',10,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91155000000','202','2','4',2,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91274000000','202','2','4',2,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91280000000','202','2','4',6,50,0,'industrial','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90002000000','303','3','4',1,50,0,'generales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90005000000','303','3','4',1,50,0,'generales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90020000000','303','3','4',2,50,0,'generales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90667000000','303','3','4',7,50,0,'generales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90709000000','303','3','4',1,50,0,'generales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90710000000','303','3','4',1,50,0,'generales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90873000000','303','3','4',1,50,0,'diseño y urbanismo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90875000000','303','3','4',1,50,0,'expresion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90876000000','303','3','4',2,50,0,'diseño y urbanismo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90879000000','303','3','4',2,50,0,'expresion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90883000000','303','3','4',3,50,0,'edificacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90886000000','303','3','4',3,50,0,'expresion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90889000000','303','3','4',4,50,0,'edificacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90892000000','303','3','4',4,50,0,'expresion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90893000000','303','3','4',5,50,0,'diseño y urbanismo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90897000000','303','3','4',6,50,0,'diseño y urbanismo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90900000000','303','3','4',6,50,0,'expresion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90901000000','303','3','4',7,50,0,'diseño y urbanismo','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90904000000','303','3','4',8,50,0,'diseño y urbanismo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90905000000','303','3','4',8,50,0,'edificacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90906000000','303','3','4',8,50,0,'generales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90971000000','303','3','4',1,50,0,'generales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91155000000','303','3','4',2,50,0,'generales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91274000000','303','3','4',5,50,0,'generales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91275000000','303','3','4',2,50,0,'edificacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91310000000','303','3','4',1,50,0,'generales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91312000000','303','3','4',2,50,0,'generales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91313000000','303','3','4',2,50,0,'generales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91314000000','303','3','4',3,50,0,'generales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91315000000','303','3','4',3,50,0,'edificacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91316000000','303','3','4',3,50,0,'diseño y urbanismo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91317000000','303','3','4',4,50,0,'edificacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91318000000','303','3','4',4,50,0,'expresion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91319000000','303','3','4',4,50,0,'historia, teoria y critica','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91320000000','303','3','4',4,50,0,'diseño y urbanismo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91322000000','303','3','4',5,50,0,'historia, teoria y critica','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91324000000','303','3','4',5,50,0,'edificacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91325000000','303','3','4',5,50,0,'diseño y urbanismo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91326000000','303','3','4',6,50,0,'historia, teoria y critica','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91328000000','303','3','4',6,50,0,'edificacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91329000000','303','3','4',6,50,0,'diseño y urbanismo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91330000000','303','3','4',7,50,0,'edificacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91331000000','303','3','4',7,50,0,'historia, teoria y critica','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91332000000','303','3','4',7,50,0,'diseño y urbanismo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91333000000','303','3','4',8,50,0,'historia, teoria y critica','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91334000000','303','3','4',9,50,0,'edificacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91335000000','303','3','4',9,50,0,'diseño y urbanismo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91336000000','303','3','4',10,50,0,'diseño y urbanismo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91337000000','303','3','4',10,50,0,'diseño y urbanismo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90002000000','404','4','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90003000000','404','4','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90005000000','404','4','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90020000000','404','4','4',2,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90034000000','404','4','4',10,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90037000000','404','4','4',3,50,0,'cursos instrumentales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90049000000','404','4','4',4,50,0,'construccion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90054000000','404','4','4',3,50,0,'cursos instrumentales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90056000000','404','4','4',3,50,0,'cursos instrumentales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90067000000','404','4','4',5,50,0,'construccion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90074000000','404','4','4',4,50,0,'cursos instrumentales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90096000000','404','4','4',7,50,0,'hidraulica','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90122000000','404','4','4',5,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90131000000','404','4','4',6,50,0,'construccion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90145000000','404','4','4',7,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90251000000','404','4','4',2,50,0,'transportes','obligatorio','especificos','presencial',NULL,NULL,NULL,NULL,NULL),('90254000000','404','4','4',4,50,0,'estructuras','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90256000000','404','4','4',4,50,0,'hidraulica','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90260000000','404','4','4',5,50,0,'estructuras','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90261000000','404','4','4',6,50,0,'transportes','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90265000000','404','4','4',6,50,0,'hidraulica','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90266000000','404','4','4',6,50,0,'estructuras','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90267000000','404','4','4',7,50,0,'transportes','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90268000000','404','4','4',4,50,0,'construccion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90269000000','404','4','4',7,50,0,'hidraulica','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90271000000','404','4','4',7,50,0,'estructuras','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90275000000','404','4','4',8,50,0,'estructuras','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90282000000','404','4','4',5,50,0,'transportes','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90285000000','404','4','4',9,50,0,'estructuras','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90286000000','404','4','4',9,50,0,'estructuras','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90295000000','404','4','4',8,50,0,'estructuras','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90309000000','404','4','4',9,50,0,'hidraulica','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90366000000','404','4','4',2,50,0,'cursos instrumentales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90412000000','404','4','4',4,50,0,'cursos instrumentales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90548000000','404','4','4',8,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90595000000','404','4','4',8,50,0,'construccion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90596000000','404','4','4',8,50,0,'hidraulica','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90597000000','404','4','4',5,50,0,'estructuras','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90655000000','404','4','4',2,50,0,'cursos instrumentales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90656000000','404','4','4',3,50,0,'cursos instrumentales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90661000000','404','4','4',2,50,0,'cursos instrumentales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90662000000','404','4','4',1,50,0,'cursos instrumentales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90663000000','404','4','4',1,50,0,'cursos instrumentales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90668000000','404','4','4',1,50,0,'cursos instrumentales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90709000000','404','4','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90710000000','404','4','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90938000000','404','4','4',5,50,0,'construccion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90939000000','404','4','4',3,50,0,'transportes','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90971000000','404','4','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91155000000','404','4','4',2,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91274000000','404','4','4',2,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91275000000','404','4','4',2,50,0,'transportes','obligatorio','especificos','presencial',NULL,NULL,NULL,NULL,NULL),('91277000000','404','4','4',3,50,0,'estructuras','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91281000000','404','4','4',6,50,0,'transportes','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91289000000','404','4','4',9,50,0,'hidraulica','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91290000000','404','4','4',9,50,0,'estructuras','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91292000000','404','4','4',10,50,0,'humanidades','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91293000000','404','4','4',10,50,0,'humanidades','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91294000000','404','4','4',10,50,0,'construccion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91295000000','404','4','4',10,50,0,'gestion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90002000000','505','5','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90003000000','505','5','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90005000000','505','5','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90020000000','505','5','4',2,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90034000000','505','5','4',10,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90037000000','505','5','4',3,50,0,'matematicas y ciencias basicas','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90053000000','505','5','4',3,50,0,'computacion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90054000000','505','5','4',3,50,0,'matematicas y ciencias basicas','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90056000000','505','5','4',3,50,0,'matematicas y ciencias basicas','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90068000000','505','5','4',4,50,0,'comunicaciones y redes','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90074000000','505','5','4',4,50,0,'matematicas y ciencias basicas','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90077000000','505','5','4',4,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90081000000','505','5','4',6,50,0,'sistemas de control y automatizacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90082000000','505','5','4',6,50,0,'comunicaciones y redes','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90083000000','505','5','4',5,50,0,'comunicaciones y redes','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90090000000','505','5','4',5,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90091000000','505','5','4',5,50,0,'matematicas y ciencias basicas','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90109000000','505','5','4',7,50,0,'sistemas de control y automatizacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90110000000','505','5','4',5,50,0,'sistemas digitales','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90111000000','505','5','4',7,50,0,'comunicaciones y redes','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90112000000','505','5','4',6,50,0,'comunicaciones y redes','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90125000000','505','5','4',7,50,0,'sistemas electricos y electronicos','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90126000000','505','5','4',7,50,0,'sistemas de control y automatizacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90127000000','505','5','4',6,50,0,'sistemas digitales','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90128000000','505','5','4',8,50,0,'comunicaciones y redes','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90148000000','505','5','4',7,50,0,'sistemas digitales','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90150000000','505','5','4',8,50,0,'sistemas electricos y electronicos','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90151000000','505','5','4',8,50,0,'sistemas de control y automatizacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90158000000','505','5','4',8,50,0,'sistemas digitales','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90171000000','505','5','4',9,50,0,'sistemas de control y automatizacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90197000000','505','5','4',9,50,0,'sistemas electricos y electronicos','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90213000000','505','5','4',8,50,0,'sistemas de control y automatizacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90366000000','505','5','4',2,50,0,'matematicas y ciencias basicas','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90412000000','505','5','4',4,50,0,'matematicas y ciencias basicas','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90655000000','505','5','4',2,50,0,'matematicas y ciencias basicas','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90656000000','505','5','4',3,50,0,'matematicas y ciencias basicas','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90661000000','505','5','4',2,50,0,'computacion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90662000000','505','5','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90663000000','505','5','4',1,50,0,'matematicas y ciencias basicas','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90667000000','505','5','4',9,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90668000000','505','5','4',1,50,0,'matematicas y ciencias basicas','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90694000000','505','5','4',4,50,0,'comunicaciones y redes','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90695000000','505','5','4',5,50,0,'matematicas y ciencias basicas','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90696000000','505','5','4',6,50,0,'sistemas de control y automatizacion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90697000000','505','5','4',10,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90698000000','505','5','4',9,50,0,'diseño de sistemas digitales','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90699000000','505','5','4',10,50,0,'diseño de sistemas electronicos','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90700000000','505','5','4',9,50,0,'comunicaciones y redes','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90709000000','505','5','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90710000000','505','5','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90971000000','505','5','4',1,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91114000000','505','5','4',2,50,0,'computacion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91155000000','505','5','4',2,50,0,'humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91274000000','505','5','4',2,50,0,'gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90002000000','606','6','4',1,50,0,'investigacion y humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90005000000','606','6','4',1,50,0,'investigacion y humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90020000000','606','6','4',2,50,0,'investigacion y humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90034000000','606','6','4',5,50,0,'investigacion y humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90037000000','606','6','4',1,50,0,'quimica biologia','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90090000000','606','6','4',5,50,0,'economia y gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90134000000','606','6','4',6,50,0,'economia y gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90141000000','606','6','4',6,50,0,'economia y gestion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90177000000','606','6','4',4,50,0,'matematicas e informatica','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90201000000','606','6','4',7,50,0,'economia y gestion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90210000000','606','6','4',9,50,0,'economia y gestion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90548000000','606','6','4',8,50,0,'economia y gestion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90635000000','606','6','4',1,50,0,'quimica biologia','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90655000000','606','6','4',2,50,0,'matematicas e informatica','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90656000000','606','6','4',3,50,0,'matematicas e informatica','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90661000000','606','6','4',2,50,0,'matematicas e informatica','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90662000000','606','6','4',1,50,0,'matematicas e informatica','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90663000000','606','6','4',1,50,0,'matematicas e informatica','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90668000000','606','6','4',1,50,0,'matematicas e informatica','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90697000000','606','6','4',6,50,0,'economia y gestion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90709000000','606','6','4',1,50,0,'economia y gestion','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90710000000','606','6','4',1,50,0,'investigacion y humanidades','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90803000000','606','6','4',7,50,0,'ciencias alimentarias','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90804000000','606','6','4',5,50,0,'ciencias alimentarias','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90805000000','606','6','4',2,50,0,'quimica biologia','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90806000000','606','6','4',3,50,0,'quimica biologia','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90808000000','606','6','4',8,50,0,'ciencias alimentarias','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90809000000','606','6','4',7,50,0,'ingenieria de alimentos','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90810000000','606','6','4',10,50,0,'ingenieria de alimentos','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90811000000','606','6','4',6,50,0,'ingenieria de alimentos','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90812000000','606','6','4',3,50,0,'matematicas e informatica','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90813000000','606','6','4',4,50,0,'ciencias alimentarias','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90814000000','606','6','4',7,50,0,'economia y gestion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90817000000','606','6','4',7,50,0,'ingenieria de alimentos','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90818000000','606','6','4',8,50,0,'ingenieria de alimentos','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90820000000','606','6','4',6,50,0,'ingenieria de alimentos','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90821000000','606','6','4',3,50,0,'quimica biologia','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90822000000','606','6','4',4,50,0,'quimica biologia','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90823000000','606','6','4',9,50,0,'ingenieria de alimentos','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90824000000','606','6','4',4,50,0,'matematicas e informatica','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90825000000','606','6','4',4,50,0,'quimica biologia','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90826000000','606','6','4',5,50,0,'ciencias alimentarias','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90829000000','606','6','4',4,50,0,'ciencias alimentarias','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90830000000','606','6','4',2,50,0,'quimica biologia','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90831000000','606','6','4',3,50,0,'quimica biologia','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90832000000','606','6','4',10,50,0,'tecnologia de alimentos','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90835000000','606','6','4',7,50,0,'tecnologia de alimentos','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90836000000','606','6','4',8,50,0,'tecnologia de alimentos','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90837000000','606','6','4',9,50,0,'tecnologia de alimentos','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90841000000','606','6','4',5,50,0,'ingenieria de alimentos','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90847000000','606','6','4',2,50,0,'quimica biologia','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90864000000','606','6','4',3,50,0,'matematicas e informatica','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90865000000','606','6','4',2,50,0,'quimica biologia','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90866000000','606','6','4',3,50,0,'quimica biologia','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90943000000','606','6','4',4,50,0,'economia y gestion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90949000000','606','6','4',4,50,0,'ciencias alimentarias','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90971000000','606','6','4',1,50,0,'idiomas','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91028000000','606','6','4',3,50,0,'investigacion y humanidades','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91029000000','606','6','4',5,50,0,'ciencias alimentarias','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91030000000','606','6','4',5,50,0,'ciencias alimentarias','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91037000000','606','6','4',9,50,0,'ingenieria de alimentos','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91038000000','606','6','4',9,50,0,'economia y gestion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91039000000','606','6','4',10,50,0,'ciencias alimentarias','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91040000000','606','6','4',10,50,0,'economia y gestion','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91044000000','606','6','4',6,50,0,'ingenieria de alimentos','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91076000000','606','6','4',6,50,0,'ingenieria de alimentos','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91077000000','606','6','4',6,50,0,'ingenieria de alimentos','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91078000000','606','6','4',7,50,0,'ciencias alimentarias','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91093000000','606','6','4',5,50,0,'ingenieria de alimentos','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91114000000','606','6','4',2,50,0,'matematicas e informatica','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91123000000','606','6','4',2,50,0,'matematicas e informatica','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91155000000','606','6','4',2,50,0,'idiomas','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90002000000','707','7','4',1,50,0,'estudios generales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90005000000','707','7','4',1,50,0,'actividades e ingles','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90020000000','707','7','4',2,50,0,'actividades e ingles','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90034000000','707','7','4',10,50,0,'ciencias administrativas y economicas','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90122000000','707','7','4',5,50,0,'ciencias administrativas y economicas','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90134000000','707','7','4',6,50,0,'ciencias administrativas y economicas','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90458000000','707','7','4',7,50,0,'ciencias administrativas y economicas','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90617000000','707','7','4',10,50,0,'ciencias administrativas y economicas','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90667000000','707','7','4',6,50,0,'ciencias administrativas y economicas','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90709000000','707','7','4',1,50,0,'estudios generales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90710000000','707','7','4',1,50,0,'estudios generales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90971000000','707','7','4',1,50,0,'actividades e ingles','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('90972000000','707','7','4',2,50,0,'ciencias aeronauticas e instruccion en vuelo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90976000000','707','7','4',3,50,0,'ciencias aeronauticas e instruccion en vuelo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90977000000','707','7','4',2,50,0,'ciencias aeronauticas e instruccion en vuelo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90978000000','707','7','4',3,50,0,'ciencias aeronauticas e instruccion en vuelo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90991000000','707','7','4',4,50,0,'ciencias aeronauticas e instruccion en vuelo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('90994000000','707','7','4',1,50,0,'ciencias aeronauticas e instruccion en vuelo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91155000000','707','7','4',2,50,0,'actividades e ingles','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91176000000','707','7','4',8,50,0,'ciencias administrativas y economicas','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91179000000','707','7','4',9,50,0,'ciencias administrativas y economicas','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91181000000','707','7','4',9,50,0,'ciencias administrativas y economicas','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91274000000','707','7','4',5,50,0,'ciencias administrativas y economicas','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91310000000','707','7','4',1,50,0,'estudios generales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91311000000','707','7','4',1,50,0,'gestion aeronautica','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91312000000','707','7','4',2,50,0,'estudios generales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91314000000','707','7','4',1,50,0,'estudios generales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91321000000','707','7','4',2,50,0,'estudios generales','obligatorio','general','presencial',NULL,NULL,NULL,NULL,NULL),('91348000000','707','7','4',6,50,0,'ciencias administrativas y economicas','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91351000000','707','7','4',2,50,0,'ciencias aeronauticas e instruccion en vuelo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91352000000','707','7','4',3,50,0,'ciencias aeronauticas e instruccion en vuelo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91353000000','707','7','4',6,50,0,'gestion aeronautica','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91354000000','707','7','4',7,50,0,'ciencias administrativas y economicas','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91355000000','707','7','4',2,50,0,'ciencias aeronauticas e instruccion en vuelo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91356000000','707','7','4',3,50,0,'ciencias aeronauticas e instruccion en vuelo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91357000000','707','7','4',4,50,0,'ciencias aeronauticas e instruccion en vuelo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91358000000','707','7','4',4,50,0,'ciencias administrativas y economicas','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91359000000','707','7','4',4,50,0,'ciencias administrativas y economicas','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91362000000','707','7','4',5,50,0,'ciencias administrativas y economicas','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91367000000','707','7','4',6,50,0,'ciencias administrativas y economicas','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91368000000','707','7','4',7,50,0,'ciencias administrativas y economicas','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91369000000','707','7','4',8,50,0,'ciencias administrativas y economicas','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91370000000','707','7','4',9,50,0,'gestiona aeronautica','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91371000000','707','7','4',10,50,0,'gestion aeronautica','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91381000000','707','7','4',5,50,0,'gestion aeronautica','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91382000000','707','7','4',8,50,0,'ciencias administrativas y economicas','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91383000000','707','7','4',3,50,0,'ciencias aeronauticas e instruccion en vuelo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91384000000','707','7','4',3,50,0,'ciencias aeronauticas e instruccion en vuelo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91385000000','707','7','4',4,50,0,'ciencias aeronauticas e instruccion en vuelo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91386000000','707','7','4',4,50,0,'ciencias aeronauticas e instruccion en vuelo','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91387000000','707','7','4',5,50,0,'ciencias administrativas y economicas','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91388000000','707','7','4',7,50,0,'ciencias administrativas y economicas','obligatorio','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91389000000','101','1','4',0,25,0,'Ing Software','Electivo','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('91390000000','101','1','4',0,25,0,'Ing Software','Electivo','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('090861E1040','101','1','4',11,25,0,'Ing Software','Electivo','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('090658E3040','101','1','4',13,25,0,'Ing Software','Electivo','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('090147E4020','101','1','4',14,25,0,'Ing Software','Electivo','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('090659E3040','101','1','4',13,25,0,'Ing Software','Electivo','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('090660E3040','101','1','4',13,25,0,'Ing Software','Electivo','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('091126E4040','101','1','4',14,25,0,'Ing Software','Electivo','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('090933E1040','101','1','4',11,25,0,'Ing Software','Electivo','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('091217E4020','101','1','4',14,25,0,'Ing Software','Electivo','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('090672E3040','101','1','4',13,25,0,'Ing Software','Electivo','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('091124E3040','101','1','4',13,25,0,'Ing Software','Electivo','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('090675E2040','101','1','4',12,25,0,'Ing Software','Electivo','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('090676E2040','101','1','4',12,25,0,'Ing Software','Electivo','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('090677E2040','101','1','4',12,25,0,'Ing Software','Electivo','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('090678E2040','101','1','4',12,25,0,'Ing Software','Electivo','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('090862E2040','101','1','4',12,25,0,'Ing Software','Electivo','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('091119E1040','101','1','4',11,25,0,'Ing Software','Electivo','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('091119E1040','101','1','4',11,25,0,'Ing Software','Electivo','especifico','presencial',NULL,NULL,NULL,NULL,NULL),('091125E4020','101','1','4',14,25,0,'Ing Software','Electivo','especifico','presencial',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `det_curso_esc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_doc_cur`
--

DROP TABLE IF EXISTS `det_doc_cur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_doc_cur` (
  `id` int(11) NOT NULL,
  `id_doc` bigint(12) NOT NULL,
  `id_cur` varchar(12) NOT NULL,
  `descri` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_doc_idx` (`id_doc`),
  KEY `fk_id_cur_idx` (`id_cur`),
  KEY `ind_numdoc` (`id_doc`),
  KEY `fk_id_cur_idx1` (`id_cur`),
  KEY `ind_numdoc1` (`id_doc`),
  CONSTRAINT `fk_id_cur` FOREIGN KEY (`id_cur`) REFERENCES `curso` (`idcurso`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_id_doc_1` FOREIGN KEY (`id_doc`) REFERENCES `docente` (`numdoc`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_doc_cur`
--

LOCK TABLES `det_doc_cur` WRITE;
/*!40000 ALTER TABLE `det_doc_cur` DISABLE KEYS */;
/*!40000 ALTER TABLE `det_doc_cur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_doc_referido`
--

DROP TABLE IF EXISTS `det_doc_referido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_doc_referido` (
  `codDet_ref` varchar(12) NOT NULL,
  `codDet_doc` bigint(11) NOT NULL,
  KEY `idDr_ref_idx` (`codDet_ref`),
  KEY `idDr_doc_idx` (`codDet_doc`),
  CONSTRAINT `idDr_doc` FOREIGN KEY (`codDet_doc`) REFERENCES `docente` (`numdoc`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idDr_ref` FOREIGN KEY (`codDet_ref`) REFERENCES `referido` (`idreferido`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_doc_referido`
--

LOCK TABLES `det_doc_referido` WRITE;
/*!40000 ALTER TABLE `det_doc_referido` DISABLE KEYS */;
/*!40000 ALTER TABLE `det_doc_referido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_interes_docente`
--

DROP TABLE IF EXISTS `det_interes_docente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_interes_docente` (
  `codOi_Inte` int(11) NOT NULL,
  `codOi_doc` bigint(12) NOT NULL,
  `codOi_sem` varchar(12) NOT NULL,
  KEY `codOi_sem_idx` (`codOi_sem`),
  KEY `codOi_sem_idx1` (`codOi_doc`),
  KEY `codOi_int_idx` (`codOi_Inte`),
  CONSTRAINT `codOi_doc` FOREIGN KEY (`codOi_doc`) REFERENCES `docente` (`numdoc`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `codOi_int` FOREIGN KEY (`codOi_Inte`) REFERENCES `intereses` (`idIntereses`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `codOi_sem` FOREIGN KEY (`codOi_sem`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_interes_docente`
--

LOCK TABLES `det_interes_docente` WRITE;
/*!40000 ALTER TABLE `det_interes_docente` DISABLE KEYS */;
/*!40000 ALTER TABLE `det_interes_docente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_seccion`
--

DROP TABLE IF EXISTS `det_seccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_seccion` (
  `codDs_sec` varchar(12) NOT NULL,
  `codDs_doc` int(11) NOT NULL,
  `turno` varchar(45) DEFAULT NULL,
  `actividad` varchar(45) DEFAULT NULL,
  `totMatr` int(11) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  KEY `idDs_secc_idx` (`codDs_sec`),
  KEY `idDs_doc_idx` (`codDs_doc`),
  CONSTRAINT `idDs_doc` FOREIGN KEY (`codDs_doc`) REFERENCES `docente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idDs_secc` FOREIGN KEY (`codDs_sec`) REFERENCES `seccion` (`idseccion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_seccion`
--

LOCK TABLES `det_seccion` WRITE;
/*!40000 ALTER TABLE `det_seccion` DISABLE KEYS */;
/*!40000 ALTER TABLE `det_seccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_usu_esc`
--

DROP TABLE IF EXISTS `det_usu_esc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_usu_esc` (
  `idUsuario` varchar(12) NOT NULL,
  `idEscuela` varchar(12) NOT NULL,
  `idPerfl` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`idUsuario`,`idEscuela`),
  KEY `fk_idesc_idx` (`idEscuela`),
  KEY `fk_perfilUsuario_idx` (`idPerfl`),
  CONSTRAINT `fk_idesc` FOREIGN KEY (`idEscuela`) REFERENCES `escuela` (`idescuela`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_idusu` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_perfilUsuario` FOREIGN KEY (`idPerfl`) REFERENCES `perfil` (`idperfil`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_usu_esc`
--

LOCK TABLES `det_usu_esc` WRITE;
/*!40000 ALTER TABLE `det_usu_esc` DISABLE KEYS */;
INSERT INTO `det_usu_esc` VALUES ('COD001','999','1'),('COD002','101','2');
/*!40000 ALTER TABLE `det_usu_esc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `disponibilidad_docente`
--

DROP TABLE IF EXISTS `disponibilidad_docente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disponibilidad_docente` (
  `id` int(11) NOT NULL,
  `cod_Docente` bigint(12) NOT NULL,
  `cod_semestre` varchar(45) NOT NULL,
  `hor_ini` int(11) DEFAULT NULL,
  `hor_fin` int(11) DEFAULT NULL,
  `dia` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `codDd_docente_idx` (`cod_Docente`),
  KEY `codDd_semestre_idx` (`cod_semestre`),
  CONSTRAINT `codDd_docente` FOREIGN KEY (`cod_Docente`) REFERENCES `docente` (`numdoc`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `codDd_semestre` FOREIGN KEY (`cod_semestre`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `disponibilidad_docente`
--

LOCK TABLES `disponibilidad_docente` WRITE;
/*!40000 ALTER TABLE `disponibilidad_docente` DISABLE KEYS */;
/*!40000 ALTER TABLE `disponibilidad_docente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `docente`
--

DROP TABLE IF EXISTS `docente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numdoc` bigint(12) NOT NULL,
  `apepat` varchar(45) NOT NULL,
  `apemat` varchar(45) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `pais` varchar(45) DEFAULT NULL,
  `fecini` date NOT NULL,
  `ley30220` tinyint(2) NOT NULL,
  `mayorgrado` varchar(45) NOT NULL,
  `menciongrado` varchar(45) NOT NULL,
  `univ` varchar(100) NOT NULL,
  `paisuniv` varchar(45) NOT NULL,
  `pregrado` tinyint(1) DEFAULT NULL,
  `maestria` tinyint(1) DEFAULT NULL,
  `doctorado` tinyint(1) DEFAULT NULL,
  `categoria` varchar(50) DEFAULT NULL,
  `regimen` varchar(45) DEFAULT NULL,
  `horaclase` int(2) NOT NULL,
  `horaactiv` int(2) NOT NULL,
  `totalhoras` int(2) NOT NULL,
  `investigador` tinyint(2) NOT NULL,
  `DINA` tinyint(1) DEFAULT NULL,
  `codD_sem` varchar(12) NOT NULL,
  `escuela` varchar(45) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telefono` int(15) DEFAULT NULL,
  `observaciones` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idD_sem_idx` (`codD_sem`),
  KEY `IND_NUMDOC` (`numdoc`),
  CONSTRAINT `idD_sem` FOREIGN KEY (`codD_sem`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docente`
--

LOCK TABLES `docente` WRITE;
/*!40000 ALTER TABLE `docente` DISABLE KEYS */;
INSERT INTO `docente` VALUES (1,12345678,'Abad','Escalante','Juan Carlos','Peru','2012-02-15',1,'Doctor','Administración','Usmp','PERU',1,0,1,'CONTRATADO','Tiempo Completo',16,4,20,1,0,'4','',NULL,NULL,NULL),(2,87654321,'Acosta','Acosta','Willian Sergio','Peru','2010-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(3,87654322,'Aguero','Martinez La Rosa','Julio Cesar','Peru','2010-03-17',0,'Titulo','Bachiller en Ciencias','Upc','PERU',1,1,0,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(4,87654320,'Acuña','Flores','Carlos Christian','Peru','2015-04-16',1,'Maestro','Bachiller en Biología','Unmsm','PERU',1,1,1,'CONTRATADO','Tiempo Completo',10,2,12,1,0,'4','',NULL,NULL,NULL),(5,87654323,'Aguirre','Medrano','Rosa Virginia','Peru','2009-03-17',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(6,87654324,'Alarco','Jerí','Iván Erick','Peru','2010-04-16',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(7,87654325,'Alarcon','Centti','Jose','Peru','2010-05-16',1,'Doctor','Bachiller en Economia','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(8,87654326,'Alegria','Vidal','Rosa Mercedes','Peru','2010-06-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(9,87654327,'Alferrano','Donofrio','Mirtha','Peru','2010-07-16',1,'Doctor','Bachiller en Ciencias','Utp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(10,87654328,'Ambia','Rebatta','Hugo Paulino','Peru','2010-08-16',1,'Doctor','Bachiller en Derecho','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(11,87654329,'Amoros','Figueroa','Rodrigo','Peru','2010-09-16',1,'Doctor','Bachiller en Ciencias','Unmsm','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(12,87654311,'Añaños','Gomez Sanchez','Roberto Enrique','Peru','2010-10-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(13,87654331,'Ara','Rojas','Silvia Liliana','Peru','2010-11-16',1,'Doctor','Bachiller en Educacion','Utp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(14,87654341,'Arrieta','Freyre','Javier Eduardo','Peru','2010-12-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(15,87654351,'Arrieta','Taboada','Amanda','Peru','2010-03-01',1,'Doctor','Bachiller en Ciencias','Uni','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(16,87654361,'Arriola','Guevara','Luis Alberto','Peru','2010-03-02',1,'Doctor','Bachiller en Ciencias','Uni','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(17,87654371,'Bacigalupo','Olivari','Miguel Angel','Peru','2010-03-03',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(18,87654381,'Balcazar','Hernandez','Yudy','Peru','2010-03-04',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(19,87654391,'Ballena','Gonzales','Manuel','Peru','2010-03-05',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(20,87654301,'Balta','Rospliglosi','Manuel Valeriano','Peru','2010-03-06',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(21,87654121,'Balta','Vargas-Machuca','Manuel','Peru','2010-03-07',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(22,87654221,'Barnet','Champommier','Yann Oliver','Peru','2010-03-08',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(23,87654421,'Barnett','Mendoza','Edy Dalmiro','Peru','2010-03-09',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(24,87654521,'Barrantes','Mann','Luis Alfonso','Peru','2010-03-10',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(25,87654621,'Barraza','Salguero','Victor Eduardo','Peru','2010-03-11',1,'Doctor','Bachiller en Ciencias','Utp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(26,87654721,'Barreto','Bardales','Tomas Fernando','Peru','2010-03-12',1,'Doctor','Bachiller en Ciencias','ULima','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(27,87654821,'Basile','Migliore','Jose Antonio','Peru','2010-03-20',1,'Doctor','Bachiller en Ciencias','Uni','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(28,87654921,'Bayona','Ore','Luz Sussy','Peru','2010-03-21',1,'Doctor','Bachiller en Ciencias','Uni','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(29,87654021,'Becerra','Pacherres','Augusto Oscar','Peru','2010-03-22',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(30,87651321,'Bedia','Guillen','Ciro Sergio','Peru','2010-03-23',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(31,87652321,'Benavente','Villena','Maria Elena','Peru','2010-03-24',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(32,87653321,'Benites','Gonzales','Willian Sergio','Peru','2010-03-25',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(33,87655321,'Benites','Vilela','Luis Fernando','Peru','2010-03-26',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(34,87656321,'Bernabel','Liza','Ana Maria','Peru','2010-03-27',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(35,87657321,'Bernal','Ortiz','Carlos Adolfo','Peru','2010-03-28',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(36,87658321,'Bernuy','Alva','Augusto Ernesto','Peru','2010-03-29',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(37,87659321,'Bertolotti','Zuñiga','Carmen Rosa','Peru','2010-03-30',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(38,87650321,'Bezada','Sanchez','Cesar Alfredo','Peru','2011-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(39,87614321,'Blanco','Zuñiga','Yvan','Peru','2012-03-16',1,'Doctor','Bachiller en Ciencias','Unmsm','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(40,87624321,'Bocangel','Weydert','Guillermo Augusto','Peru','2013-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(41,87634321,'Buendia','Rios','Hildebrando','Peru','2014-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(42,87644321,'Bustos','Diaz','Silverio','Peru','2015-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(43,87664321,'Cabrera','Iturria','Ricardo','Peru','2016-03-16',1,'Doctor','Bachiller en Ciencias','Utp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(44,87674321,'Caceres','Echegaray','Hector Alejandro','Peru','2009-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(45,87684321,'Caceres','Lampen','Manuel Alejandro','Peru','2008-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(46,87694321,'Calderon','Caceres','Jose Luis','Peru','2007-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(47,87604321,'Campos','Perez','Rosalvina','Peru','2006-03-16',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(48,87154321,'Cano','Tejada','Jose Antonio','Peru','2005-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(49,87254321,'Caparachin','Chuquihuaraca','Jaime Santos','Peru','2004-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(50,87354321,'Cardenas','Lucero','Luis','Peru','2003-03-16',1,'Doctor','Bachiller en Ciencias','Uni','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(51,87454321,'Cardenas','Martinez','Jose Antonio','Peru','2002-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(52,87554321,'Cardenas','Zavala','Germain Leonardo','Peru','2001-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(53,87754321,'Carpio','Delgado','Guitter Guillermo','Peru','2000-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(54,87854321,'Casavilca','Maldonado','Edmundo','Peru','2010-04-01',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(55,87954321,'Castillo','Cavero','Rodolfo','Peru','2010-04-02',1,'Doctor','Bachiller en Ciencias','ULima','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(56,87054321,'Castillo','Sini','Gustavo Alejandro','Peru','2010-04-03',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(57,81654321,'Castro','Salazar','Fredy Adan','Peru','2010-04-04',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(58,82654321,'Cataño','Espinoza','Humberto Daniel','Peru','2010-04-05',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(59,83654321,'Ccoyure','Tito','Ricardo Wilber','Peru','2010-04-06',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(60,84654321,'Celi','Saavedra','Luis','Peru','2010-04-07',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(61,85654321,'Cerdan','Chavarri','Mario Wilbe','Peru','2010-04-08',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(62,86654321,'Cerpa','Espinosa','David','Peru','2010-04-09',1,'Doctor','Bachiller en Ciencias','Ucv','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(63,88654321,'Cerron','Ruiz','Eulogio','Peru','2010-04-10',1,'Doctor','Bachiller en Ciencias','Uap','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(64,89654321,'Cevallos','Echevarria','Alejandro Nestor','Peru','2010-04-11',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(65,80654321,'Chacon','Moscoso','Hugo Liu','Peru','2010-04-12',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(66,17654321,'Chanca','Cortez','Luis Manuel','Peru','2010-04-13',1,'Doctor','Bachiller en Ciencias','Utp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(67,27654321,'Chavarry','Vallejos','Carlos Magno','Peru','2010-04-14',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(68,37654321,'Chavez','Rodriguez','Michael Jesus','Peru','2010-04-15',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(69,47654321,'Cieza','Davila','Javier Eduardo','Peru','2010-04-17',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(70,57654321,'Consigliere','Cevasco','Luis Ricardo','Peru','2010-04-18',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(71,67654321,'Contreras','Villarreal','Luis Wilfredo','Peru','2010-04-19',1,'Doctor','Bachiller en Ciencias','ULima','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(72,77654321,'Cortez','Silva','Dimas Antonio','Peru','2010-04-20',1,'Doctor','Bachiller en Ciencias','Unmsm','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(73,97654321,'Cuadros','Ricra','Ruben Dario','Peru','2010-04-21',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(74,7654321,'De La Vega','Picoaga','Fresia','Peru','2010-04-22',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(75,84654321,'De La Torre','Puente','Maria Elena','Peru','2010-04-23',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(76,87954321,'De Los Rios','Hermoza','Justo Rafael','Peru','2010-04-24',1,'Doctor','Bachiller en Ciencias','Utp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(77,81554321,'De Olarte','Tristan','Jorge Luis','Peru','2010-04-25',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(78,82154321,'Del Carpio','Damian','Christian Carlos','Peru','2010-04-26',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(79,82254321,'Diaz','Tejada','Gabriel Eduardo','Peru','2010-04-27',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(80,82354321,'Durán','Ramirez','Gary Gary','Peru','2010-04-28',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(81,82454321,'Egoavil','La Torre','Victor Raul','Peru','2010-04-29',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(82,82554321,'Elaez','Cisneros','Eliazaf Guillermo','Peru','2010-04-30',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(83,82654321,'Enriquez','Quinde','Benjamin Carlos','Peru','2010-05-01',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(84,82754321,'Esparza','Castillo','Olenka','Peru','2010-05-02',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(85,82854321,'Estela','Benavides','Bertha','Peru','2010-05-03',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(86,82954321,'Falcon','Soto','Arnaldo','Peru','2010-05-04',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(87,83054321,'Fano','Miranda','Gonzalo','Peru','2010-05-05',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(88,83154321,'Figueroa','Lezama','Rafael','Peru','2010-05-06',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(89,83254321,'Figueroa','Revilla','Jorge Martin','Peru','2010-05-07',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(90,83354321,'Florian','Castillo','Tulio Elias','Peru','2010-05-08',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(91,83454321,'Fuentes','Flores','Maximo','Peru','2010-05-09',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(92,83554321,'Galindo','Guerra','Gary Joseph Loui','Peru','2010-05-10',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(93,83654321,'Gallegos','Valderrama','Meriedi','Peru','2010-05-11',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(94,83754321,'Galloso','Gentile','Alberto Cesar','Peru','2010-05-12',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(95,83854321,'Gamarra','Villacorta','Raul','Peru','2010-05-13',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(96,83954321,'Gamboa','Cruzado','Javier Arturo','Peru','2010-05-14',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(97,84054321,'Gamero','Rengifo','Luis Fernando','Peru','2010-05-18',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(98,84154321,'Garcia','Farje','Ruben Osvaldo','Peru','2010-05-17',1,'Doctor','Bachiller en Ciencias','ULima','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(99,84254321,'Garcia','Flores','Elva Nelly','Peru','2010-05-19',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(100,84354321,'Garcia','Guerra','Juan Diego','Peru','2010-05-20',1,'Doctor','Bachiller en Ciencias','Utp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(101,84454321,'Garcia','Lorente','Cesar','Peru','2010-05-21',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(102,84554321,'Garcia','Nuñez','Luz Elena','Peru','2010-05-22',1,'Doctor','Bachiller en Ciencias','Unmsm','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(103,12365479,'Garcia','Sanchez','Marta Pamela','Peru','2008-03-13',1,'Maestría','Bachiller en Ciencias','Upn','PERU',0,1,1,'CONTRATADO','Tiempo Completo',15,8,23,1,0,'4','',NULL,NULL,NULL),(104,88888888,'Geronimo','Vasquez','Alfonso Herminio','Peru','2010-03-15',1,'Doctor','Bachiller en Arquitectura','Usil','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',15,5,20,1,0,'4','',NULL,NULL,NULL),(105,11111111,'Gomero','Cordova','Eduardo Fernando','Peru','2003-03-14',0,'Título','Bachiller en Economía','Ucv','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(106,34532689,'Gonzales','Alva','Luis Eduardo','Peru','2010-03-13',1,'Maestría','Bachiller en Educación','Upn','PERU',1,1,0,'CONTRATADO','Tiempo Completo',14,5,19,0,0,'4','',NULL,NULL,NULL),(107,99999999,'Gonzales','Chavesta','Celso','Peru','1999-03-16',1,'Maestro','Bachiller en Computación y Sistemas','Uap','PERU',0,1,1,'CONTRATADO','Tiempo Parcial',13,5,18,1,0,'4','',NULL,NULL,NULL),(108,17893492,'Gonzales','Sanchez','Juan Julio','Peru','2007-03-10',1,'Título','Bachiller en Derecho','Usil','PERU',1,1,1,'CONTRATADO','Tiempo Completo',12,8,20,1,0,'4','',NULL,NULL,NULL),(109,9431874,'Gonzalo','Silva','Eduardo Ramon','Peru','2004-03-20',1,'Doctor','Bachiller en Ciencias Matemáticas','Uigv','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',15,6,21,1,0,'4','',NULL,NULL,NULL),(110,74073297,'Grandez','Pizarro','Waldy Mercedes','Peru','2012-03-23',1,'Título','Bachiller en Economía','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(111,23148930,'Guerrero','Navarro','Raul Agustin','Peru','2005-03-13',1,'Doctor','Bachiller en Ciencias Contables','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Completo',15,6,21,1,0,'4','',NULL,NULL,NULL),(112,83704341,'Guzmán','Tasayco','Alfonso','Peru','2010-03-18',1,'Maestro','Bachiller en Derecho','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(113,1010101,'Guzman','Rouviros','Julio Alejandro','Peru','2004-03-12',1,'Doctor','Bachiller en Ciencias','Utp','PERU',1,0,1,'CONTRATADO','Tiempo Parcial',13,6,19,1,0,'4','',NULL,NULL,NULL),(123,333333,'Abad','Escalante','Juan Carlos','Perú','2010-02-15',1,'Doctor','Administración','Usmp','Perú',1,0,1,'1','Tiempo Completo',16,4,20,1,0,'4','sistemas',NULL,NULL,'');
/*!40000 ALTER TABLE `docente` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER DocenteAuditoria AFTER INSERT ON docente
  FOR EACH ROW
		INSERT INTO `docente_audi`
(`id_doc`,
`numdoc`,
`apepat`,
`apemat`,
`nombre`,
`pais`,
`fecini`,
`ley30220`,
`mayorgrado`,
`menciongrado`,
`univ`,
`paisuniv`,
`pregrado`,
`maestria`,
`doctorado`,
`categoria`,
`regimen`,
`horaclase`,
`horaactiv`,
`totalhoras`,
`investigador`,
`DINA`,
`codD_sem`,
`escuela`,
`email`,
`telefono`,
`observaciones`
)
VALUES
( New.`id`,
New.`numdoc`,
New.`apepat`,
New.`apemat`,
New.`nombre`,
New.`pais`,
New.`fecini`,
New.`ley30220`,
New.`mayorgrado`,
New.`menciongrado`,
New.`univ`,
New.`paisuniv`,
New.`pregrado`,
New.`maestria`,
New.`doctorado`,
New.`categoria`,
New.`regimen`,
New.`horaclase`,
New.`horaactiv`,
New.`totalhoras`,
New.`investigador`,
New.`DINA`,
New.`codD_sem`,
New.`escuela`,
New.`email`,
New.`telefono`,
New.`observaciones`
) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER Doc_Aud_Upd AFTER UPDATE ON docente
  FOR EACH ROW
		INSERT INTO `docente_audi`
(`id_doc`,
`numdoc`,
`apepat`,
`apemat`,
`nombre`,
`pais`,
`fecini`,
`ley30220`,
`mayorgrado`,
`menciongrado`,
`univ`,
`paisuniv`,
`pregrado`,
`maestria`,
`doctorado`,
`categoria`,
`regimen`,
`horaclase`,
`horaactiv`,
`totalhoras`,
`investigador`,
`DINA`,
`codD_sem`,
`escuela`,
`email`,
`telefono`,
`observaciones`
)
VALUES
( New.`id`,
New.`numdoc`,
New.`apepat`,
New.`apemat`,
New.`nombre`,
New.`pais`,
New.`fecini`,
New.`ley30220`,
New.`mayorgrado`,
New.`menciongrado`,
New.`univ`,
New.`paisuniv`,
New.`pregrado`,
New.`maestria`,
New.`doctorado`,
New.`categoria`,
New.`regimen`,
New.`horaclase`,
New.`horaactiv`,
New.`totalhoras`,
New.`investigador`,
New.`DINA`,
New.`codD_sem`,
New.`escuela`,
New.`email`,
New.`telefono`,
New.`observaciones`
) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `docente_audi`
--

DROP TABLE IF EXISTS `docente_audi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docente_audi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_doc` int(11) NOT NULL,
  `numdoc` bigint(12) NOT NULL,
  `apepat` varchar(45) NOT NULL,
  `apemat` varchar(45) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `pais` varchar(45) DEFAULT NULL,
  `fecini` date NOT NULL,
  `ley30220` tinyint(2) NOT NULL,
  `mayorgrado` varchar(45) NOT NULL,
  `menciongrado` varchar(45) NOT NULL,
  `univ` varchar(100) NOT NULL,
  `paisuniv` varchar(45) NOT NULL,
  `pregrado` tinyint(1) DEFAULT NULL,
  `maestria` tinyint(1) DEFAULT NULL,
  `doctorado` tinyint(1) DEFAULT NULL,
  `categoria` varchar(50) DEFAULT NULL,
  `regimen` varchar(45) DEFAULT NULL,
  `horaclase` int(2) NOT NULL,
  `horaactiv` int(2) NOT NULL,
  `totalhoras` int(2) NOT NULL,
  `investigador` tinyint(2) NOT NULL,
  `DINA` tinyint(1) DEFAULT NULL,
  `codD_sem` varchar(12) NOT NULL,
  `escuela` varchar(45) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telefono` int(15) DEFAULT NULL,
  `observaciones` varchar(45) DEFAULT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_doc` (`id_doc`),
  CONSTRAINT `docente_audi_ibfk_1` FOREIGN KEY (`id_doc`) REFERENCES `docente` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docente_audi`
--

LOCK TABLES `docente_audi` WRITE;
/*!40000 ALTER TABLE `docente_audi` DISABLE KEYS */;
INSERT INTO `docente_audi` VALUES (1,123,12341234,'Abad','Escalante','Juan Carlos','Perú','2010-02-15',1,'Doctor','Administración','Usmp','Perú',1,0,1,'1','Tiempo Completo',16,4,20,1,0,'4','sistemas',NULL,NULL,'','2017-09-17 02:05:18'),(2,123,333333,'Abad','Escalante','Juan Carlos','Perú','2010-02-15',1,'Doctor','Administración','Usmp','Perú',1,0,1,'1','Tiempo Completo',16,4,20,1,0,'4','sistemas',NULL,NULL,'','2017-09-17 03:12:27');
/*!40000 ALTER TABLE `docente_audi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipo`
--

DROP TABLE IF EXISTS `equipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipo` (
  `idequipo` varchar(12) NOT NULL,
  `codE_aula` varchar(12) NOT NULL,
  `modelo` varchar(45) DEFAULT NULL,
  `decripcion` varchar(45) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  PRIMARY KEY (`idequipo`),
  KEY `idE_aula_idx` (`codE_aula`),
  CONSTRAINT `idE_aula` FOREIGN KEY (`codE_aula`) REFERENCES `aula` (`idaula`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipo`
--

LOCK TABLES `equipo` WRITE;
/*!40000 ALTER TABLE `equipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `escuela`
--

DROP TABLE IF EXISTS `escuela`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `escuela` (
  `idescuela` varchar(12) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `facultad` varchar(45) DEFAULT NULL,
  `usu_crea_reg` varchar(45) DEFAULT NULL,
  `fec_crea_reg` datetime DEFAULT NULL,
  `ult_usu_mod_reg` varchar(45) DEFAULT NULL,
  `fec_ult_mod_reg` datetime DEFAULT NULL,
  PRIMARY KEY (`idescuela`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `escuela`
--

LOCK TABLES `escuela` WRITE;
/*!40000 ALTER TABLE `escuela` DISABLE KEYS */;
INSERT INTO `escuela` VALUES ('101','Ingenieria de Computacion y Sistemas','FIA',NULL,NULL,NULL,NULL),('202','Ingenieria Industrial','FIA',NULL,NULL,NULL,NULL),('303','Arquitectura','FIA',NULL,NULL,NULL,NULL),('404','Ingeniería Civil','FIA',NULL,NULL,NULL,NULL),('505','Ingeniería Electrónica','FIA',NULL,NULL,NULL,NULL),('606','Ingeniería en Industrias Alimentarias','FIA',NULL,NULL,NULL,NULL),('707','Ciencias Aeronáuticas','FIA',NULL,NULL,NULL,NULL),('999','Admin','Admin',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `escuela` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horario`
--

DROP TABLE IF EXISTS `horario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horario` (
  `idHorario` int(11) NOT NULL AUTO_INCREMENT,
  `codH_sem` varchar(12) NOT NULL,
  `codH_curso` varchar(12) NOT NULL,
  `codH_doc` int(11) NOT NULL,
  `codH_secc` varchar(12) NOT NULL,
  `codH_aula` varchar(12) NOT NULL,
  `dia` varchar(12) DEFAULT NULL,
  `hora_ini` int(11) DEFAULT NULL,
  `hora_fiN` int(11) DEFAULT NULL,
  `cod_esc` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`idHorario`,`codH_sem`,`codH_curso`,`codH_doc`,`codH_secc`,`codH_aula`),
  KEY `fk_horario_semestre` (`codH_sem`),
  KEY `fk_horario_curso` (`codH_curso`),
  KEY `fk_horario_docente` (`codH_doc`),
  KEY `fk_horario_seccion` (`codH_secc`),
  KEY `fk_horario_aula` (`codH_aula`),
  KEY `fk_horario_escuela` (`cod_esc`),
  CONSTRAINT `fk_horario_aula` FOREIGN KEY (`codH_aula`) REFERENCES `aula` (`idaula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_horario_curso` FOREIGN KEY (`codH_curso`) REFERENCES `curso` (`idcurso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_horario_docente` FOREIGN KEY (`codH_doc`) REFERENCES `docente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_horario_escuela` FOREIGN KEY (`cod_esc`) REFERENCES `escuela` (`idescuela`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_horario_seccion` FOREIGN KEY (`codH_secc`) REFERENCES `seccion` (`idseccion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_horario_semestre` FOREIGN KEY (`codH_sem`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horario`
--

LOCK TABLES `horario` WRITE;
/*!40000 ALTER TABLE `horario` DISABLE KEYS */;
INSERT INTO `horario` VALUES (1,'4','90002000000',1,'50A','13','LUNES',800,930,'101'),(2,'4','90003000000',1,'51A','14','LUNES',800,910,'101'),(3,'4','90002000000',1,'52A','15','MIERCOLES',800,845,'101'),(4,'4','90034000000',2,'53A','1','MARTES',800,845,'101'),(5,'4','90060000000',3,'58A','13','MARTES',800,930,'101'),(6,'4','90037000000',6,'55A','14','MARTES',800,1015,'202'),(7,'4','90049000000',7,'56A','15','LUNES',800,1015,'404'),(8,'4','90053000000',8,'57A','15','LUNES',845,1100,'101'),(9,'4','90054000000',2,'54A','13','MARTES',845,1015,'101'),(10,'4','90056000000',10,'59A','14','LUNES',930,1100,NULL),(11,'4','90060000000',11,'60B','13','MIERCOLES',800,930,NULL),(12,'4','90067000000',12,'61B','13','MIERCOLES',930,1100,NULL),(13,'4','90068000000',13,'62B','20','LUNES',800,845,NULL),(14,'4','90069000000',14,'63B','14','MIERCOLES',800,845,NULL),(15,'4','90070000000',15,'64B','20','LUNES',845,1015,NULL),(16,'4','90072000000',16,'65B','20','LUNES',1015,1230,NULL),(17,'4','90074000000',17,'66B','14','MIERCOLES',845,1015,NULL),(18,'4','90077000000',18,'67B','20','MARTES',800,845,NULL),(19,'4','90081000000',19,'68B','22','LUNES',800,845,NULL),(20,'4','90082000000',20,'69B','22','LUNES',845,1015,NULL),(21,'4','90083000000',21,'70C','21','LUNES',800,1015,NULL),(22,'4','90085000000',22,'71C','22','MARTES',800,930,NULL),(23,'4','90086000000',23,'72C','13','JUEVES',800,930,NULL),(24,'4','90087000000',24,'73C','21','LUNES',1015,1230,NULL),(25,'4','90088000000',25,'74C','10','JUEVES',800,1015,NULL),(26,'4','90089000000',26,'75C','13','JUEVES',800,930,NULL),(27,'4','90090000000',27,'76C','21','MIERCOLES',800,1015,NULL),(28,'4','90091000000',28,'77C','22','MIERCOLES',800,930,NULL),(29,'4','90096000000',29,'78C','13','VIERNES',800,1015,NULL),(30,'4','90109000000',30,'79C','13','VIERNES',1015,1230,NULL),(31,'4','90110000000',31,'80D','10','VIERNES',800,930,NULL),(32,'4','90111000000',32,'81D','21','JUEVES',800,1100,NULL),(33,'4','90112000000',33,'82D','12','JUEVES',800,1015,NULL);
/*!40000 ALTER TABLE `horario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `intereses`
--

DROP TABLE IF EXISTS `intereses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `intereses` (
  `idIntereses` int(11) NOT NULL AUTO_INCREMENT,
  `codDocente` bigint(11) NOT NULL,
  `idCurso` varchar(12) NOT NULL,
  `int_adicional` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`idIntereses`),
  KEY `idI_doc` (`codDocente`),
  KEY `idI_cur` (`idCurso`),
  CONSTRAINT `idI_cur` FOREIGN KEY (`idCurso`) REFERENCES `curso` (`idcurso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idI_doc` FOREIGN KEY (`codDocente`) REFERENCES `docente` (`numdoc`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `intereses`
--

LOCK TABLES `intereses` WRITE;
/*!40000 ALTER TABLE `intereses` DISABLE KEYS */;
/*!40000 ALTER TABLE `intereses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pabellon`
--

DROP TABLE IF EXISTS `pabellon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pabellon` (
  `idpabellon` varchar(12) NOT NULL,
  `numero` int(11) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idpabellon`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pabellon`
--

LOCK TABLES `pabellon` WRITE;
/*!40000 ALTER TABLE `pabellon` DISABLE KEYS */;
INSERT INTO `pabellon` VALUES ('1',NULL,'Generales'),('2',NULL,'Especialidades'),('3',NULL,'Laboratorio'),('4',NULL,'Biblioteca'),('5',NULL,'Fia data');
/*!40000 ALTER TABLE `pabellon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parametros`
--

DROP TABLE IF EXISTS `parametros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parametros` (
  `codP_esc` varchar(12) NOT NULL,
  `codP_sem` varchar(12) NOT NULL,
  `ciclo` int(11) NOT NULL,
  `creditos` int(11) DEFAULT NULL,
  PRIMARY KEY (`codP_esc`,`codP_sem`,`ciclo`),
  KEY `idP_sem_idx` (`codP_sem`),
  CONSTRAINT `idP_esc` FOREIGN KEY (`codP_esc`) REFERENCES `escuela` (`idescuela`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idP_sem` FOREIGN KEY (`codP_sem`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parametros`
--

LOCK TABLES `parametros` WRITE;
/*!40000 ALTER TABLE `parametros` DISABLE KEYS */;
/*!40000 ALTER TABLE `parametros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfil`
--

DROP TABLE IF EXISTS `perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfil` (
  `idperfil` varchar(12) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `permisos` varchar(45) DEFAULT NULL,
  `descrip` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idperfil`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfil`
--

LOCK TABLES `perfil` WRITE;
/*!40000 ALTER TABLE `perfil` DISABLE KEYS */;
INSERT INTO `perfil` VALUES ('1','administrador','Admin',NULL),('2','director','Director',NULL);
/*!40000 ALTER TABLE `perfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plan_curricular`
--

DROP TABLE IF EXISTS `plan_curricular`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plan_curricular` (
  `idPlan` varchar(45) NOT NULL,
  `idEscuela` varchar(12) DEFAULT NULL,
  `descripcion` varchar(700) DEFAULT NULL,
  `codPc_sem` varchar(12) NOT NULL,
  `estado` int(1) NOT NULL,
  `usu_crea_reg` varchar(45) DEFAULT NULL,
  `fec_crea_reg` datetime DEFAULT NULL,
  `ult_usu_mod_reg` varchar(45) DEFAULT NULL,
  `fec_ult_mod_reg` datetime DEFAULT NULL,
  PRIMARY KEY (`idPlan`),
  UNIQUE KEY `ult_usu_mod_reg_UNIQUE` (`ult_usu_mod_reg`),
  KEY `idPc_sem_idx` (`codPc_sem`),
  KEY `fkIdEscuela_idx` (`idEscuela`),
  CONSTRAINT `fkIdEscuela` FOREIGN KEY (`idEscuela`) REFERENCES `escuela` (`idescuela`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idPc_sem` FOREIGN KEY (`codPc_sem`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plan_curricular`
--

LOCK TABLES `plan_curricular` WRITE;
/*!40000 ALTER TABLE `plan_curricular` DISABLE KEYS */;
INSERT INTO `plan_curricular` VALUES ('1',NULL,'Ingeniería de Computación y Sistemas','4',0,NULL,NULL,NULL,NULL),('2',NULL,'Ingeniería Industrial','4',0,NULL,NULL,NULL,NULL),('3',NULL,'Arquitectura','4',0,NULL,NULL,NULL,NULL),('4',NULL,'Ingeniería Civil','4',0,NULL,NULL,NULL,NULL),('5',NULL,'Ingeniería Electrónica','4',0,NULL,NULL,NULL,NULL),('6',NULL,'Ingeniería en Industrias Alimentarias','4',0,NULL,NULL,NULL,NULL),('7',NULL,'Ciencias Aeronáuticas','4',0,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `plan_curricular` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `referido`
--

DROP TABLE IF EXISTS `referido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `referido` (
  `idreferido` varchar(12) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `ape_Pat` varchar(45) DEFAULT NULL,
  `ape_Mat` varchar(45) DEFAULT NULL,
  `num_doc` varchar(16) DEFAULT NULL,
  `edad` int(3) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `especialidad` varchar(45) DEFAULT NULL,
  `titulos` varchar(45) DEFAULT NULL,
  `disponibilidad` tinyint(4) DEFAULT NULL,
  `univ_logro_acad` varchar(150) DEFAULT NULL,
  `logro_acad_insti` varchar(150) DEFAULT NULL,
  `logro_diplo` varchar(45) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `idsemestre` varchar(12) NOT NULL,
  `max_grad_alcanzado` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idreferido`),
  KEY `fk_referido_semestre` (`idsemestre`),
  CONSTRAINT `fk_referido_semestre` FOREIGN KEY (`idsemestre`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `referido`
--

LOCK TABLES `referido` WRITE;
/*!40000 ALTER TABLE `referido` DISABLE KEYS */;
/*!40000 ALTER TABLE `referido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seccion`
--

DROP TABLE IF EXISTS `seccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seccion` (
  `idseccion` varchar(16) NOT NULL,
  `descrip` varchar(45) DEFAULT NULL,
  `cuposTot` int(11) DEFAULT NULL,
  `modalidades` varchar(45) DEFAULT NULL,
  `codS_cur` varchar(12) DEFAULT NULL,
  `codS_sem` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`idseccion`),
  KEY `idS_cur_idx` (`codS_cur`),
  KEY `idS_sem_idx` (`codS_sem`),
  CONSTRAINT `idS_cur` FOREIGN KEY (`codS_cur`) REFERENCES `curso` (`idcurso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idS_sem` FOREIGN KEY (`codS_sem`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seccion`
--

LOCK TABLES `seccion` WRITE;
/*!40000 ALTER TABLE `seccion` DISABLE KEYS */;
INSERT INTO `seccion` VALUES ('50A','Seccion_A',12,'Mod1','90002000000','4'),('51A','Seccion_51A',12,'Mod2','90002000000','4'),('52A','Seccion52A',12,'Mod4',NULL,'4'),('53A','Seccion53A',10,'Mod3','90034000000','4'),('54A','Seccion54A',12,'Mod5',NULL,'4');
/*!40000 ALTER TABLE `seccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `semestre`
--

DROP TABLE IF EXISTS `semestre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `semestre` (
  `idSemestre` varchar(12) NOT NULL,
  `ano` year(4) DEFAULT NULL,
  `semestre` int(11) DEFAULT NULL,
  `cant_sema` int(11) DEFAULT NULL,
  `estado` int(1) NOT NULL,
  `usu_crea_reg` varchar(45) DEFAULT NULL,
  `fec_crea_reg` datetime DEFAULT NULL,
  `ult_usu_mod_reg` varchar(45) DEFAULT NULL,
  `fec_ult_mod_reg` datetime DEFAULT NULL,
  PRIMARY KEY (`idSemestre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `semestre`
--

LOCK TABLES `semestre` WRITE;
/*!40000 ALTER TABLE `semestre` DISABLE KEYS */;
INSERT INTO `semestre` VALUES ('1',2016,0,4,0,NULL,NULL,NULL,NULL),('2',2016,1,16,0,NULL,NULL,NULL,NULL),('3',2016,2,16,0,NULL,NULL,NULL,NULL),('4',2017,1,16,0,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `semestre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idusuario` varchar(12) NOT NULL,
  `nombreUsu` varchar(45) DEFAULT NULL,
  `Password` varchar(45) DEFAULT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `ape_Pat` varchar(45) DEFAULT NULL,
  `ape_Mat` varchar(45) DEFAULT NULL,
  `correoE` varchar(45) DEFAULT NULL,
  `fecha_actualizacion` date DEFAULT NULL,
  PRIMARY KEY (`idusuario`),
  KEY `nombreUsu` (`nombreUsu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES ('COD001','12345678','123','Juan Carlos','Abad','Escalante','sj','2017-06-06'),('COD002','87654321','321','Willian Sergio','Acosta','Acosta',NULL,NULL);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'db_final'
--

--
-- Dumping routines for database 'db_final'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-18 11:22:53
