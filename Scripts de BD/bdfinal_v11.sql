-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: db
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alumno`
--

DROP TABLE IF EXISTS `alumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alumno` (
  `idalumno` varchar(12) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `ape_pat` varchar(45) DEFAULT NULL,
  `ape_mat` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idalumno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumno`
--

LOCK TABLES `alumno` WRITE;
/*!40000 ALTER TABLE `alumno` DISABLE KEYS */;
INSERT INTO `alumno` VALUES ('1','MARTIN','BAZALAR','CONTRERAS'),('2','NOAH','BAZALAR','NEYRA');
/*!40000 ALTER TABLE `alumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aula`
--

DROP TABLE IF EXISTS `aula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aula` (
  `idaula` varchar(12) NOT NULL,
  `num_aula` int(11) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `codA_pab` varchar(12) NOT NULL,
  PRIMARY KEY (`idaula`),
  KEY `idA_pab_idx` (`codA_pab`),
  CONSTRAINT `idA_pab` FOREIGN KEY (`codA_pab`) REFERENCES `pabellon` (`idpabellon`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aula`
--

LOCK TABLES `aula` WRITE;
/*!40000 ALTER TABLE `aula` DISABLE KEYS */;
INSERT INTO `aula` VALUES ('1',101,NULL,'1'),('10',302,NULL,'1'),('11',303,NULL,'1'),('12',304,NULL,'1'),('13',101,NULL,'2'),('14',102,NULL,'2'),('15',103,NULL,'2'),('16',104,NULL,'2'),('17',105,NULL,'2'),('18',106,NULL,'2'),('19',201,NULL,'2'),('2',102,NULL,'1'),('20',202,NULL,'2'),('21',203,NULL,'2'),('22',204,NULL,'2'),('23',205,NULL,'2'),('24',206,NULL,'2'),('25',301,NULL,'2'),('26',302,NULL,'2'),('27',303,NULL,'2'),('28',304,NULL,'2'),('29',305,NULL,'2'),('3',103,NULL,'1'),('30',306,NULL,'2'),('31',101,NULL,'5'),('32',102,NULL,'5'),('33',103,NULL,'5'),('34',104,NULL,'5'),('35',201,NULL,'5'),('36',101,NULL,'3'),('37',102,NULL,'3'),('38',103,NULL,'3'),('39',201,NULL,'3'),('4',104,NULL,'1'),('40',202,NULL,'3'),('41',203,NULL,'3'),('42',301,NULL,'3'),('43',302,NULL,'3'),('44',303,NULL,'3'),('5',201,NULL,'1'),('6',202,NULL,'1'),('7',203,NULL,'1'),('8',204,NULL,'1'),('9',301,NULL,'1');
/*!40000 ALTER TABLE `aula` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curso`
--

DROP TABLE IF EXISTS `curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curso` (
  `index_curso` int(11) NOT NULL AUTO_INCREMENT,
  `idcurso` varchar(12) NOT NULL,
  `sicat` varchar(12) DEFAULT NULL,
  `nombre` varchar(60) DEFAULT NULL,
  `creditos` int(11) DEFAULT NULL,
  `h_teo` int(11) DEFAULT NULL,
  `h_lab` int(11) DEFAULT NULL,
  `h_pract` int(11) DEFAULT NULL,
  `estado_hab_inhab` tinyint(1) DEFAULT NULL,
  `req01` varchar(15) DEFAULT NULL,
  `req02` varchar(15) DEFAULT NULL,
  `req03` varchar(11) DEFAULT NULL,
  `cooreq` varchar(11) DEFAULT NULL,
  `cred_req` varchar(12) DEFAULT NULL,
  `nombreProp` varchar(45) DEFAULT NULL,
  `codLab` varchar(12) DEFAULT NULL,
  `usu_crea_reg` varchar(45) DEFAULT NULL,
  `fec_crea_reg` datetime DEFAULT NULL,
  `ult_usu_mod_reg` varchar(45) DEFAULT NULL,
  `fec_ult_mod_reg` datetime DEFAULT NULL,
  PRIMARY KEY (`index_curso`,`idcurso`),
  KEY `idcurso` (`idcurso`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curso`
--

LOCK TABLES `curso` WRITE;
/*!40000 ALTER TABLE `curso` DISABLE KEYS */;
INSERT INTO `curso` VALUES (1,'TR000501010','090005','ACTIVIDADES I',1,1,0,2,1,'','','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(2,'09000301030','090003','FILOSOFIA',3,3,0,0,1,'','','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(3,'09066301040','090663','GEOMETRIA ANALITICA',4,3,2,3,1,'','','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(4,'TR000101010','090971','INGLES I',1,0,2,0,1,'','','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(5,'09066201020','090662','INTRO. A LA INGENIERIA',2,1,2,0,1,'','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(6,'09000201020','090002','LENGUAJE',2,1,2,0,1,'','','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(7,'09066801051','090668','MATEMATICA DISCRETA',5,4,2,0,1,'','','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(8,'09071001020','090710','METODOS DE ESTUDIO ',2,1,2,0,1,'','','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(9,'09070901030','090709','REALIDAD NACIONAL',3,3,0,0,1,'','','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(10,'TR000602010','090020','ACTIVIDADES II',1,0,2,0,1,'','','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(11,'09036602050','090366','ALGEBRA LINEAL',5,4,2,0,1,'','','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(12,'09065502050','090655','CALCULO I',5,4,2,0,1,'09066801051','09066301040','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(13,'09066502031','090665','FUNDAMENTOS DISEÑO WEB',3,2,2,0,1,'09066201020','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(14,'TR000202010','091155','INGLES II',1,0,2,0,1,'TR000101010','','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(15,'09127402030','091274','INTRODUC. A LA ECONOMIA',3,2,2,0,1,'09070901030','','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(16,'09111402050','091114','INTRODUCCION A LA PROGRAM',5,3,1,3,1,'09066801051','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(17,'09111402050','091123','LAB.INTROD. A LA PROGRAM ',0,0,0,0,1,'09066801051','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(18,'09005303050','090053','ALGORIT. Y ESTR. DATOS I',5,3,1,3,1,'09111402050','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(19,'09005403040','090054','ESTADIST.Y PROBABILID. I',4,3,2,0,1,'09065502050','','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(20,'09005603050','090056','FISICA I',5,3,2,2,1,'09036602050','09065502050','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(21,'09005303050','090038','LAB. A Y E DE DATOS I',0,0,0,0,1,'09111402050','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(22,'09005603050','090058','LAB. FISICA I',0,0,0,0,1,'09036602050','09065502050','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(23,'09111503050','091128','LAB.TECNOLOGIA  DE INF.I',0,0,0,0,1,'09111402050','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(24,'09127603030','091276','SISTEMAS DE INFORMACION',3,2,2,0,1,'09066201020','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(25,'09111503050','091115','TECNOLOGIA INFORMACION I',5,4,0,2,1,'09111402050','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(26,'09006904050','090069','ALGORIT. Y ESTR. DATOS II',5,3,1,3,1,'09005303050','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(27,'09006004040','090060','ESTADIST.Y PROBABILID. II',4,3,2,0,1,'09005403040','','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(28,'09007404050','090074','FISICA II',5,3,2,2,1,'09005603050','','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(29,'09006904050','090059','LAB. A Y E DE DATOS II',0,0,0,0,1,'09005303050','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(30,'09007404050','090075','LAB. FISICA II',0,0,0,0,1,'09005603050','','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(31,'09114904040','091152','LAB.TECNOLOGIA DE INF.II',0,0,0,0,1,'09111503050','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(32,'09007704040','090077','MICROECONOMIA',4,3,2,0,1,'09127402030','','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(33,'09114904040','091149','TECNOLOGIA INFORMACION II',4,3,0,2,1,'09111503050','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(34,'09012205043','090122','CONTABILIDAD GENERAL',4,3,2,0,1,'','','','','80 CRED. APR','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(35,'09093205051','090932','GESTION DE PROCESOS',5,4,0,2,1,'09127603030','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(36,'09009005040','090090','INGENIERIA ADMINISTRATIVA',4,3,2,0,1,'09007704040','','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(37,'09093205051','091296','LAB. GESTION DE PROCESOS',0,0,0,0,1,'09127603030','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(38,'09008905050','090099','LAB. TEO.DI.BASE DE DATOS',0,0,0,0,1,'09006904050','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(39,'09127905040','091279','SERV. Y SIST. OPERATIVOS',4,3,0,2,1,'09114904040','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(40,'09008905050','090089','TEO. DIS. BASE DE DATOS',5,3,1,3,1,'09006904050','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(41,'09013106041','090131','INGENIERIA DE COSTOS',4,3,2,0,1,'09012205043','','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(42,'09011906050','090119','INGENIERIA DE SOFTWARE I',5,3,2,2,1,'09093205051','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(43,'09008506040','090085','INVESTIGACION OPERATIVA I',4,3,2,0,1,'09006004040','09066201020','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(44,'09011906050','090100','LAB. ING. SW I',0,0,0,0,1,'09093205051','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(45,'09067106050','091297','LAB. PROGRAMACION I',0,0,0,0,1,'09008905050','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(46,'09067106050','090671','PROGRAMACION I',5,3,0,4,1,'09008905050','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(47,'09008806040','090088','TEORIA GENERAL SISTEMAS',4,4,0,0,1,'09009005040','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(48,'09014507040','090145','GESTION FINANCIERA',4,3,2,0,1,'09013106041  ','','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(49,'09013707050','090137','INGENIERIA DE SOFTWARE II',5,3,1,3,1,'09011906050','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(50,'09066607040','090666','INTELIGENCIA ART.ROBOTICA',4,4,0,0,1,'09067106050','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(51,'09013707050','090123','LAB. ING. SW II',0,0,0,0,1,'09011906050','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(52,'09112107050','091121','TALLER DE PROYECTOS',5,0,10,0,1,'09067106050','09011906050','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(53,'09128808040','091288','ARQUITECTURA EMPRESARIAL',4,4,0,0,1,'09008806040','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(54,'09072108040','090721','DISE. IMPLEMENT. DE SIST.',4,3,2,0,1,'09013707050','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(55,'09054808040','090548','FORMUL.Y EVALUAC.DE PROY.',4,4,0,0,1,'09014507040','','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(56,'09066408040','090664','GESTION DE RECURSOS T.I.',4,4,0,0,1,'09013707050','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(57,'09093409040','090934','INTELIGENCIA DE NEGOCIOS',4,4,0,0,1,'09128808040','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(58,'09066700021','090667','LIDERAZGO Y ORATORIA',2,1,2,0,1,'','','','','100 CRED. AP','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(59,'09067009040','090670','PLAN. ESTRATEGICO DE TI',4,4,0,0,1,'09066408040','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(60,'09067309040','090673','PROYECTO I',4,4,0,0,1,'09054808040','09112107050','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(61,'09067909040','090679','SEG. Y AUDITORIA DE SI',4,3,2,0,1,'09072108040','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(62,'09003410022','090034','ETICA Y MORAL',2,1,2,0,1,'','','','','170 CRED. AP','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(63,'09134910040','091349','MARKETING DIGITAL',4,4,0,0,1,'09054808040','','','','','Departamento Académico\r',NULL,NULL,NULL,NULL,NULL),(64,'09007010040','090070','PROYECTO II',4,4,0,0,1,'09067309040','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(65,'090861E1040','090861','ADM. DE BASE DE DATOS',4,2,0,4,1,'09008905050','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(66,'090205E1040','090205','COMERCIO ELECTRONICO',4,4,0,0,1,'','','','','134 CRED. AP','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(67,'090933E1040','090933','GESTION DEL CONOCIMIENTO',4,3,2,0,1,'','','','','150 CRED. AP','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(68,'091119E1040','091119','SIST.INTEGRADO DE GESTION',4,4,0,0,1,'09066408040','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(69,'090608E1040','090608','TALLER CREATIVIDAD EMPR.',4,4,0,0,1,'09112107050','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(70,'090675E2040','090675','REDES Y CONECT. I CCNA',4,2,0,4,1,'09127905040','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(71,'090676E2040','090676','REDES Y CONECT. II CCNA',4,2,0,4,1,'090675E2040','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(72,'090677E2040','090677','REDES Y CONECT. III CCNA',4,2,0,4,1,'090676E2040','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(73,'090678E2040','090678','REDES Y CONECT. IV CCNA',4,2,0,4,1,'090677E2040','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(74,'090862E2040','090862','SEGURIDAD INFORMATICA',4,4,0,0,1,'09067909040','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(75,'090658E3040','090658','CALIDAD DE SOFTWARE',4,4,0,0,1,'091124E3040','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(76,'090672E3040','090672','PROGRAMACION II',4,4,0,0,1,'09067106050','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(77,'091124E3040','091124','PRUEBAS DE SOFTWARE',4,4,0,0,1,'09013707050','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(78,'090147E4020','090147','COMPORTAMIENTO ORGANIZAC.',2,2,0,0,1,'09009005040','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(79,'09086300020','090863','GESTION DE LA INNOVACION',2,2,0,0,1,'09054808040','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(80,'09085200030','090852','GESTION DE PROYECTOS-PMI',3,2,2,0,1,'09054808040','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(81,'09061700040','090617','GESTION ESTRATEGICA',4,3,2,0,1,'','','','','120 CRED. AP','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(82,'091217E4020','091217','INTROD A LA INV.INFORM.',2,2,0,0,1,'','','','','150 CRED. AP','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(83,'09011607040','090116','INVESTIGAC. OPERATIVA II',4,3,2,0,1,'09008506040','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(84,'090659E3040','090659','DESARROLLO DE APLICACIONES I',4,4,0,0,1,'090672E3040','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(85,'090660E3040','090660','DESARROLLO DE APLICACIONES I',4,4,0,0,1,'090659E3040','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(86,'091125E4020','091125','TOPICOS DE COMPUTACION',2,2,0,0,1,'','','','','134 CRED. AP','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL),(87,'091126E4040','091126','DESARROLLO DE JUEGOS',4,4,0,0,1,'09013707050','','','','','Ingeniería de Computación y Sistemas\r',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `curso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_alum_seccion`
--

DROP TABLE IF EXISTS `det_alum_seccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_alum_seccion` (
  `codDas_alum` varchar(12) NOT NULL,
  `codDas_secc` varchar(12) NOT NULL,
  KEY `idDas_alum_idx` (`codDas_alum`),
  KEY `idDas_secc_idx` (`codDas_secc`),
  CONSTRAINT `idDas_alum` FOREIGN KEY (`codDas_alum`) REFERENCES `alumno` (`idalumno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idDas_secc` FOREIGN KEY (`codDas_secc`) REFERENCES `seccion` (`idseccion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_alum_seccion`
--

LOCK TABLES `det_alum_seccion` WRITE;
/*!40000 ALTER TABLE `det_alum_seccion` DISABLE KEYS */;
/*!40000 ALTER TABLE `det_alum_seccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_asig_aula`
--

DROP TABLE IF EXISTS `det_asig_aula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_asig_aula` (
  `codDaa_aula` varchar(12) NOT NULL,
  `codDaa_seccion` varchar(12) NOT NULL,
  `disponibilidad` tinyint(4) DEFAULT NULL,
  KEY `idDaa_aula_idx` (`codDaa_aula`),
  KEY `idDaa_secc_idx` (`codDaa_seccion`),
  CONSTRAINT `idDaa_aula` FOREIGN KEY (`codDaa_aula`) REFERENCES `aula` (`idaula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idDaa_secc` FOREIGN KEY (`codDaa_seccion`) REFERENCES `seccion` (`idseccion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_asig_aula`
--

LOCK TABLES `det_asig_aula` WRITE;
/*!40000 ALTER TABLE `det_asig_aula` DISABLE KEYS */;
/*!40000 ALTER TABLE `det_asig_aula` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_curso_esc`
--

DROP TABLE IF EXISTS `det_curso_esc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_curso_esc` (
  `index_curso` int(11) NOT NULL,
  `codDce_curso` varchar(12) NOT NULL,
  `sicat` varchar(12) DEFAULT NULL,
  `codDce_escuela` varchar(12) NOT NULL,
  `idPlan` varchar(12) NOT NULL,
  `codDce_sem` varchar(12) NOT NULL,
  `ciclo` int(11) DEFAULT NULL,
  `cupos` int(11) DEFAULT NULL,
  `matriculados` int(11) DEFAULT NULL,
  `cat_fia` varchar(45) DEFAULT NULL,
  `tipo_fia` varchar(45) DEFAULT NULL,
  `tipo_sunedu` varchar(45) DEFAULT NULL,
  `tipo_pres_virt` varchar(45) DEFAULT NULL,
  `usu_crea_reg` varchar(45) DEFAULT NULL,
  `fec_crea_reg` datetime DEFAULT NULL,
  `ult_usu_mod_reg` varchar(45) DEFAULT NULL,
  `dec_ult_mod_reg` datetime DEFAULT NULL,
  `det_curso_esccol` varchar(45) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`index_curso`,`codDce_curso`),
  KEY `idDce_esc_idx` (`codDce_escuela`),
  KEY `idSem_idx` (`codDce_sem`),
  KEY `idpla` (`idPlan`),
  CONSTRAINT `idesc` FOREIGN KEY (`codDce_escuela`) REFERENCES `escuela` (`idescuela`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idpla` FOREIGN KEY (`idPlan`) REFERENCES `plan_curricular` (`idPlan`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idsem` FOREIGN KEY (`codDce_sem`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `pk_det_cur_esc` FOREIGN KEY (`index_curso`, `codDce_curso`) REFERENCES `curso` (`index_curso`, `idcurso`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_curso_esc`
--

LOCK TABLES `det_curso_esc` WRITE;
/*!40000 ALTER TABLE `det_curso_esc` DISABLE KEYS */;
INSERT INTO `det_curso_esc` VALUES (1,'TR000501010','090005','101','1','4',1,50,0,'humanidades','obligatorio','general','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(2,'09000301030','090003','101','1','4',1,50,0,'humanidades','obligatorio','general','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(3,'09066301040','090663','101','1','4',1,50,0,'matematicas y ciencias','obligatorio','general','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(4,'TR000101010','090971','101','1','4',1,50,0,'humanidades','obligatorio','general','virtual\r',NULL,NULL,NULL,NULL,NULL,1),(5,'09066201020','090662','101','1','4',1,50,0,'matematicas y ciencias','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(6,'09000201020','090002','101','1','4',1,50,0,'humanidades','obligatorio','general','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(7,'09066801051','090668','101','1','4',1,50,0,'matematicas y ciencias','obligatorio','general','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(8,'09071001020','090710','101','1','4',1,50,0,'humanidades','obligatorio','general','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(9,'09070901030','090709','101','1','4',1,50,0,'humanidades','obligatorio','general','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(10,'TR000602010','090020','101','1','4',2,50,0,'humanidades','obligatorio','general','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(11,'09036602050','090366','101','1','4',2,50,0,'matematicas y ciencias','obligatorio','general','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(12,'09065502050','090655','101','1','4',2,50,0,'metodos cuantitativos','obligatorio','general','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(13,'09066502031','090665','101','1','4',2,50,0,'ingenieria de software','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(14,'TR000202010','091155','101','1','4',2,50,0,'humanidades','obligatorio','general','virtual\r',NULL,NULL,NULL,NULL,NULL,1),(15,'09127402030','091274','101','1','4',2,50,0,'humanidades','obligatorio','general','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(16,'09111402050','091114','101','1','4',2,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(18,'09005303050','090053','101','1','4',3,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(19,'09005403040','090054','101','1','4',3,50,0,'humanidades','obligatorio','general','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(20,'09005603050','090056','101','1','4',3,50,0,'humanidades','obligatorio','general','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(24,'09127603030','091276','101','1','4',3,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(25,'09111503050','091115','101','1','4',3,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(26,'09006904050','090069','101','1','4',4,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(27,'09006004040','090060','101','1','4',4,50,0,'humanidades','obligatorio','general','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(28,'09007404050','090074','101','1','4',4,50,0,'humanidades','obligatorio','general','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(32,'09007704040','090077','101','1','4',4,50,0,'humanidades','obligatorio','general','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(33,'09114904040','091149','101','1','4',4,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(34,'09012205043','090122','101','1','4',5,50,0,'humanidades','obligatorio','general','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(35,'09093205051','090932','101','1','4',5,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(36,'09009005040','090090','101','1','4',5,50,0,'humanidades','obligatorio','general','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(39,'09127905040','091279','101','1','4',5,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(40,'09008905050','090089','101','1','4',5,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(41,'09013106041','090131','101','1','4',6,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(42,'09011906050','090119','101','1','4',6,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(43,'09008506040','090085','101','1','4',6,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(46,'09067106050','090671','101','1','4',6,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(47,'09008806040','090088','101','1','4',6,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(48,'09014507040','090145','101','1','4',7,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(49,'09013707050','090137','101','1','4',7,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(50,'09066607040','090666','101','1','4',7,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(52,'09112107050','091121','101','1','4',7,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(53,'09128808040','091288','101','1','4',8,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(54,'09072108040','090721','101','1','4',8,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(55,'09054808040','090548','101','1','4',8,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(56,'09066408040','090664','101','1','4',8,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(57,'09093409040','090934','101','1','4',9,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(58,'09066700021','090667','101','1','4',9,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(59,'09067009040','090670','101','1','4',9,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(60,'09067309040','090673','101','1','4',9,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(61,'09067909040','090679','101','1','4',9,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(62,'09003410022','090034','101','1','4',10,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(63,'09134910040','091349','101','1','4',10,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(64,'09007010040','090070','101','1','4',10,50,0,'humanidades','obligatorio','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(65,'090861E1040','090861','101','1','4',11,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(66,'090205E1040','090205','101','1','4',11,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(67,'090933E1040','090933','101','1','4',11,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(68,'091119E1040','091119','101','1','4',11,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(69,'090608E1040','090608','101','1','4',11,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(70,'090675E2040','090675','101','1','4',12,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(71,'090676E2040','090676','101','1','4',12,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(72,'090677E2040','090677','101','1','4',12,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(73,'090678E2040','090678','101','1','4',12,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(74,'090862E2040','090862','101','1','4',12,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(75,'090658E3040','090658','101','1','4',13,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(76,'090672E3040','090672','101','1','4',13,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(77,'091124E3040','091124','101','1','4',13,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(78,'090147E4020','090147','101','1','4',14,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(79,'09086300020','090863','101','1','4',14,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(80,'09085200030','090852','101','1','4',14,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(81,'09061700040','090617','101','1','4',14,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(82,'091217E4020','091217','101','1','4',14,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(83,'09011607040','090116','101','1','4',14,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(84,'090659E3040','090659','101','1','4',13,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(85,'090660E3040','090660','101','1','4',13,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(86,'091125E4020','091125','101','1','4',14,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1),(87,'091126E4040','091126','101','1','4',14,50,0,'ingenieria de software','electivo','especifico','presencial\r',NULL,NULL,NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `det_curso_esc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_doc_cur`
--

DROP TABLE IF EXISTS `det_doc_cur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_doc_cur` (
  `id` int(11) NOT NULL,
  `id_doc` bigint(12) NOT NULL,
  `id_cur` varchar(12) NOT NULL,
  `descri` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_doc_idx` (`id_doc`),
  KEY `fk_id_cur_idx` (`id_cur`),
  KEY `ind_numdoc` (`id_doc`),
  KEY `fk_id_cur_idx1` (`id_cur`),
  KEY `ind_numdoc1` (`id_doc`),
  CONSTRAINT `fk_id_cur` FOREIGN KEY (`id_cur`) REFERENCES `curso` (`idcurso`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_id_doc_1` FOREIGN KEY (`id_doc`) REFERENCES `docente` (`numdoc`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_doc_cur`
--

LOCK TABLES `det_doc_cur` WRITE;
/*!40000 ALTER TABLE `det_doc_cur` DISABLE KEYS */;
/*!40000 ALTER TABLE `det_doc_cur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_doc_referido`
--

DROP TABLE IF EXISTS `det_doc_referido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_doc_referido` (
  `codDet_ref` varchar(12) NOT NULL,
  `codDet_doc` bigint(11) NOT NULL,
  KEY `idDr_ref_idx` (`codDet_ref`),
  KEY `idDr_doc_idx` (`codDet_doc`),
  CONSTRAINT `idDr_doc` FOREIGN KEY (`codDet_doc`) REFERENCES `docente` (`numdoc`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idDr_ref` FOREIGN KEY (`codDet_ref`) REFERENCES `referido` (`idreferido`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_doc_referido`
--

LOCK TABLES `det_doc_referido` WRITE;
/*!40000 ALTER TABLE `det_doc_referido` DISABLE KEYS */;
/*!40000 ALTER TABLE `det_doc_referido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_interes_docente`
--

DROP TABLE IF EXISTS `det_interes_docente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_interes_docente` (
  `codOi_Inte` int(11) NOT NULL,
  `codOi_doc` bigint(12) NOT NULL,
  `codOi_sem` varchar(12) NOT NULL,
  KEY `codOi_sem_idx` (`codOi_sem`),
  KEY `codOi_sem_idx1` (`codOi_doc`),
  KEY `codOi_int_idx` (`codOi_Inte`),
  CONSTRAINT `codOi_doc` FOREIGN KEY (`codOi_doc`) REFERENCES `docente` (`numdoc`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `codOi_int` FOREIGN KEY (`codOi_Inte`) REFERENCES `intereses` (`idIntereses`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `codOi_sem` FOREIGN KEY (`codOi_sem`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_interes_docente`
--

LOCK TABLES `det_interes_docente` WRITE;
/*!40000 ALTER TABLE `det_interes_docente` DISABLE KEYS */;
/*!40000 ALTER TABLE `det_interes_docente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_seccion`
--

DROP TABLE IF EXISTS `det_seccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_seccion` (
  `codDs_sec` varchar(12) NOT NULL,
  `codDs_doc` int(11) NOT NULL,
  `turno` varchar(45) DEFAULT NULL,
  `actividad` varchar(45) DEFAULT NULL,
  `totMatr` int(11) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  KEY `idDs_secc_idx` (`codDs_sec`),
  KEY `idDs_doc_idx` (`codDs_doc`),
  CONSTRAINT `idDs_doc` FOREIGN KEY (`codDs_doc`) REFERENCES `docente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idDs_secc` FOREIGN KEY (`codDs_sec`) REFERENCES `seccion` (`idseccion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_seccion`
--

LOCK TABLES `det_seccion` WRITE;
/*!40000 ALTER TABLE `det_seccion` DISABLE KEYS */;
/*!40000 ALTER TABLE `det_seccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_usu_esc`
--

DROP TABLE IF EXISTS `det_usu_esc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_usu_esc` (
  `idUsuario` varchar(12) NOT NULL,
  `idEscuela` varchar(12) NOT NULL,
  `idPerfl` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`idUsuario`,`idEscuela`),
  KEY `fk_idesc_idx` (`idEscuela`),
  KEY `fk_perfilUsuario_idx` (`idPerfl`),
  CONSTRAINT `fk_idesc` FOREIGN KEY (`idEscuela`) REFERENCES `escuela` (`idescuela`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_idusu` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_perfilUsuario` FOREIGN KEY (`idPerfl`) REFERENCES `perfil` (`idperfil`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_usu_esc`
--

LOCK TABLES `det_usu_esc` WRITE;
/*!40000 ALTER TABLE `det_usu_esc` DISABLE KEYS */;
INSERT INTO `det_usu_esc` VALUES ('COD001','999','1'),('COD002','101','2');
/*!40000 ALTER TABLE `det_usu_esc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `disponibilidad_docente`
--

DROP TABLE IF EXISTS `disponibilidad_docente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disponibilidad_docente` (
  `id` int(11) NOT NULL,
  `cod_Docente` bigint(12) NOT NULL,
  `cod_semestre` varchar(45) NOT NULL,
  `hor_ini` int(11) DEFAULT NULL,
  `hor_fin` int(11) DEFAULT NULL,
  `dia` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `codDd_docente_idx` (`cod_Docente`),
  KEY `codDd_semestre_idx` (`cod_semestre`),
  CONSTRAINT `codDd_docente` FOREIGN KEY (`cod_Docente`) REFERENCES `docente` (`numdoc`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `codDd_semestre` FOREIGN KEY (`cod_semestre`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `disponibilidad_docente`
--

LOCK TABLES `disponibilidad_docente` WRITE;
/*!40000 ALTER TABLE `disponibilidad_docente` DISABLE KEYS */;
/*!40000 ALTER TABLE `disponibilidad_docente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `docente`
--

DROP TABLE IF EXISTS `docente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numdoc` bigint(12) NOT NULL,
  `apepat` varchar(45) NOT NULL,
  `apemat` varchar(45) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `pais` varchar(45) DEFAULT NULL,
  `fecini` date NOT NULL,
  `ley30220` tinyint(2) NOT NULL,
  `mayorgrado` varchar(45) NOT NULL,
  `menciongrado` varchar(45) NOT NULL,
  `univ` varchar(100) NOT NULL,
  `paisuniv` varchar(45) NOT NULL,
  `pregrado` tinyint(1) DEFAULT NULL,
  `maestria` tinyint(1) DEFAULT NULL,
  `doctorado` tinyint(1) DEFAULT NULL,
  `categoria` varchar(50) DEFAULT NULL,
  `regimen` varchar(45) DEFAULT NULL,
  `horaclase` int(2) NOT NULL,
  `horaactiv` int(2) NOT NULL,
  `totalhoras` int(2) NOT NULL,
  `investigador` tinyint(2) NOT NULL,
  `DINA` tinyint(1) DEFAULT NULL,
  `codD_sem` varchar(12) NOT NULL,
  `escuela` varchar(45) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telefono` int(15) DEFAULT NULL,
  `observaciones` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idD_sem_idx` (`codD_sem`),
  KEY `IND_NUMDOC` (`numdoc`),
  CONSTRAINT `idD_sem` FOREIGN KEY (`codD_sem`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docente`
--

LOCK TABLES `docente` WRITE;
/*!40000 ALTER TABLE `docente` DISABLE KEYS */;
INSERT INTO `docente` VALUES (1,12345678,'Abad','Escalante','Juan Carlos','Peru','2012-02-15',1,'Doctor','Administración','Usmp','PERU',1,0,1,'CONTRATADO','Tiempo Completo',16,4,20,1,0,'4','',NULL,NULL,NULL),(2,87654321,'Acosta','Acosta','Willian Sergio','Peru','2010-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(3,87654322,'Aguero','Martinez La Rosa','Julio Cesar','Peru','2010-03-17',0,'Titulo','Bachiller en Ciencias','Upc','PERU',1,1,0,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(4,87654320,'Acuña','Flores','Carlos Christian','Peru','2015-04-16',1,'Maestro','Bachiller en Biología','Unmsm','PERU',1,1,1,'CONTRATADO','Tiempo Completo',10,2,12,1,0,'4','',NULL,NULL,NULL),(5,87654323,'Aguirre','Medrano','Rosa Virginia','Peru','2009-03-17',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(6,87654324,'Alarco','Jerí','Iván Erick','Peru','2010-04-16',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(7,87654325,'Alarcon','Centti','Jose','Peru','2010-05-16',1,'Doctor','Bachiller en Economia','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(8,87654326,'Alegria','Vidal','Rosa Mercedes','Peru','2010-06-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(9,87654327,'Alferrano','Donofrio','Mirtha','Peru','2010-07-16',1,'Doctor','Bachiller en Ciencias','Utp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(10,87654328,'Ambia','Rebatta','Hugo Paulino','Peru','2010-08-16',1,'Doctor','Bachiller en Derecho','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(11,87654329,'Amoros','Figueroa','Rodrigo','Peru','2010-09-16',1,'Doctor','Bachiller en Ciencias','Unmsm','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(12,87654311,'Añaños','Gomez Sanchez','Roberto Enrique','Peru','2010-10-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(13,87654331,'Ara','Rojas','Silvia Liliana','Peru','2010-11-16',1,'Doctor','Bachiller en Educacion','Utp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(14,87654341,'Arrieta','Freyre','Javier Eduardo','Peru','2010-12-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(15,87654351,'Arrieta','Taboada','Amanda','Peru','2010-03-01',1,'Doctor','Bachiller en Ciencias','Uni','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(16,87654361,'Arriola','Guevara','Luis Alberto','Peru','2010-03-02',1,'Doctor','Bachiller en Ciencias','Uni','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(17,87654371,'Bacigalupo','Olivari','Miguel Angel','Peru','2010-03-03',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(18,87654381,'Balcazar','Hernandez','Yudy','Peru','2010-03-04',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(19,87654391,'Ballena','Gonzales','Manuel','Peru','2010-03-05',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(20,87654301,'Balta','Rospliglosi','Manuel Valeriano','Peru','2010-03-06',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(21,87654121,'Balta','Vargas-Machuca','Manuel','Peru','2010-03-07',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(22,87654221,'Barnet','Champommier','Yann Oliver','Peru','2010-03-08',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(23,87654421,'Barnett','Mendoza','Edy Dalmiro','Peru','2010-03-09',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(24,87654521,'Barrantes','Mann','Luis Alfonso','Peru','2010-03-10',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(25,87654621,'Barraza','Salguero','Victor Eduardo','Peru','2010-03-11',1,'Doctor','Bachiller en Ciencias','Utp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(26,87654721,'Barreto','Bardales','Tomas Fernando','Peru','2010-03-12',1,'Doctor','Bachiller en Ciencias','ULima','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(27,87654821,'Basile','Migliore','Jose Antonio','Peru','2010-03-20',1,'Doctor','Bachiller en Ciencias','Uni','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(28,87654921,'Bayona','Ore','Luz Sussy','Peru','2010-03-21',1,'Doctor','Bachiller en Ciencias','Uni','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(29,87654021,'Becerra','Pacherres','Augusto Oscar','Peru','2010-03-22',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(30,87651321,'Bedia','Guillen','Ciro Sergio','Peru','2010-03-23',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(31,87652321,'Benavente','Villena','Maria Elena','Peru','2010-03-24',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(32,87653321,'Benites','Gonzales','Willian Sergio','Peru','2010-03-25',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(33,87655321,'Benites','Vilela','Luis Fernando','Peru','2010-03-26',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(34,87656321,'Bernabel','Liza','Ana Maria','Peru','2010-03-27',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(35,87657321,'Bernal','Ortiz','Carlos Adolfo','Peru','2010-03-28',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(36,87658321,'Bernuy','Alva','Augusto Ernesto','Peru','2010-03-29',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(37,87659321,'Bertolotti','Zuñiga','Carmen Rosa','Peru','2010-03-30',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(38,87650321,'Bezada','Sanchez','Cesar Alfredo','Peru','2011-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(39,87614321,'Blanco','Zuñiga','Yvan','Peru','2012-03-16',1,'Doctor','Bachiller en Ciencias','Unmsm','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(40,87624321,'Bocangel','Weydert','Guillermo Augusto','Peru','2013-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(41,87634321,'Buendia','Rios','Hildebrando','Peru','2014-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(42,87644321,'Bustos','Diaz','Silverio','Peru','2015-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(43,87664321,'Cabrera','Iturria','Ricardo','Peru','2016-03-16',1,'Doctor','Bachiller en Ciencias','Utp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(44,87674321,'Caceres','Echegaray','Hector Alejandro','Peru','2009-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(45,87684321,'Caceres','Lampen','Manuel Alejandro','Peru','2008-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(46,87694321,'Calderon','Caceres','Jose Luis','Peru','2007-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(47,87604321,'Campos','Perez','Rosalvina','Peru','2006-03-16',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(48,87154321,'Cano','Tejada','Jose Antonio','Peru','2005-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(49,87254321,'Caparachin','Chuquihuaraca','Jaime Santos','Peru','2004-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(50,87354321,'Cardenas','Lucero','Luis','Peru','2003-03-16',1,'Doctor','Bachiller en Ciencias','Uni','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(51,87454321,'Cardenas','Martinez','Jose Antonio','Peru','2002-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(52,87554321,'Cardenas','Zavala','Germain Leonardo','Peru','2001-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(53,87754321,'Carpio','Delgado','Guitter Guillermo','Peru','2000-03-16',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(54,87854321,'Casavilca','Maldonado','Edmundo','Peru','2010-04-01',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(55,87954321,'Castillo','Cavero','Rodolfo','Peru','2010-04-02',1,'Doctor','Bachiller en Ciencias','ULima','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(56,87054321,'Castillo','Sini','Gustavo Alejandro','Peru','2010-04-03',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(57,81654321,'Castro','Salazar','Fredy Adan','Peru','2010-04-04',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(58,82654321,'Cataño','Espinoza','Humberto Daniel','Peru','2010-04-05',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(59,83654321,'Ccoyure','Tito','Ricardo Wilber','Peru','2010-04-06',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(60,84654321,'Celi','Saavedra','Luis','Peru','2010-04-07',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(61,85654321,'Cerdan','Chavarri','Mario Wilbe','Peru','2010-04-08',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(62,86654321,'Cerpa','Espinosa','David','Peru','2010-04-09',1,'Doctor','Bachiller en Ciencias','Ucv','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(63,88654321,'Cerron','Ruiz','Eulogio','Peru','2010-04-10',1,'Doctor','Bachiller en Ciencias','Uap','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(64,89654321,'Cevallos','Echevarria','Alejandro Nestor','Peru','2010-04-11',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(65,80654321,'Chacon','Moscoso','Hugo Liu','Peru','2010-04-12',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(66,17654321,'Chanca','Cortez','Luis Manuel','Peru','2010-04-13',1,'Doctor','Bachiller en Ciencias','Utp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(67,27654321,'Chavarry','Vallejos','Carlos Magno','Peru','2010-04-14',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(68,37654321,'Chavez','Rodriguez','Michael Jesus','Peru','2010-04-15',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(69,47654321,'Cieza','Davila','Javier Eduardo','Peru','2010-04-17',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(70,57654321,'Consigliere','Cevasco','Luis Ricardo','Peru','2010-04-18',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(71,67654321,'Contreras','Villarreal','Luis Wilfredo','Peru','2010-04-19',1,'Doctor','Bachiller en Ciencias','ULima','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(72,77654321,'Cortez','Silva','Dimas Antonio','Peru','2010-04-20',1,'Doctor','Bachiller en Ciencias','Unmsm','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(73,97654321,'Cuadros','Ricra','Ruben Dario','Peru','2010-04-21',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(74,7654321,'De La Vega','Picoaga','Fresia','Peru','2010-04-22',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(75,84654321,'De La Torre','Puente','Maria Elena','Peru','2010-04-23',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(76,87954321,'De Los Rios','Hermoza','Justo Rafael','Peru','2010-04-24',1,'Doctor','Bachiller en Ciencias','Utp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(77,81554321,'De Olarte','Tristan','Jorge Luis','Peru','2010-04-25',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(78,82154321,'Del Carpio','Damian','Christian Carlos','Peru','2010-04-26',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(79,82254321,'Diaz','Tejada','Gabriel Eduardo','Peru','2010-04-27',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(80,82354321,'Durán','Ramirez','Gary Gary','Peru','2010-04-28',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(81,82454321,'Egoavil','La Torre','Victor Raul','Peru','2010-04-29',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(82,82554321,'Elaez','Cisneros','Eliazaf Guillermo','Peru','2010-04-30',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(83,82654321,'Enriquez','Quinde','Benjamin Carlos','Peru','2010-05-01',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(84,82754321,'Esparza','Castillo','Olenka','Peru','2010-05-02',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(85,82854321,'Estela','Benavides','Bertha','Peru','2010-05-03',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(86,82954321,'Falcon','Soto','Arnaldo','Peru','2010-05-04',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(87,83054321,'Fano','Miranda','Gonzalo','Peru','2010-05-05',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(88,83154321,'Figueroa','Lezama','Rafael','Peru','2010-05-06',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(89,83254321,'Figueroa','Revilla','Jorge Martin','Peru','2010-05-07',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(90,83354321,'Florian','Castillo','Tulio Elias','Peru','2010-05-08',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(91,83454321,'Fuentes','Flores','Maximo','Peru','2010-05-09',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(92,83554321,'Galindo','Guerra','Gary Joseph Loui','Peru','2010-05-10',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(93,83654321,'Gallegos','Valderrama','Meriedi','Peru','2010-05-11',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(94,83754321,'Galloso','Gentile','Alberto Cesar','Peru','2010-05-12',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(95,83854321,'Gamarra','Villacorta','Raul','Peru','2010-05-13',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(96,83954321,'Gamboa','Cruzado','Javier Arturo','Peru','2010-05-14',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(97,84054321,'Gamero','Rengifo','Luis Fernando','Peru','2010-05-18',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(98,84154321,'Garcia','Farje','Ruben Osvaldo','Peru','2010-05-17',1,'Doctor','Bachiller en Ciencias','ULima','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(99,84254321,'Garcia','Flores','Elva Nelly','Peru','2010-05-19',1,'Doctor','Bachiller en Ciencias','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(100,84354321,'Garcia','Guerra','Juan Diego','Peru','2010-05-20',1,'Doctor','Bachiller en Ciencias','Utp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(101,84454321,'Garcia','Lorente','Cesar','Peru','2010-05-21',1,'Doctor','Bachiller en Ciencias','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(102,84554321,'Garcia','Nuñez','Luz Elena','Peru','2010-05-22',1,'Doctor','Bachiller en Ciencias','Unmsm','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(103,12365479,'Garcia','Sanchez','Marta Pamela','Peru','2008-03-13',1,'Maestría','Bachiller en Ciencias','Upn','PERU',0,1,1,'CONTRATADO','Tiempo Completo',15,8,23,1,0,'4','',NULL,NULL,NULL),(104,88888888,'Geronimo','Vasquez','Alfonso Herminio','Peru','2010-03-15',1,'Doctor','Bachiller en Arquitectura','Usil','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',15,5,20,1,0,'4','',NULL,NULL,NULL),(105,11111111,'Gomero','Cordova','Eduardo Fernando','Peru','2003-03-14',0,'Título','Bachiller en Economía','Ucv','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(106,34532689,'Gonzales','Alva','Luis Eduardo','Peru','2010-03-13',1,'Maestría','Bachiller en Educación','Upn','PERU',1,1,0,'CONTRATADO','Tiempo Completo',14,5,19,0,0,'4','',NULL,NULL,NULL),(107,99999999,'Gonzales','Chavesta','Celso','Peru','1999-03-16',1,'Maestro','Bachiller en Computación y Sistemas','Uap','PERU',0,1,1,'CONTRATADO','Tiempo Parcial',13,5,18,1,0,'4','',NULL,NULL,NULL),(108,17893492,'Gonzales','Sanchez','Juan Julio','Peru','2007-03-10',1,'Título','Bachiller en Derecho','Usil','PERU',1,1,1,'CONTRATADO','Tiempo Completo',12,8,20,1,0,'4','',NULL,NULL,NULL),(109,9431874,'Gonzalo','Silva','Eduardo Ramon','Peru','2004-03-20',1,'Doctor','Bachiller en Ciencias Matemáticas','Uigv','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',15,6,21,1,0,'4','',NULL,NULL,NULL),(110,74073297,'Grandez','Pizarro','Waldy Mercedes','Peru','2012-03-23',1,'Título','Bachiller en Economía','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(111,23148930,'Guerrero','Navarro','Raul Agustin','Peru','2005-03-13',1,'Doctor','Bachiller en Ciencias Contables','Usmp','PERU',1,1,1,'CONTRATADO','Tiempo Completo',15,6,21,1,0,'4','',NULL,NULL,NULL),(112,83704341,'Guzmán','Tasayco','Alfonso','Peru','2010-03-18',1,'Maestro','Bachiller en Derecho','Upc','PERU',1,1,1,'CONTRATADO','Tiempo Parcial',12,6,18,1,0,'4','',NULL,NULL,NULL),(113,1010101,'Guzman','Rouviros','Julio Alejandro','Peru','2004-03-12',1,'Doctor','Bachiller en Ciencias','Utp','PERU',1,0,1,'CONTRATADO','Tiempo Parcial',13,6,19,1,0,'4','',NULL,NULL,NULL),(123,333333,'Abad','Escalante','Juan Carlos','Perú','2010-02-15',1,'Doctor','Administración','Usmp','Perú',1,0,1,'1','Tiempo Completo',16,4,20,1,0,'4','sistemas',NULL,NULL,'');
/*!40000 ALTER TABLE `docente` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER DocenteAuditoria AFTER INSERT ON docente
  FOR EACH ROW
		INSERT INTO `docente_audi`
(`id_doc`,
`numdoc`,
`apepat`,
`apemat`,
`nombre`,
`pais`,
`fecini`,
`ley30220`,
`mayorgrado`,
`menciongrado`,
`univ`,
`paisuniv`,
`pregrado`,
`maestria`,
`doctorado`,
`categoria`,
`regimen`,
`horaclase`,
`horaactiv`,
`totalhoras`,
`investigador`,
`DINA`,
`codD_sem`,
`escuela`,
`email`,
`telefono`,
`observaciones`
)
VALUES
( New.`id`,
New.`numdoc`,
New.`apepat`,
New.`apemat`,
New.`nombre`,
New.`pais`,
New.`fecini`,
New.`ley30220`,
New.`mayorgrado`,
New.`menciongrado`,
New.`univ`,
New.`paisuniv`,
New.`pregrado`,
New.`maestria`,
New.`doctorado`,
New.`categoria`,
New.`regimen`,
New.`horaclase`,
New.`horaactiv`,
New.`totalhoras`,
New.`investigador`,
New.`DINA`,
New.`codD_sem`,
New.`escuela`,
New.`email`,
New.`telefono`,
New.`observaciones`
) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER Doc_Aud_Upd AFTER UPDATE ON docente
  FOR EACH ROW
		INSERT INTO `docente_audi`
(`id_doc`,
`numdoc`,
`apepat`,
`apemat`,
`nombre`,
`pais`,
`fecini`,
`ley30220`,
`mayorgrado`,
`menciongrado`,
`univ`,
`paisuniv`,
`pregrado`,
`maestria`,
`doctorado`,
`categoria`,
`regimen`,
`horaclase`,
`horaactiv`,
`totalhoras`,
`investigador`,
`DINA`,
`codD_sem`,
`escuela`,
`email`,
`telefono`,
`observaciones`
)
VALUES
( New.`id`,
New.`numdoc`,
New.`apepat`,
New.`apemat`,
New.`nombre`,
New.`pais`,
New.`fecini`,
New.`ley30220`,
New.`mayorgrado`,
New.`menciongrado`,
New.`univ`,
New.`paisuniv`,
New.`pregrado`,
New.`maestria`,
New.`doctorado`,
New.`categoria`,
New.`regimen`,
New.`horaclase`,
New.`horaactiv`,
New.`totalhoras`,
New.`investigador`,
New.`DINA`,
New.`codD_sem`,
New.`escuela`,
New.`email`,
New.`telefono`,
New.`observaciones`
) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `docente_audi`
--

DROP TABLE IF EXISTS `docente_audi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docente_audi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_doc` int(11) NOT NULL,
  `numdoc` bigint(12) NOT NULL,
  `apepat` varchar(45) NOT NULL,
  `apemat` varchar(45) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `pais` varchar(45) DEFAULT NULL,
  `fecini` date NOT NULL,
  `ley30220` tinyint(2) NOT NULL,
  `mayorgrado` varchar(45) NOT NULL,
  `menciongrado` varchar(45) NOT NULL,
  `univ` varchar(100) NOT NULL,
  `paisuniv` varchar(45) NOT NULL,
  `pregrado` tinyint(1) DEFAULT NULL,
  `maestria` tinyint(1) DEFAULT NULL,
  `doctorado` tinyint(1) DEFAULT NULL,
  `categoria` varchar(50) DEFAULT NULL,
  `regimen` varchar(45) DEFAULT NULL,
  `horaclase` int(2) NOT NULL,
  `horaactiv` int(2) NOT NULL,
  `totalhoras` int(2) NOT NULL,
  `investigador` tinyint(2) NOT NULL,
  `DINA` tinyint(1) DEFAULT NULL,
  `codD_sem` varchar(12) NOT NULL,
  `escuela` varchar(45) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telefono` int(15) DEFAULT NULL,
  `observaciones` varchar(45) DEFAULT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_doc` (`id_doc`),
  CONSTRAINT `docente_audi_ibfk_1` FOREIGN KEY (`id_doc`) REFERENCES `docente` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docente_audi`
--

LOCK TABLES `docente_audi` WRITE;
/*!40000 ALTER TABLE `docente_audi` DISABLE KEYS */;
INSERT INTO `docente_audi` VALUES (1,123,12341234,'Abad','Escalante','Juan Carlos','Perú','2010-02-15',1,'Doctor','Administración','Usmp','Perú',1,0,1,'1','Tiempo Completo',16,4,20,1,0,'4','sistemas',NULL,NULL,'','2017-09-17 02:05:18'),(2,123,333333,'Abad','Escalante','Juan Carlos','Perú','2010-02-15',1,'Doctor','Administración','Usmp','Perú',1,0,1,'1','Tiempo Completo',16,4,20,1,0,'4','sistemas',NULL,NULL,'','2017-09-17 03:12:27');
/*!40000 ALTER TABLE `docente_audi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipo`
--

DROP TABLE IF EXISTS `equipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipo` (
  `idequipo` varchar(12) NOT NULL,
  `codE_aula` varchar(12) NOT NULL,
  `modelo` varchar(45) DEFAULT NULL,
  `decripcion` varchar(45) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  PRIMARY KEY (`idequipo`),
  KEY `idE_aula_idx` (`codE_aula`),
  CONSTRAINT `idE_aula` FOREIGN KEY (`codE_aula`) REFERENCES `aula` (`idaula`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipo`
--

LOCK TABLES `equipo` WRITE;
/*!40000 ALTER TABLE `equipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `escuela`
--

DROP TABLE IF EXISTS `escuela`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `escuela` (
  `idescuela` varchar(12) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `facultad` varchar(45) DEFAULT NULL,
  `usu_crea_reg` varchar(45) DEFAULT NULL,
  `fec_crea_reg` datetime DEFAULT NULL,
  `ult_usu_mod_reg` varchar(45) DEFAULT NULL,
  `fec_ult_mod_reg` datetime DEFAULT NULL,
  PRIMARY KEY (`idescuela`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `escuela`
--

LOCK TABLES `escuela` WRITE;
/*!40000 ALTER TABLE `escuela` DISABLE KEYS */;
INSERT INTO `escuela` VALUES ('101','Ingenieria de Computacion y Sistemas','FIA',NULL,NULL,NULL,NULL),('202','Ingenieria Industrial','FIA',NULL,NULL,NULL,NULL),('303','Arquitectura','FIA',NULL,NULL,NULL,NULL),('404','Ingeniería Civil','FIA',NULL,NULL,NULL,NULL),('505','Ingeniería Electrónica','FIA',NULL,NULL,NULL,NULL),('606','Ingeniería en Industrias Alimentarias','FIA',NULL,NULL,NULL,NULL),('707','Ciencias Aeronáuticas','FIA',NULL,NULL,NULL,NULL),('999','Admin','Admin',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `escuela` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horario`
--

DROP TABLE IF EXISTS `horario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horario` (
  `idHorario` int(11) NOT NULL AUTO_INCREMENT,
  `codH_sem` varchar(12) NOT NULL,
  `codH_curso` varchar(12) NOT NULL,
  `codH_doc` int(11) NOT NULL,
  `codH_secc` varchar(12) NOT NULL,
  `codH_aula` varchar(12) NOT NULL,
  `dia` varchar(12) DEFAULT NULL,
  `hora_ini` int(11) DEFAULT NULL,
  `hora_fiN` int(11) DEFAULT NULL,
  `cod_esc` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`idHorario`,`codH_sem`,`codH_curso`,`codH_doc`,`codH_secc`,`codH_aula`),
  KEY `fk_horario_semestre` (`codH_sem`),
  KEY `fk_horario_curso` (`codH_curso`),
  KEY `fk_horario_docente` (`codH_doc`),
  KEY `fk_horario_seccion` (`codH_secc`),
  KEY `fk_horario_aula` (`codH_aula`),
  KEY `fk_horario_escuela` (`cod_esc`),
  CONSTRAINT `fk_horario_aula` FOREIGN KEY (`codH_aula`) REFERENCES `aula` (`idaula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_horario_curso` FOREIGN KEY (`codH_curso`) REFERENCES `curso` (`idcurso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_horario_docente` FOREIGN KEY (`codH_doc`) REFERENCES `docente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_horario_escuela` FOREIGN KEY (`cod_esc`) REFERENCES `escuela` (`idescuela`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_horario_seccion` FOREIGN KEY (`codH_secc`) REFERENCES `seccion` (`idseccion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_horario_semestre` FOREIGN KEY (`codH_sem`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horario`
--

LOCK TABLES `horario` WRITE;
/*!40000 ALTER TABLE `horario` DISABLE KEYS */;
INSERT INTO `horario` VALUES (1,'4','90002000000',1,'50A','13','LUNES',800,930,'101'),(2,'4','90003000000',1,'51A','14','LUNES',800,910,'101'),(3,'4','90002000000',1,'52A','15','MIERCOLES',800,845,'101'),(4,'4','90034000000',2,'53A','1','MARTES',800,845,'101'),(5,'4','90060000000',3,'58A','13','MARTES',800,930,'101'),(6,'4','90037000000',6,'55A','14','MARTES',800,1015,'202'),(7,'4','90049000000',7,'56A','15','LUNES',800,1015,'404'),(8,'4','90053000000',8,'57A','15','LUNES',845,1100,'101'),(9,'4','90054000000',2,'54A','13','MARTES',845,1015,'101'),(10,'4','90056000000',10,'59A','14','LUNES',930,1100,NULL),(11,'4','90060000000',11,'60B','13','MIERCOLES',800,930,NULL),(12,'4','90067000000',12,'61B','13','MIERCOLES',930,1100,NULL),(13,'4','90068000000',13,'62B','20','LUNES',800,845,NULL),(14,'4','90069000000',14,'63B','14','MIERCOLES',800,845,NULL),(15,'4','90070000000',15,'64B','20','LUNES',845,1015,NULL),(16,'4','90072000000',16,'65B','20','LUNES',1015,1230,NULL),(17,'4','90074000000',17,'66B','14','MIERCOLES',845,1015,NULL),(18,'4','90077000000',18,'67B','20','MARTES',800,845,NULL),(19,'4','90081000000',19,'68B','22','LUNES',800,845,NULL),(20,'4','90082000000',20,'69B','22','LUNES',845,1015,NULL),(21,'4','90083000000',21,'70C','21','LUNES',800,1015,NULL),(22,'4','90085000000',22,'71C','22','MARTES',800,930,NULL),(23,'4','90086000000',23,'72C','13','JUEVES',800,930,NULL),(24,'4','90087000000',24,'73C','21','LUNES',1015,1230,NULL),(25,'4','90088000000',25,'74C','10','JUEVES',800,1015,NULL),(26,'4','90089000000',26,'75C','13','JUEVES',800,930,NULL),(27,'4','90090000000',27,'76C','21','MIERCOLES',800,1015,NULL),(28,'4','90091000000',28,'77C','22','MIERCOLES',800,930,NULL),(29,'4','90096000000',29,'78C','13','VIERNES',800,1015,NULL),(30,'4','90109000000',30,'79C','13','VIERNES',1015,1230,NULL),(31,'4','90110000000',31,'80D','10','VIERNES',800,930,NULL),(32,'4','90111000000',32,'81D','21','JUEVES',800,1100,NULL),(33,'4','90112000000',33,'82D','12','JUEVES',800,1015,NULL);
/*!40000 ALTER TABLE `horario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `intereses`
--

DROP TABLE IF EXISTS `intereses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `intereses` (
  `idIntereses` int(11) NOT NULL AUTO_INCREMENT,
  `codDocente` bigint(11) NOT NULL,
  `idCurso` varchar(12) NOT NULL,
  `int_adicional` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`idIntereses`),
  KEY `idI_doc` (`codDocente`),
  KEY `idI_cur` (`idCurso`),
  CONSTRAINT `idI_cur` FOREIGN KEY (`idCurso`) REFERENCES `curso` (`idcurso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idI_doc` FOREIGN KEY (`codDocente`) REFERENCES `docente` (`numdoc`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `intereses`
--

LOCK TABLES `intereses` WRITE;
/*!40000 ALTER TABLE `intereses` DISABLE KEYS */;
/*!40000 ALTER TABLE `intereses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pabellon`
--

DROP TABLE IF EXISTS `pabellon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pabellon` (
  `idpabellon` varchar(12) NOT NULL,
  `numero` int(11) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idpabellon`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pabellon`
--

LOCK TABLES `pabellon` WRITE;
/*!40000 ALTER TABLE `pabellon` DISABLE KEYS */;
INSERT INTO `pabellon` VALUES ('1',NULL,'Generales'),('2',NULL,'Especialidades'),('3',NULL,'Laboratorio'),('4',NULL,'Biblioteca'),('5',NULL,'Fia data');
/*!40000 ALTER TABLE `pabellon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parametros`
--

DROP TABLE IF EXISTS `parametros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parametros` (
  `codP_esc` varchar(12) NOT NULL,
  `codP_sem` varchar(12) NOT NULL,
  `ciclo` int(11) NOT NULL,
  `creditos` int(11) DEFAULT NULL,
  PRIMARY KEY (`codP_esc`,`codP_sem`,`ciclo`),
  KEY `idP_sem_idx` (`codP_sem`),
  CONSTRAINT `idP_esc` FOREIGN KEY (`codP_esc`) REFERENCES `escuela` (`idescuela`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idP_sem` FOREIGN KEY (`codP_sem`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parametros`
--

LOCK TABLES `parametros` WRITE;
/*!40000 ALTER TABLE `parametros` DISABLE KEYS */;
/*!40000 ALTER TABLE `parametros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfil`
--

DROP TABLE IF EXISTS `perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfil` (
  `idperfil` varchar(12) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `permisos` varchar(45) DEFAULT NULL,
  `descrip` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idperfil`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfil`
--

LOCK TABLES `perfil` WRITE;
/*!40000 ALTER TABLE `perfil` DISABLE KEYS */;
INSERT INTO `perfil` VALUES ('1','administrador','Admin',NULL),('2','director','Director',NULL);
/*!40000 ALTER TABLE `perfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plan_curricular`
--

DROP TABLE IF EXISTS `plan_curricular`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plan_curricular` (
  `idPlan` varchar(45) NOT NULL,
  `idEscuela` varchar(12) NOT NULL,
  `descripcion` varchar(700) DEFAULT NULL,
  `codPc_sem` varchar(12) NOT NULL,
  `usu_crea_reg` varchar(45) DEFAULT NULL,
  `fec_crea_reg` datetime DEFAULT NULL,
  `ult_usu_mod_reg` varchar(45) DEFAULT NULL,
  `fec_ult_mod_reg` datetime DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idPlan`),
  UNIQUE KEY `ult_usu_mod_reg_UNIQUE` (`ult_usu_mod_reg`),
  KEY `idPc_sem_idx` (`codPc_sem`),
  KEY `fkIdEscuela_idx` (`idEscuela`),
  CONSTRAINT `fkIdEscuela` FOREIGN KEY (`idEscuela`) REFERENCES `escuela` (`idescuela`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idPc_sem` FOREIGN KEY (`codPc_sem`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plan_curricular`
--

LOCK TABLES `plan_curricular` WRITE;
/*!40000 ALTER TABLE `plan_curricular` DISABLE KEYS */;
INSERT INTO `plan_curricular` VALUES ('1','101','Ingeniería de Computación y Sistemas','4',NULL,NULL,NULL,NULL,0),('2','202','Ingeniería Industrial','4',NULL,NULL,NULL,NULL,0),('3','303','Arquitectura','4',NULL,NULL,NULL,NULL,0),('4','404','Ingeniería Civil','4',NULL,NULL,NULL,NULL,0),('5','505','Ingeniería Electrónica','4',NULL,NULL,NULL,NULL,0),('6','606','Ingeniería en Industrias Alimentarias','4',NULL,NULL,NULL,NULL,0),('7','707','Ciencias Aeronáuticas','4',NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `plan_curricular` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `referido`
--

DROP TABLE IF EXISTS `referido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `referido` (
  `idreferido` varchar(12) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `ape_Pat` varchar(45) DEFAULT NULL,
  `ape_Mat` varchar(45) DEFAULT NULL,
  `num_doc` varchar(16) DEFAULT NULL,
  `edad` int(3) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `especialidad` varchar(45) DEFAULT NULL,
  `titulos` varchar(45) DEFAULT NULL,
  `disponibilidad` tinyint(4) DEFAULT NULL,
  `univ_logro_acad` varchar(150) DEFAULT NULL,
  `logro_acad_insti` varchar(150) DEFAULT NULL,
  `logro_diplo` varchar(45) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `idsemestre` varchar(12) NOT NULL,
  `max_grad_alcanzado` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idreferido`),
  KEY `fk_referido_semestre` (`idsemestre`),
  CONSTRAINT `fk_referido_semestre` FOREIGN KEY (`idsemestre`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `referido`
--

LOCK TABLES `referido` WRITE;
/*!40000 ALTER TABLE `referido` DISABLE KEYS */;
/*!40000 ALTER TABLE `referido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seccion`
--

DROP TABLE IF EXISTS `seccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seccion` (
  `idseccion` varchar(16) NOT NULL,
  `descrip` varchar(45) DEFAULT NULL,
  `cuposTot` int(11) DEFAULT NULL,
  `modalidades` varchar(45) DEFAULT NULL,
  `codS_cur` varchar(12) DEFAULT NULL,
  `codS_sem` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`idseccion`),
  KEY `idS_cur_idx` (`codS_cur`),
  KEY `idS_sem_idx` (`codS_sem`),
  CONSTRAINT `idS_cur` FOREIGN KEY (`codS_cur`) REFERENCES `curso` (`idcurso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idS_sem` FOREIGN KEY (`codS_sem`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seccion`
--

LOCK TABLES `seccion` WRITE;
/*!40000 ALTER TABLE `seccion` DISABLE KEYS */;
INSERT INTO `seccion` VALUES ('50A','Seccion_A',12,'Mod1','90002000000','4'),('51A','Seccion_51A',12,'Mod2','90002000000','4'),('52A','Seccion52A',12,'Mod4',NULL,'4'),('53A','Seccion53A',10,'Mod3','90034000000','4'),('54A','Seccion54A',12,'Mod5',NULL,'4');
/*!40000 ALTER TABLE `seccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `semestre`
--

DROP TABLE IF EXISTS `semestre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `semestre` (
  `idSemestre` varchar(12) NOT NULL,
  `ano` year(4) DEFAULT NULL,
  `semestre` int(11) DEFAULT NULL,
  `cant_sema` int(11) DEFAULT NULL,
  `cred_especialidad` int(11) DEFAULT NULL,
  `cred_general` int(11) DEFAULT NULL,
  `usu_crea_reg` varchar(45) DEFAULT NULL,
  `fec_crea_reg` datetime DEFAULT NULL,
  `ult_usu_mod_reg` varchar(45) DEFAULT NULL,
  `fec_ult_mod_reg` datetime DEFAULT NULL,
  PRIMARY KEY (`idSemestre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `semestre`
--

LOCK TABLES `semestre` WRITE;
/*!40000 ALTER TABLE `semestre` DISABLE KEYS */;
INSERT INTO `semestre` VALUES ('1',2016,0,4,NULL,NULL,NULL,NULL,NULL,NULL),('2',2016,1,16,NULL,NULL,NULL,NULL,NULL,NULL),('3',2016,2,16,NULL,NULL,NULL,NULL,NULL,NULL),('4',2017,1,16,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `semestre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idusuario` varchar(12) NOT NULL,
  `nombreUsu` varchar(45) DEFAULT NULL,
  `Password` varchar(45) DEFAULT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `ape_Pat` varchar(45) DEFAULT NULL,
  `ape_Mat` varchar(45) DEFAULT NULL,
  `correoE` varchar(45) DEFAULT NULL,
  `fecha_actualizacion` date DEFAULT NULL,
  PRIMARY KEY (`idusuario`),
  KEY `nombreUsu` (`nombreUsu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES ('COD001','12345678','123','Juan Carlos','Abad','Escalante','sj','2017-06-06'),('COD002','87654321','321','Willian Sergio','Acosta','Acosta',NULL,NULL);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'db'
--

--
-- Dumping routines for database 'db'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-25 17:43:27
